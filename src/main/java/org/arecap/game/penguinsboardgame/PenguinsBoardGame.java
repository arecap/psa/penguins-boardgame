package org.arecap.game.penguinsboardgame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PenguinsBoardGame {

    public static void main(String... args) {
        SpringApplication.run(PenguinsBoardGame.class, args);
    }

}
