var PenguinUserModel = {

    clientUuid : null,

    ssmContext: null,

}


var PenguinUserModelPresenter = {

    modelReady: false,

    mineOrLoadUserModel: function(callback) {
        if(PenguinUserModelPresenter.getSessionUserModel() != null) {
            PenguinUserModel = JSON.parse(window.sessionStorage.pum);
            callback.$server.loadWebClientModel(PenguinUserModel);
            PenguinUserModelPresenter.modelReady = true;
        }  else {
            callback.$server.mineWebClient();
        }
    },

    initModel: function(uuidBlock) {
        PenguinUserModel.clientUuid = uuidBlock;
        if(PenguinUserModelPresenter.hasPersistenceStorage()) {
            PenguinUserModelPresenter.persist();
        }
    },

    hasPersistenceStorage: function() {
        return 'undefined' != typeof window.localStorage;
    },

    modelSaveSsmContext(ssmContext) {
        PenguinUserModel.ssmContext = JSON.parse(ssmContext);
        if(PenguinUserModelPresenter.hasPersistenceStorage()) {
            PenguinUserModelPresenter.persist();
        }
    },

    persist: function() {
        try {
            window.sessionStorage.setItem("pum", JSON.stringify(PenguinUserModel));
            PenguinUserModelPresenter.modelReady = true;
        } catch (err) {
            PenguinUserModelPresenter.modelReady = false;
        }
    },

    getSessionUserModel: function() {
        if('undefined' != window.sessionStorage.pum) {
            return window.sessionStorage.pum;
        }
        return null;
    },


}
