package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Player {

    private String uuid;

    private String avatarPath;

    private String colorCode;

}
