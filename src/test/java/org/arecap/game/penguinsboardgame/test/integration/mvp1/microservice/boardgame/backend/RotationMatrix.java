package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;

public final class RotationMatrix {

    public static ComplexPlane getComplexPlaneWithRotationAngle(ComplexPlane complexPlane, Double rotationAngle) {
        return new ComplexPlane(complexPlane.getRealValue() * Math.cos(Math.toRadians(rotationAngle))
                - complexPlane.getImaginaryValue() * Math.sin(Math.toRadians(rotationAngle)),
                complexPlane.getRealValue() * Math.sin(Math.toRadians(rotationAngle))
                        + complexPlane.getImaginaryValue() * Math.cos(Math.toRadians(rotationAngle)));
    }

}
