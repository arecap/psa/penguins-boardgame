package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.gui;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.flow.shared.Registration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Stream;

public interface Broadcaster<T> {

    Logger logger = LoggerFactory.getLogger(Broadcaster.class);

    ObjectMapper objectMapper = new ObjectMapper();

    Map<String, Consumer<T>> getConsumerMap();

    default Registration register(String uuid, Consumer<T> consumer) {
        getConsumerMap().put(uuid, consumer);
        return () -> getConsumerMap().remove(consumer);
    }

    @Async
    default void broadcast(String uuid, T message) throws JsonProcessingException {
        logger.info("broadcast message:\t" + objectMapper.writeValueAsString(message) + "\texclude channel:\t" + uuid);
        getConsumerMap().keySet().parallelStream().filter(key -> !key.equals(uuid))
                .map(key -> getConsumerMap().get(key))
                .forEach(consumer -> consumer.accept(message));
    }

    @Async
    default void broadcast(T message) throws JsonProcessingException {
        logger.info("broadcast message:\t" + objectMapper.writeValueAsString(message));
        getConsumerMap().values().parallelStream().forEach(consumer -> consumer.accept(message));
    }

    @Async
    default void sendTo(String uuid, T message) throws JsonProcessingException {
        Optional<Consumer<T>> consumer = Optional.ofNullable(getConsumerMap().get(uuid));
        if(consumer.isPresent()) {
            logger.info("broadcast message:\t" + objectMapper.writeValueAsString(message) + "send to channel:\t" + uuid);
            consumer.get().accept(message);
        } else {
            logger.error("channel uuid:\t" + uuid + "\tnot registered!");
        }

    }

    @Async
    default void sendTo(String[] uuids, T message) throws JsonProcessingException {
        logger.info("Start broadcast channels:\t" + objectMapper.writeValueAsString(uuids));
        Stream.of(uuids).parallel().forEach(uuid -> {
            try {
                sendTo(uuid, message);
            } catch (JsonProcessingException e) {
                logger.error("Message is not serializable!");
            }
        });
    }

}
