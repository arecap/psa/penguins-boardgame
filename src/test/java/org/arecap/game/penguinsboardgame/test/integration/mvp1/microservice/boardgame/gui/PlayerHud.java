package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.gui;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import lombok.Setter;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public class PlayerHud extends Div implements ComponentEventListener<DetachEvent> {


    private final Logger logger = LoggerFactory.getLogger(PlayerHud.class);


    private Paragraph scoreDisplayText = new Paragraph("000");

    private Div scoreDisplayLayout = new Div(scoreDisplayText);

    private Icon scoreIcon = VaadinIcon.USER_STAR.create();

    private HorizontalLayout scoreLayout = new HorizontalLayout(scoreIcon, scoreDisplayLayout);

    private Paragraph timerDisplayText = new Paragraph("00:00");

    private Div timerDisplayLayout = new Div(timerDisplayText);

    private Icon timerIcon = VaadinIcon.USER_CLOCK.create();

    private HorizontalLayout timerLayout = new HorizontalLayout(timerIcon, timerDisplayLayout);

    private HorizontalLayout playerInfoLayout = new HorizontalLayout(timerLayout, scoreLayout);

    private HorizontalLayout pawnsLayout = new HorizontalLayout();

    private Map<Integer, Div> pawnsImages = new LinkedHashMap<>();

    private Button menuButton = new Button(VaadinIcon.MENU.create());

    private Div menuButtonLayout = new Div(menuButton);

    private VerticalLayout playerParticipantsLayout = new VerticalLayout();

    @Setter
    private Optional<MainMenu> mainMenu = Optional.empty();

    public PlayerHud() {
        setup();
    }

    public PlayerHud(Component... children) {
        super(children);
        setup();
    }

    @Override
    public void onComponentEvent(DetachEvent event) {
        logger.info("detach player hud");
    }

    private void setup() {
        setSizeFull();
        getStyle().set("pointer-events", "none");
        getStyle().set("position", "absolute");
        getStyle().set("top", "0px");
        getStyle().set("left", "0px");
        getStyle().set("background-color", "transparent");
        getStyle().set("z-index", "1100");
        timerLayout.setWidth("100px");
        timerLayout.getStyle().set("border", "2px solid #506787");
        timerLayout.getStyle().set("background-color", "rgb(246, 156, 153)");
        timerIcon.setColor("#506787");
        timerIcon.getStyle().set("height", "100%");
        timerDisplayText.getStyle().set("color", "#506787");
        scoreLayout.setWidth("100px");
        scoreLayout.getStyle().set("border", "2px solid #506787");
        scoreLayout.getStyle().set("background-color", "rgb(246, 156, 153)");
        scoreIcon.setColor("#506787");
        scoreIcon.getStyle().set("height", "100%");
        scoreDisplayText.getStyle().set("color", "#506787");
        playerInfoLayout.setWidthFull();
        playerInfoLayout.setHeight("30px");
        playerInfoLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
//        playerInfoLayout.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.END);
//        playerInfoLayout.getStyle().set("pointer-events", "all");
        playerInfoLayout.getStyle().set("position", "absolute");
        playerInfoLayout.getStyle().set("left", "0px");
        playerInfoLayout.getStyle().set("top", "18px");
        playerInfoLayout.getStyle().set("background-color", "transparent");

        pawnsLayout.setWidthFull();
        pawnsLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
        pawnsLayout.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.END);
        pawnsLayout.getStyle().set("pointer-events", "all");
        pawnsLayout.getStyle().set("position", "absolute");
        pawnsLayout.getStyle().set("left", "0px");
        pawnsLayout.getStyle().set("bottom", "0px");
        pawnsLayout.getStyle().set("border-top", "5px solid #506787");
//        pawnsLayout.getStyle().set("border-left", "5px solid #506787");
//        pawnsLayout.getStyle().set("border-right", "5px solid #506787");
        menuButtonLayout.getStyle().set("pointer-events", "all");
        menuButtonLayout.getStyle().set("position", "absolute");
        menuButtonLayout.getStyle().set("right", "15px");
        menuButtonLayout.getStyle().set("top", "15px");
        menuButton.addClickListener(e -> menuButtonClick());
        menuButton.setWidth("46px");
        menuButton.setHeight("46px");
        menuButton.getStyle().set("background-color", "rgb(246, 156, 153)");
        menuButton.getStyle().set("border-radius", "50%");
        menuButton.getStyle().set("border", "5px solid #506787");
//        playerParticipantsLayout.getStyle().set("pointer-events", "none");
        playerParticipantsLayout.getStyle().set("position", "absolute");
        playerParticipantsLayout.getStyle().set("left", "0px");
        playerParticipantsLayout.getStyle().set("top", "0px");
        playerParticipantsLayout.getStyle().set("min-height", "70%");
        playerParticipantsLayout.setPadding(false);
        playerParticipantsLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.EVENLY);
//        playerParticipantsLayout.setAlignItems(FlexComponent.Alignment.CENTER);
        add(playerInfoLayout, pawnsLayout, menuButtonLayout, playerParticipantsLayout);
    }

    private void menuButtonClick() {
        //TODO
        logger.info("menu button clicked");
        if(mainMenu.isPresent()) {
            UI.getCurrent().getPage().executeJs("$('#"+ MainMenu.MAIN_MENU_ELEMENT_ID +"').modal('show');\n" +
                    "$('#"+ MainMenu.MAIN_MENU_ELEMENT_ID +"').on('hidden.bs.modal', function (e) {\n" +
                    "\t$0.$server.onMainMenuHidden();\n" +
                    "})\n", getElement());
            getStyle().set("display", "none");
        }
    }

    @ClientCallable
    public void onMainMenuHidden() {
        getStyle().remove("display");
    }

    public void setupPlayers(BoardGame boardGame, Player player, Game2dGraphicTransformSettings game2dGraphicTransformSettings) {
//        menuButton.setWidth(clientBounds.getRealValue() > 900D ? "46px" : "26px");
//        menuButton.setHeight(clientBounds.getRealValue() > 900D ? "46px" : "26px");
        //TODO
        pawnsLayout.getStyle().set("background-color", player.getColorCode());
        pawnsLayout.setHeight(game2dGraphicTransformSettings.getClientBounds().getRealValue() > 900D ? "55px" : "37px");
        pawnsLayout.removeAll();
        pawnsImages.clear();
        Map<Integer, Penguin> availablePenguins = BoardGameService.getAvailablePenguins(boardGame, player);
        for(int i = 0; i < availablePenguins.size(); ++i) {
            Div imageContainer = new Div();
            imageContainer.setId("penguin-index-"+i);
            imageContainer.addClickListener(clickEvent -> selectPenguin(Integer.valueOf(clickEvent.getSource().getId().get().substring(14))));
            imageContainer.getStyle().set("position", "relative");
            Image image = new Image("frontend/images/player_ice.png", "");
            image.setWidth("100%");
//            image.getStyle().set("position", "absolute");
            image.getStyle().set("left", "0px");
            image.getStyle().set("top", "0px");
            imageContainer.setWidth(game2dGraphicTransformSettings.getClientBounds().getRealValue() > 900D ? "106px" : "64px");
            imageContainer.add(image);
            pawnsLayout.add(imageContainer);
            pawnsImages.put(i, imageContainer);
        }
        setupAvailablePawns(availablePenguins);
        setupPlayerParticipantsLayout(boardGame, player);
    }

    private void setupPlayerParticipantsLayout(BoardGame boardGame, Player player) {
        playerParticipantsLayout.removeAll();
        boardGame.getPlayers().values().stream()
                .filter(p -> !p.equals(player))
                .forEach(participant -> setupParticipant(boardGame, participant));
    }

    private void setupParticipant(BoardGame boardGame, Player participant) {
        HorizontalLayout participantLayout = new HorizontalLayout();
        participantLayout.setPadding(false);
        participantLayout.getStyle().set("pointer-events", "all");
        participantLayout.getStyle().set("border-style", "solid");
        participantLayout.getStyle().set("border-color", "#506787");
        participantLayout.getStyle().set("border-width", "5px 5px 5px 0px");
        participantLayout.getStyle().set("border-radius", "0px 35% 39% 0px");
        participantLayout.setHeight("65px");
        participantLayout.setWidth("100px");
        participantLayout.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        participantLayout.getStyle().set("background-color", participant.getColorCode());
        setupParticipantPawnAvatar(participantLayout, BoardGameService.getPlayerPenguins(boardGame, participant.getUuid()).get(0));
        setupParticipantInfo(participantLayout, BoardGameService.getAvailablePenguins(boardGame, participant).size(),
                BoardGameService.getPlayerScore(boardGame, participant));
        playerParticipantsLayout.add(participantLayout);
    }

    private void setupParticipantInfo(HorizontalLayout participantLayout, Integer availablePawns, Integer playerScore) {
        Paragraph availablePawnsText = new Paragraph(availablePawns + "");
        availablePawnsText.getStyle().set("color", "white");
        availablePawnsText.getStyle().set("margin", "0px");
        availablePawnsText.getStyle().set("padding", "0px");
        Paragraph playerScoreText = new Paragraph(playerScore + "");
        playerScoreText.getStyle().set("color", "white");
        playerScoreText.getStyle().set("margin", "0px");
        playerScoreText.getStyle().set("padding", "0px");
        Div participantInfoLayout = new Div(availablePawnsText, playerScoreText);
        participantInfoLayout.getStyle().set("margin-left", "5px");
        participantLayout.add(participantInfoLayout);
    }

    private void setupParticipantPawnAvatar(HorizontalLayout participantLayout, Penguin penguin) {
        Image penguinImage = new Image(penguin.getAvatarPath(), "");
        penguinImage.setWidth("100%");
        penguinImage.getStyle().set("position", "relative");
        Div imageContainer = new Div(penguinImage);
        imageContainer.setWidth("55px");
        participantLayout.add(imageContainer);
    }

    private void selectPenguin(Integer penguinIndex) {
        logger.info("select penguin index:\t" + penguinIndex);
    }

    private void setupAvailablePawns(Map<Integer, Penguin> availablePenguins) {
        availablePenguins.keySet().stream().forEach(index -> setupAvailablePawn(pawnsImages.get(index), availablePenguins.get(index)));
    }

    private void setupAvailablePawn(Div imageContainer, Penguin penguin) {
        imageContainer.getChildren().forEach(component -> ((HasStyle)component).getStyle().set("position", "absolute"));
        Image penguinImage = new Image(penguin.getAvatarPath(), "");
        penguinImage.setWidth("100%");
        penguinImage.getStyle().set("position", "relative");
        imageContainer.add(penguinImage);
    }

}
