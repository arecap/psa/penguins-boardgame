package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Getter @Setter
public class UuidBlock implements Serializable {

    private Logger logger = LoggerFactory.getLogger(UuidBlock.class);

    private String hash;
    private String previousHash;
    private String data;
    private long timeStamp;
    private int nonce;

    public UuidBlock(String data, String previousHash, long timeStamp) {
        setData(data);
        setPreviousHash(previousHash);
        setTimeStamp(timeStamp);
        setHash(calculateBlockHash());
    }

    public String calculateBlockHash() {
        String dataToHash = previousHash
                + Long.toString(timeStamp)
                + Integer.toString(nonce)
                + data;
        MessageDigest digest = null;
        byte[] bytes = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            bytes = digest.digest(dataToHash.getBytes());
        } catch (NoSuchAlgorithmException ex) {
            logger.error(ex.getMessage(), ex.getStackTrace());
        }
        StringBuffer buffer = new StringBuffer();
        for (byte b : bytes) {
            buffer.append(String.format("%02x", b));
        }
        return buffer.toString();
    }

    public String mineBlock(int prefix) {
        String prefixString = new String(new char[prefix]).replace('\0', '0');
        while (!hash.substring(0, prefix).equals(prefixString)) {
            nonce++;
            hash = calculateBlockHash();
        }
        return hash;
    }

}
