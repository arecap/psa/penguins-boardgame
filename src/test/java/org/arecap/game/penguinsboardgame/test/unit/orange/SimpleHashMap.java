package org.arecap.game.penguinsboardgame.test.unit.orange;

import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

public class SimpleHashMap<K, V> implements Map<K, V> {


    private static final int MIN_TABLE_SIZE = 2;

    class Node<K, V> implements Map.Entry<K, V> {

        @Getter
        private K key;

        @Getter
        private V value;

        @Getter @Setter
        private Node<K, V> collision;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        V mergeCollision(Node<K, V> hashCollisionNode) {
            if(key.equals(hashCollisionNode.getKey())) {
                return setValue(hashCollisionNode.getValue());
            }
            if(Optional.ofNullable(collision).isPresent()) {
                return collision.mergeCollision(hashCollisionNode);
            }
            setCollision(hashCollisionNode);
            tableSize++;
            return null;
        }

        @Override
        public V setValue(V value) {
            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }
    }

    transient int tableSize = 0;

    transient Node<K, V>[] table;

    @Override
    public int size() {
        return Optional.ofNullable(table).isPresent() ? table.length : 0;
    }

    @Override
    public boolean isEmpty() {
        return Optional.ofNullable(table).isPresent();
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public V get(Object key) {
        Optional<Node<K, V>> r = getNode(key.hashCode(), key);
        return r.isPresent() ? r.get().getValue() : null;
    }

    private Optional<Node<K, V>> getNode(int hashCode, Object key) {
        return null;
    }

    @Override
    public V put(K key, V value) {
        Node<K, V> newNode = new Node<>(key, value);
        Optional<Node<K,V>> node = table == null ? Optional.empty()
                : Optional.ofNullable(table[key.hashCode() & (table.length - 1)]); // mod the table allocation length
        if(node.isPresent()) {
            return putHashCollisionNode(node.get(), newNode);
        }
        if(table == null || table.length < tableSize) {
            resizeTable();
        }
        tableSize++;
        table[key.hashCode() & (table.length - 1)] = newNode;
        return null;
    }

    private void resizeTable() {
        Node<K, V>[] newTable = new Node[table == null ? MIN_TABLE_SIZE : table.length << 1];
        if(table != null) {
            tableSize = 0;
            configureNewTable(newTable);
        }
        table = newTable;
    }

    private void configureNewTable(Node<K, V>[] newTable) {
        Stream.of(table)
                .filter(node -> node != null)
                .forEach(node -> setNetTable(node, newTable));
    }

    private void setNetTable(Node<K, V> node, Node<K, V>[] newTable) {
        if(Optional.ofNullable(node.getCollision()).isPresent()) {
            setNetTable(node.getCollision(), newTable);
        }
        Node<K, V> newNode = new Node<>(node.getKey(), node.getValue());
        int position = node.getKey().hashCode() & (newTable.length - 1); // mod the table allocation length
        Optional<Node<K, V>> hashNode = Optional.ofNullable(newTable[position]);
        if(hashNode.isPresent()) {
            hashNode.get().mergeCollision(newNode);
        } else {
           tableSize++;
           newTable[position] = newNode;
        }
    }

    private V putHashCollisionNode(Node<K, V> first, Node<K, V> newNode) {
        if(first.getKey().equals(newNode.getKey())) {
            return first.setValue(newNode.getValue());
        }
        return first.mergeCollision(newNode);
    }

    @Override
    public V remove(Object key) {
        return null;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<K> keySet() {
        return null;
    }

    @Override
    public Collection<V> values() {
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }
}
