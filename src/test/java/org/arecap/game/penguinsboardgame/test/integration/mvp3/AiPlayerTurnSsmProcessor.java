package org.arecap.game.penguinsboardgame.test.integration.mvp3;

import org.springframework.statemachine.StateContext;

public interface AiPlayerTurnSsmProcessor {

    boolean isAiPlayerTurn(StateContext<String, String> stateContext);

    void onAiPlayerTurn(StateContext<String, String> stateContext);

}
