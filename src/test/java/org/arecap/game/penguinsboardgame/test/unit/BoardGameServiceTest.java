package org.arecap.game.penguinsboardgame.test.unit;

import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.*;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

public class BoardGameServiceTest {

    @Test
    public void testBoardGameService() {
        Game2dGraphicTransformSettings game2dGraphicTransformSettings = new Game2dGraphicTransformSettings();
        game2dGraphicTransformSettings.setClientBounds(new ComplexPlane(1200d, 900d));
        game2dGraphicTransformSettings.setClientCenter(new ComplexPlane(600D, 450D));
        game2dGraphicTransformSettings.setScale(1D);
        game2dGraphicTransformSettings.setRotationAngle(0D);
        BoardGame boardGame4Players = BoardGameService.constructBoardGame(4, game2dGraphicTransformSettings);
        Assert.isTrue(boardGame4Players.getIceBoard().keySet().size() == 60, "Ice Board error");
        int totalScore = getBoardGameTotalScore(boardGame4Players);
        Assert.isTrue(totalScore == 100, "Ice Board score error");
        Assert.isTrue(boardGame4Players.getPenguins().keySet().size() == 8, "Penguins number error");
        boardGame4Players.getPlayerPenguins().values().stream()
                .forEach(playerPenguins -> Assert.isTrue(playerPenguins.size() == 2, "Player penguins size error"));
        BoardGame boardGame3Players = BoardGameService.constructBoardGame(3, game2dGraphicTransformSettings);
        Assert.isTrue(boardGame3Players.getPenguins().keySet().size() == 9, "Penguins number error");
        boardGame3Players.getPlayerPenguins().values().stream()
                .forEach(playerPenguins -> Assert.isTrue(playerPenguins.size() == 3, "Player penguins size error"));
        BoardGame boardGame2Players = BoardGameService.constructBoardGame(2, game2dGraphicTransformSettings);
        Assert.isTrue(boardGame2Players.getPenguins().keySet().size() == 8, "Penguins number error");
        boardGame2Players.getPlayerPenguins().values().stream()
                .forEach(playerPenguins -> Assert.isTrue(playerPenguins.size() == 4, "Player penguins size error"));
    }

    private int getBoardGameTotalScore(BoardGame boardGame) {
        return boardGame.getIceBoard().values().stream().map(Ice::getFishValue).reduce(0, Integer::sum);
    }

}
