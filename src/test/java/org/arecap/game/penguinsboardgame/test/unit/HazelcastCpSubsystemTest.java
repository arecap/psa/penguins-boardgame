package org.arecap.game.penguinsboardgame.test.unit;

import com.hazelcast.config.Config;
import com.hazelcast.config.cp.CPSubsystemConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IAtomicLong;
import com.hazelcast.cp.CPGroup;
import com.hazelcast.cp.CPSubsystemManagementService;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/*
 * https://hazelcast.com/blog/riding-the-cp-subsystem/
 */
public class HazelcastCpSubsystemTest {

    private static Logger logger = LoggerFactory.getLogger(HazelcastCpSubsystemTest.class);


    @Test
    public void hcpsBasicTest() throws InterruptedException {
        Config config = new Config();
        config.getCPSubsystemConfig().setCPMemberCount(3);
        HazelcastInstance hz1 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz2 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz3 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz4 = Hazelcast.newHazelcastInstance(config);

        for (HazelcastInstance hz : Arrays.asList(hz1, hz2, hz3)) {
            hz.getCPSubsystem().getCPSubsystemManagementService().awaitUntilDiscoveryCompleted(10, TimeUnit.SECONDS);
            logger.info(hz.getCluster().getLocalMember() + " initialized the CP subsystem with identity: "
                    + hz.getCPSubsystem().getLocalCPMember());
        }

        // We can access to the CP data structures from any Hazelcast member
        logger.info("hz1 member counter value:\n" + hz1.getCPSubsystem().getAtomicLong("counter").incrementAndGet());
        logger.info("hz4 member counter value:\n" + hz4.getCPSubsystem().getAtomicLong("counter").incrementAndGet());
    }

    @Test
    public void hcpsMemberGroupTest() throws InterruptedException, ExecutionException {
        Config config = new Config();
        config.getCPSubsystemConfig().setCPMemberCount(5);
        config.getCPSubsystemConfig().setGroupSize(3);
        HazelcastInstance hz1 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz2 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz3 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz4 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz5 = Hazelcast.newHazelcastInstance(config);

        for (HazelcastInstance hz : Arrays.asList(hz1, hz2, hz3, hz4, hz5)) {
            hz.getCPSubsystem().getCPSubsystemManagementService().awaitUntilDiscoveryCompleted(10, TimeUnit.SECONDS);
            logger.info(hz.getCluster().getLocalMember() + " initialized the CP subsystem with identity: "
                    + hz.getCPSubsystem().getLocalCPMember());
        }

        CPSubsystemManagementService cpSubsystemManagementService = hz1.getCPSubsystem().getCPSubsystemManagementService();
        CPGroup metadataGroup = cpSubsystemManagementService.getCPGroup(CPGroup.METADATA_CP_GROUP_NAME).get();
        assert metadataGroup.members().size() == 3;
        logger.info("Metadata CP group has the following CP members: " + metadataGroup.members());

        // Let's initiate the Default CP group

        hz1.getCPSubsystem().getAtomicLong("counter1").incrementAndGet();

        CPGroup defaultGroup = cpSubsystemManagementService.getCPGroup(CPGroup.DEFAULT_GROUP_NAME).get();
        assert defaultGroup.members().size() == 3;
        logger.info("Default CP group has the following CP members: " + defaultGroup.members());

        // Let's create another CP group

        String customCPGroupName = "custom";
        hz1.getCPSubsystem().getAtomicLong("counter2@" + customCPGroupName).incrementAndGet();

        CPGroup customGroup = cpSubsystemManagementService.getCPGroup(customCPGroupName).get();
        assert customGroup.members().size() == 3;
        logger.info(customCPGroupName + " CP group has the following CP members: " + customGroup.members());
    }

    @Test
    public void hcpsInstanceFailuresTest() throws InterruptedException, ExecutionException {
        Config config = new Config();
        config.getCPSubsystemConfig().setCPMemberCount(5);
        HazelcastInstance hz1 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz2 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz3 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz4 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz5 = Hazelcast.newHazelcastInstance(config);

        for (HazelcastInstance hz : Arrays.asList(hz1, hz2, hz3, hz4, hz5)) {
            hz.getCPSubsystem().getCPSubsystemManagementService().awaitUntilDiscoveryCompleted(10, TimeUnit.SECONDS);
            logger.info(hz.getCluster().getLocalMember() + " initialized the CP subsystem with identity: "
                    + hz.getCPSubsystem().getLocalCPMember());
        }

        IAtomicLong counter = hz1.getCPSubsystem().getAtomicLong("counter");
        counter.incrementAndGet();

        String hz3CPMemberUid = hz3.getCPSubsystem().getLocalCPMember().getUuid();
        String hz4CPMemberUid = hz4.getCPSubsystem().getLocalCPMember().getUuid();
        String hz5CPMemberUid = hz5.getCPSubsystem().getLocalCPMember().getUuid();

        // 2 CP members crash...
        hz3.getLifecycleService().terminate();
        hz4.getLifecycleService().terminate();

        // Crashed CP members are removed.
        CPSubsystemManagementService cpSubsystemManagementService = hz1.getCPSubsystem().getCPSubsystemManagementService();
        cpSubsystemManagementService.removeCPMember(hz3CPMemberUid).get();
        cpSubsystemManagementService.removeCPMember(hz4CPMemberUid).get();

        CPGroup metadataGroup = cpSubsystemManagementService.getCPGroup(CPGroup.METADATA_CP_GROUP_NAME).get();
        assert metadataGroup.members().size() == 3;
        logger.info("Metadata CP group has the following CP members: " + metadataGroup.members());

        // Whoops! Another CP member crashes...
        hz5.getLifecycleService().terminate();

        logger.info("The CP Subsystem is still available with 2 CP members running.");
        counter.incrementAndGet();

        cpSubsystemManagementService.removeCPMember(hz5CPMemberUid).get();

        // Let's start new members and promote them to the CP role
        HazelcastInstance hz6 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz7 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz8 = Hazelcast.newHazelcastInstance(config);
        hz6.getCPSubsystem().getCPSubsystemManagementService().promoteToCPMember().get();
        hz7.getCPSubsystem().getCPSubsystemManagementService().promoteToCPMember().get();
        hz8.getCPSubsystem().getCPSubsystemManagementService().promoteToCPMember().get();

        // Now all CP groups are recovered back to 5 CP members
        metadataGroup = cpSubsystemManagementService.getCPGroup(CPGroup.METADATA_CP_GROUP_NAME).get();
        assert metadataGroup.members().size() == 5;
        logger.info("Metadata CP group has the following CP members: " + metadataGroup.members());
    }


    @Test
    public void hcpsAutomationFailuresCpMembersTest() throws InterruptedException, ExecutionException {
        Config config = new Config();
        CPSubsystemConfig cpSubsystemConfig = config.getCPSubsystemConfig();
        cpSubsystemConfig.setCPMemberCount(5);
        cpSubsystemConfig.setSessionHeartbeatIntervalSeconds(1);
        cpSubsystemConfig.setSessionTimeToLiveSeconds(5);
        cpSubsystemConfig.setMissingCPMemberAutoRemovalSeconds(10);
        HazelcastInstance hz1 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz2 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz3 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz4 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz5 = Hazelcast.newHazelcastInstance(config);

        for (HazelcastInstance hz : Arrays.asList(hz1, hz2, hz3, hz4, hz5)) {
            hz.getCPSubsystem().getCPSubsystemManagementService().awaitUntilDiscoveryCompleted(1, TimeUnit.MINUTES);
            logger.info(hz.getCluster().getLocalMember() + " initialized the CP subsystem with identity: "
                    + hz.getCPSubsystem().getLocalCPMember());
        }

        // We add a new CP member to the cluster.
        HazelcastInstance hz6 = Hazelcast.newHazelcastInstance(config);
        hz6.getCPSubsystem().getCPSubsystemManagementService().promoteToCPMember().get();

        // A CP member crashes...
        hz5.getLifecycleService().terminate();

        CPSubsystemManagementService cpSubsystemManagementService = hz1.getCPSubsystem().getCPSubsystemManagementService();

        // The crashed CP member will be automatically removed and substituted by the new CP member.
        while (true) {
            CPGroup metadataGroup = cpSubsystemManagementService.getCPGroup(CPGroup.METADATA_CP_GROUP_NAME).get();
            if (metadataGroup.members().size() == 5 &&
                    metadataGroup.members().contains(hz6.getCPSubsystem().getLocalCPMember())) {
                logger.info("The promoted member has been added to the Metadata CP group member list: "
                        + metadataGroup.members());
                break;
            }

            Thread.sleep(1000);
        }
    }

    @Test
    public void hcpsObliviateRescueTest() throws InterruptedException, ExecutionException {
        Config config = new Config();
        CPSubsystemConfig cpSubsystemConfig = config.getCPSubsystemConfig();
        cpSubsystemConfig.setCPMemberCount(3);
        HazelcastInstance hz1 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz2 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz3 = Hazelcast.newHazelcastInstance(config);

        for (HazelcastInstance hz : Arrays.asList(hz1, hz2, hz3)) {
            hz.getCPSubsystem().getCPSubsystemManagementService().awaitUntilDiscoveryCompleted(10, TimeUnit.SECONDS);
            logger.info(hz.getCluster().getLocalMember() + " initialized the CP subsystem with identity: "
                    + hz.getCPSubsystem().getLocalCPMember());
        }

        // 2 CP member crashes and the CP Subsystem loses its availability :(
        hz2.getLifecycleService().terminate();
        hz3.getLifecycleService().terminate();

        // The only option left is to restart the CP Subsystem.
        // To do this, we need to make sure that there are 3 members in the Hazelcast cluster
        HazelcastInstance hz4 = Hazelcast.newHazelcastInstance(config);
        HazelcastInstance hz5 = Hazelcast.newHazelcastInstance(config);

        CPSubsystemManagementService cpSubsystemManagementService = hz1.getCPSubsystem().getCPSubsystemManagementService();
        cpSubsystemManagementService.restart().get();

        for (HazelcastInstance hz : Arrays.asList(hz4, hz5)) {
            hz.getCPSubsystem().getCPSubsystemManagementService().awaitUntilDiscoveryCompleted(10, TimeUnit.SECONDS);
            logger.info(hz.getCluster().getLocalMember() + " initialized the CP subsystem with identity: "
                    + hz.getCPSubsystem().getLocalCPMember());
        }

        // The CP subsystem is formed by the new cluster members
        Collection cpMembers = cpSubsystemManagementService.getCPMembers().get();
        assert cpMembers.size() == 3;
        assert cpMembers.contains(hz1.getCPSubsystem().getLocalCPMember());
        assert cpMembers.contains(hz4.getCPSubsystem().getLocalCPMember());
        assert cpMembers.contains(hz5.getCPSubsystem().getLocalCPMember());

        for (HazelcastInstance hz : Arrays.asList(hz1, hz4, hz5)) {
            hz.getLifecycleService().terminate();
        }
    }

}
