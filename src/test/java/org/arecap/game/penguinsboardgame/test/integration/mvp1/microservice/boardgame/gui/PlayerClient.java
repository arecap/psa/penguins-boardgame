package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.gui;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.Getter;
import lombok.Setter;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.DistributedImdgBlockchainService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PreDestroy;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;

@SpringComponent
@UIScope
public class PlayerClient  {

    private static Logger logger = LoggerFactory.getLogger(PlayerClient.class);

    private static String entity = "client";

    @Autowired
    private DistributedImdgBlockchainService distributedImdgBlockchainService;

    @Getter @Setter
    private String uuid;

    private String blockchainNodeId;

    public void registerMine(Consumer<String> uuidCallback) {
        register(uuidCallback);
        try {
            distributedImdgBlockchainService.mineBlock(UUID.randomUUID().toString(), entity);
        } catch (JsonProcessingException e) {
            //todo
            logger.error("mine error");

        }
    }

    public void register(Consumer<String> uuidCallback) {
        blockchainNodeId = distributedImdgBlockchainService.registerNode(uuidBlock -> {
            if(!Optional.ofNullable(uuid).isPresent()) {
                uuid = uuidBlock.getHash();
                uuidCallback.accept(uuid);
            }
            logger.info("client ready blockchain hash:\t" + uuidBlock.getHash());
        }, entity);
    }

    @PreDestroy
    private void preDestroyPostProcessor() {
        logger.info("destroy blockchainNodeId:\t" + blockchainNodeId);
        distributedImdgBlockchainService.destroyNode(blockchainNodeId, entity);
    }


    public boolean isValid(String clientUuid) {
        return distributedImdgBlockchainService.isValid(clientUuid, entity);
    }
}
