package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.gui;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.VaadinIcon;

public class MainMenu extends Div {

    public static String MAIN_MENU_ELEMENT_ID = "penguinsBoardGameModalMainMenu";

    private Image footerLogo = new Image("frontend/images/noiantech_logo.png", "");

    private Div modalMenuFooter = new Div(footerLogo);

    private Button gameStats = new Button("Game Stats", VaadinIcon.DASHBOARD.create());

    private Button resumeGame = new Button("Resume Game", VaadinIcon.CUBES.create());

    private Button newGame = new Button("New Game", VaadinIcon.CROSSHAIRS.create());

    private Div modalMenuBody = new Div(newGame, resumeGame, gameStats);

    private Image headerLogo = new Image("frontend/images/logo2.png", "");

    public Div modalMenuHeader = new Div(headerLogo);

    private Div modalContentMenu = new Div(modalMenuHeader, modalMenuBody, modalMenuFooter);

    private Div modalDialogMenu = new Div(modalContentMenu);

    public MainMenu() {
        add(modalDialogMenu);
        setup();
    }


    private void setup() {
        setupMainMenu();
        setupModalContentMenu();
    }

    private void setupModalContentMenu() {
        modalDialogMenu.addClassNames("modal-dialog", "modal-dialog-centered");
        modalDialogMenu.getElement().setAttribute("role", "document");
        modalContentMenu.addClassName("modal-content");
        modalMenuHeader.addClassName("modal-header");
        modalMenuBody.addClassName("modal-body");
        modalMenuBody.getStyle().set("display", "grid");
        modalMenuFooter.addClassName("modal-footer");
        footerLogo.setWidthFull();
    }

    private void setupMainMenu() {
        addClassNames("modal", "fade");
        setId(MAIN_MENU_ELEMENT_ID);
        getElement().setAttribute("tabindex", "-1");
        getElement().setAttribute("role", "dialog");
        getElement().setAttribute("aria-labelledby", MAIN_MENU_ELEMENT_ID + "Title");
        getElement().setAttribute("aria-hidden", "true");
    }

}
