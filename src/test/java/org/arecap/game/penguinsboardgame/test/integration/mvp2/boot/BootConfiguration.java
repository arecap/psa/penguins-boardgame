package org.arecap.game.penguinsboardgame.test.integration.mvp2.boot;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ITopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.FixedSubscriberChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.PollableChannel;

import javax.annotation.PostConstruct;

@Configuration
@EnableIntegration
public class BootConfiguration {


    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public Config config() {
        Config config = new Config();
        config.getCPSubsystemConfig().setCPMemberCount(3);
        return config;
    }

    @Bean
    @Primary
    public HazelcastInstance hazelcastInstance() {
        return Hazelcast.newHazelcastInstance(config());
    }

    @Bean
    public HazelcastInstance dummyHazelcastInstanceCpSubsystem1() {
        return Hazelcast.newHazelcastInstance(config());
    }

    @Bean
    public HazelcastInstance dummyHazelcastInstanceCpSubsystem2() {
        return Hazelcast.newHazelcastInstance(config());
    }

    @Bean
    public PollableChannel hazelcastQueueChannel() {
        return new QueueChannel(hazelcastInstance().getQueue("siQueue"));
    }

    @Bean
    public ITopic<Message<?>> siTopic() {
        return hazelcastInstance().getTopic("siTopic");
    }

    @Bean
    public MessageChannel publishToHazelcastTopicChannel(ITopic<Message<?>> siTopic) {
        return new FixedSubscriberChannel(siTopic::publish);
    }

    @Bean
    public MessageChannel fromHazelcastTopicChannel() {
        return new DirectChannel();
    }

    @PostConstruct
    public void init() {
        siTopic().addMessageListener(m ->
                fromHazelcastTopicChannel().send(m.getMessageObject()));

    }

}
