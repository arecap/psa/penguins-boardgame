package org.arecap.game.penguinsboardgame.test.unit.orange;

import org.junit.Test;
import org.springframework.util.Assert;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class TestMiddleFromList {

    private Integer[] list1 = {3, 4, 6, 2, 3, 7};

    private Integer[] list2 = {3, 4, 6, 2, 3};

    private Integer[] list3 = {3};

    private Integer[] list4 = {};

    @Test
    public void getMiddleFromList1Test() {
        AtomicInteger mid = new AtomicInteger(-1);
        AtomicInteger index = new AtomicInteger(0);
        Stream.of(list1).parallel().forEach(e -> {
            if ((index.getAndIncrement() % 2) == 0) {
                mid.incrementAndGet();
            }
        });
        Assert.isTrue(list1[mid.get()] == 6);
    }

    @Test
    public void getMiddleFromList2Test() {
        AtomicInteger mid = new AtomicInteger(-1);
        AtomicInteger index = new AtomicInteger(0);
        Stream.of(list2).parallel().forEach(e -> {
            if ((index.getAndIncrement() % 2) == 0) {
                mid.incrementAndGet();
            }
        });
        Assert.isTrue(list2[mid.get()] == 6);
    }

    @Test
    public void getMiddleFromList3Test() {
        AtomicInteger mid = new AtomicInteger(-1);
        AtomicInteger index = new AtomicInteger(0);
        Stream.of(list3).parallel().forEach(e -> {
            if ((index.getAndIncrement() % 2) == 0) {
                mid.incrementAndGet();
            }
        });
        Assert.isTrue(list3[mid.get()] == 3);
    }

    @Test
    public void getMiddleFromList4Test() {
        AtomicInteger mid = new AtomicInteger(-1);
        AtomicInteger index = new AtomicInteger(0);
        Stream.of(list4).parallel().forEach(e -> {
            if ((index.getAndIncrement() % 2) == 0) {
                mid.incrementAndGet();
            }
        });
        Assert.isTrue(mid.get() == -1);
    }

}