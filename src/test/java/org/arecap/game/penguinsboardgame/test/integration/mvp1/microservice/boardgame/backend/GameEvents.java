package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;

public enum GameEvents {

    PLAY, PAUSE, CONNECT, DISCONNECT

}
