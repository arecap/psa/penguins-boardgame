package org.arecap.game.penguinsboardgame.test.integration.mvp1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
//@EnableWebExchange
public class PenguinsBoardgameApplication {

	public static void main(String[] args) {
		SpringApplication.run(PenguinsBoardgameApplication.class, args);
	}

//	@Bean
//	public BeanUtil getBeanUtil() {
//		return new BeanUtil();
//	}
//
//	@Bean
//	public ContextLinkWrapper getContextLinkWrapper() {
//		return new ContextLinkWrapper();
//	}
//
//	@Bean
//	public LexicalExpressionLink getLexicalExpressionLink() {
//		return new LexicalExpressionLink();
//	}
//
//	@Bean
//	public LexicalPhraseLink getLexicalPhraseLink() {
//		return new LexicalPhraseLink();
//	}
//
//	@Bean
//	public SourceAnnotationLink getSourceAnnotationLink() {
//		return new SourceAnnotationLink();
//	}
//
//	@Bean
//	public SourceClassLink getSourceClassLink() {
//		return new SourceClassLink();
//	}
//
//	@Bean
//	public SourceNameLink getSourceNameLink() {
//		return new SourceNameLink();
//	}
//
//	@Bean
//	public SourcePackageLink getSourcePackageLink() {
//		return new SourcePackageLink();
//	}
//
//	@Bean
//	public SourceTypeLink getSourceTypeLink() {
//		return new SourceTypeLink();
//	}

}
