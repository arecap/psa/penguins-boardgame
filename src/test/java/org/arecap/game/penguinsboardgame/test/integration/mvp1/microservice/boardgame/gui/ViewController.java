package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.gui;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@SpringComponent
@UIScope
public class ViewController extends Div  {

    private static Logger logger = LoggerFactory.getLogger(ViewController.class);

    @PostConstruct
    protected void postConstructPostProcessor() {
        logger.info("post construct");
    }

    @PreDestroy
    protected void preDestroyPostProcessor() {
        logger.info("pre destroy");
    }


}
