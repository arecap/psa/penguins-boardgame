package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.hazelcast;

import com.hazelcast.core.HazelcastInstance;

public final class HazelcastConcurrencyUtil {

    public static boolean hasCpSubsystem(HazelcastInstance instance) {
        return instance.getConfig().getCPSubsystemConfig().getCPMemberCount() != 0;
    }

    public static boolean isCpSubsystemReady(HazelcastInstance instance) {
        return instance.getConfig().getCPSubsystemConfig().getCPMemberCount() <= instance.getCluster().getMembers().size();
    }

}
