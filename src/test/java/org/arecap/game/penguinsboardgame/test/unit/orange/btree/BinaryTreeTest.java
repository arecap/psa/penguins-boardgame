package org.arecap.game.penguinsboardgame.test.unit.orange.btree;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Slf4j
public class BinaryTreeTest {


    @SneakyThrows
    @Test
    public void btTest() {
        BinaryTree bt = constructBinaryTree();
        assertTrue(bt.containsNode(7));
        assertTrue(bt.containsNode(6));
        assertTrue(bt.containsNode(4));

        assertFalse(bt.containsNode(1));

        assertTrue(bt.containsNode(9));
        bt.delete(4);
        print(bt);
        assertFalse(bt.containsNode(4));
        bt.delete(5);
        print(bt);
        bt.delete(3);
        print(bt);
        bt.add(4);
        print(bt);
        bt.add(5);
        print(bt);
        bt.add(3);
        print(bt);
    }

    private static void print(BinaryTree binaryTree) {
        System.out.println();
        System.out.print("In order deep-first search:\t");
        BinaryTree.traverseInOrder(binaryTree.getRoot(), BinaryTreeTest::print);
        System.out.println();
        System.out.print("Pre order deep-first search:\t");
        BinaryTree.traversePreOrder(binaryTree.getRoot(), BinaryTreeTest::print);
        System.out.println();
        System.out.print("Post order deep-first search:\t");
        BinaryTree.traversePostOrder(binaryTree.getRoot(), BinaryTreeTest::print);
        System.out.println();
        System.out.print("Level order breadth-first search:\t");
        BinaryTree.traverseLevelOrder(binaryTree, BinaryTreeTest::print);
        System.out.println();
    }

    private static void print(Node node) {
        System.out.print(" " + node.getValue());
    }

    @SneakyThrows
    private BinaryTree constructBinaryTree() {
        BinaryTree bt = new BinaryTree();
        bt.add(6);
        bt.add(4);
        bt.add(8);
        bt.add(3);
        bt.add(5);
        bt.add(7);
        bt.add(9);
        print(bt);
        return bt;
    }


}
