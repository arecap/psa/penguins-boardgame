package org.arecap.game.penguinsboardgame.test.integration.mvp2.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.SneakyThrows;
import org.arecap.game.penguinsboardgame.test.integration.mvp2.blockchain.BlockchainService;
import org.arecap.game.penguinsboardgame.test.integration.mvp2.blockchain.UuidBlock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;

@SpringComponent
@UIScope
public class PlayerClientUiRequest {

    private static Logger logger = LoggerFactory.getLogger(PlayerClientUiRequest.class);

    private static String topic = "PlayerClientRegistry";

    @Autowired
    private HazelcastInstance hazelcastInstance;

//    @Autowired
//    private TopicEventBus topicEventBus;

    @Autowired
    private BlockchainService<UuidBlock> blockchainService;

    @Autowired
    private ObjectMapper objectMapper;

    private String blockchainNodeId;

    private Consumer<PlayerClient> clientCallback;

//    private Map<String, Consumer<PlayerClient>> serverListeners = new ConcurrentHashMap<>();

    @PostConstruct
    private void postConstructPostProcessor() {
        logger.info("postConstructPostProcessor");
        blockchainNodeId = blockchainService.registerNode(this::newPlayerClientResponse, PlayerClient.entity);

    }

    @PreDestroy
    private void preDestroyPostProcessor() {
        logger.info("preDestroyPostProcessor");
        blockchainService.destroyNode(blockchainNodeId, PlayerClient.entity);
    }

    public void requestNewPlayerClient(Consumer<PlayerClient> listener) {
        clientCallback = listener;
        blockchainService.mineBlock(UUID.randomUUID().toString(), PlayerClient.entity);
    }

    private IMap<String, PlayerClient> getRegistry() {
        return hazelcastInstance.getMap(PlayerClient.entity);
    }

    public boolean isRegistered(PlayerClient playerClient) {
        return isRegistered(playerClient.getUuid());
    }

    public boolean isValid(String playerClientUuid) {
        return blockchainService.valid(playerClientUuid, PlayerClient.entity);
    }

    public boolean isRegistered(String playerClientUuid) {
        return getRegistry().containsKey(playerClientUuid);
    }

    public PlayerClient getPlayerClient(String playerClientUuid) {
        return getRegistry().get(playerClientUuid);
    }

    @SneakyThrows
    public void register(PlayerClient playerClient) {
        logger.info("register playerClient instance:\t" + objectMapper.writeValueAsString(playerClient));
        getRegistry().put(playerClient.getUuid(), playerClient);
    }

    @SneakyThrows
    public void unregister(PlayerClient playerClient) {
        logger.info("unregister playerClient instance:\t" + objectMapper.writeValueAsString(playerClient));
        getRegistry().remove(playerClient.getUuid());
    }

    @SneakyThrows
    private void newPlayerClientResponse(UuidBlock uuidBlock) {
        logger.info("newPlayerClientResponse blockchainService mine UuidBlock:\t" + objectMapper.writeValueAsString(uuidBlock));
        PlayerClient playerClient = constructPlayerClient(uuidBlock.getHash());
        logger.info("new PlayerClient instance:\t" + objectMapper.writeValueAsString(playerClient));
        if(!isRegistered(playerClient)) {
            logger.info("playerClient instance not register");
            register(playerClient);
            if(Optional.ofNullable(clientCallback).isPresent()) {
                clientCallback.accept(playerClient);
            }
        }
//        topicEventBus.publish(topic, new GenericMessage<>(playerClient));
    }


    public PlayerClient constructPlayerClient(String hash) {
        PlayerClient playerClient = new PlayerClient();
        playerClient.setUuid(hash);
        return playerClient;
    }

}
