package org.arecap.game.penguinsboardgame.test.integration.mvp2.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.HazelcastInstance;
import lombok.SneakyThrows;
import org.arecap.game.penguinsboardgame.test.integration.mvp2.messaging.TopicEventBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.function.Consumer;

@Service
public class PlayerClientRegistry extends DirectChannel {

    private static Logger logger = LoggerFactory.getLogger(PlayerClientRegistry.class);

    private static String topic = "PlayerClientRegistry";

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Autowired
    private TopicEventBus topicEventBus;

//    @Autowired
//    private BlockchainService<UuidBlock> blockchainService;
//
    @Autowired
    private ObjectMapper objectMapper;
//
//
//    private String blockchainNodeId;
//
//
//    private Map<String, Consumer<PlayerClient>> serverListeners = new ConcurrentHashMap<>();


    public void registerClient(Consumer<PlayerClient> listener) {

    }

    @PostConstruct
    private void postProcessPostConstruct() {
        logger.info("postProcessPostConstruct");
        this.subscribe(this::postProcessMessage);
        topicEventBus.subscribe(topic, this);
//        blockchainNodeId = blockchainService.registerNode(this::newPlayerClientResponse, PlayerClient.entity);

    }

    @SneakyThrows
    private void postProcessMessage(Message<?> message) {
        logger.info("message:\t" + objectMapper.writeValueAsString(message));
//        if(message.getPayload().getClass().isAssignableFrom(PlayerClient.class)) {
//            serverListeners.values().parallelStream()
//                    .forEach(l -> l.accept((PlayerClient) message.getPayload()));
//        }
    }

    @PreDestroy
    public void postProcessPreDestroy() {
        logger.info("postProcessPreDestroy");
        this.unsubscribe(this::postProcessMessage);
        topicEventBus.unsubscribe(topic);
    }


    public void publishPlayerClient(PlayerClient playerClient) {
        topicEventBus.publish(topic, new GenericMessage<>(playerClient));
    }

//    private IMap<String, PlayerClient> getRegistry() {
//        return hazelcastInstance.getMap(PlayerClient.entity);
//    }
//
//    public boolean isRegistered(PlayerClient playerClient) {
//        return getRegistry().containsValue(playerClient.getUuid());
//    }
//
//    @SneakyThrows
//    public void register(PlayerClient playerClient) {
//        logger.info("register playerClient instance:\t" + objectMapper.writeValueAsString(playerClient));
//        getRegistry().put(playerClient.getUuid(), playerClient);
//    }
//
//    @SneakyThrows
//    public void unregister(PlayerClient playerClient) {
//        logger.info("unregister playerClient instance:\t" + objectMapper.writeValueAsString(playerClient));
//        getRegistry().remove(playerClient.getUuid());
//    }
//
//    public void requestNewPlayerClient() {
//        blockchainService.mineBlock(UUID.randomUUID().toString(), PlayerClient.entity);
//    }
//
//    @SneakyThrows
//    private void newPlayerClientResponse(UuidBlock uuidBlock) {
//        logger.info("newPlayerClientResponse blockchainService mine UuidBlock:\t" + objectMapper.writeValueAsString(uuidBlock));
//        PlayerClient playerClient = new PlayerClient();
//        playerClient.setUuid(uuidBlock.getHash());
//        logger.info("new PlayerClient instance:\t" + objectMapper.writeValueAsString(playerClient));
//        if(!isRegistered(playerClient)) {
//            logger.info("playerClient instance not register");
//            register(playerClient);
//        }
//        topicEventBus.publish(topic, new GenericMessage<>(playerClient));
//    }

}
