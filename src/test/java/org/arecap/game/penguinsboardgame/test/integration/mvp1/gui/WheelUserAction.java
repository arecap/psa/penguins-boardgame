package org.arecap.game.penguinsboardgame.test.integration.mvp1.gui;

import lombok.Getter;
import lombok.Setter;

public class WheelUserAction extends MouseUserAction {

    @Getter @Setter
    private Double deltaX;

    @Getter @Setter
    private Double deltaY;

    @Getter @Setter
    private Double deltaZ;

    @Getter @Setter
    private Integer deltaMode;

}
