package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public final class BoardGameService {

    public static BoardGame constructBoardGame(Integer numberOfPlayers, Game2dGraphicTransformSettings game2dGraphicTransformSettings) {
        BoardGame boardGame = new BoardGame();
        boardGame.setUuid(UUID.randomUUID().toString());
        boardGame.setGameElapsedTimeMs(0L);
        boardGame.setPlayersNumber(numberOfPlayers);
        boardGame.setPenguinsRepartition(new HashMap<>());
        setupPlayers(boardGame, numberOfPlayers);
        setupPenguins(boardGame);
        constructIceBoard(boardGame, game2dGraphicTransformSettings);
        return boardGame;
    }

    public static void updateBoardGame2dGraphicTransformSettings(BoardGame boardGame, Game2dGraphicTransformSettings game2dGraphicTransformSettings) {
        AtomicInteger rootNth = new AtomicInteger(0);
        BoardGameGeometryService.getBoardGameGeometry(game2dGraphicTransformSettings).stream()
                .forEach(geometry
                        -> updateIceGeometry(boardGame, rootNth.getAndIncrement(), geometry));

    }


    public static List<Penguin> getPlayerPenguins(BoardGame boardGame, String playerUuid) {
        return boardGame.getPenguins().values().stream()
                .filter(penguin -> boardGame.getPlayerPenguins().get(playerUuid).contains(penguin.getUuid()))
                .collect(Collectors.toList());
    }

    public static Map<Integer, Penguin> getAvailablePenguins(BoardGame boardGame, Player player) {
        //TODO
        AtomicInteger penguinIndex = new AtomicInteger(0);
        return getPlayerPenguins(boardGame, player.getUuid()).stream()
                .collect(Collectors.toMap(penguin -> penguinIndex.getAndIncrement(), penguin -> penguin));
    }

    public static Integer getPlayerScore(BoardGame boardGame, Player participant) {
        AtomicInteger playerScore = new AtomicInteger(0);
        boardGame.getIceBoard().values().stream()
                .filter(ice -> ice.isVisited())
                .filter(ice -> boardGame.getPlayerPenguins().get(participant.getUuid()).contains(ice.getPenguinUuid()))
                .forEach(ice -> playerScore.addAndGet(ice.getFishValue()));
        return playerScore.get();
    }

    private static void updateIceGeometry(BoardGame boardGame, int rootNth, DoubleWidthRegularHexagonGeometry geometry) {
        boardGame.getIceBoard().get(rootNth).setGeometry(geometry);
    }

    private static void setupPlayers(BoardGame boardGame, Integer numberOfPlayers) {
        boardGame.setPlayers(new HashMap<>());
        for(int i = 0; i < numberOfPlayers; ++i) {
            Player player = PlayerService.constructPlayer(i);
            boardGame.getPlayers().put(player.getUuid(), player);
        }
    }

    private static void setupPenguins(BoardGame boardGame) {
        int playerNumberOfPenguins = boardGame.getPlayersNumber() == 2 ? 4 : (boardGame.getPlayersNumber() == 3 ? 3 : 2);
        boardGame.setPenguins(new HashMap<>());
        boardGame.setPlayerPenguins(new HashMap<>());
        AtomicInteger colorCodePlayerIdx = new AtomicInteger(0);
        boardGame.getPlayers().values().stream()
                .forEach(player -> setupPenguins(boardGame, player, playerNumberOfPenguins));

    }

    private static void setupPenguins(BoardGame boardGame, Player player,  int playerNumberOfPenguins) {
        boardGame.getPlayerPenguins().put(player.getUuid(), new ArrayList<>());
        for(int i = 0; i < playerNumberOfPenguins; ++i) {
            Penguin penguin = new Penguin();
            penguin.setUuid(UUID.randomUUID().toString());
            penguin.setAvatarPath(player.getAvatarPath());
            boardGame.getPenguins().put(penguin.getUuid(), penguin);
            boardGame.getPlayerPenguins().get(player.getUuid()).add(penguin.getUuid());
        }
    }

    private static void constructIceBoard(BoardGame boardGame, Game2dGraphicTransformSettings game2dGraphicTransformSettings) {
        boardGame.setIceBoard(new HashMap<>());
        AtomicInteger rootNth = new AtomicInteger(0);
        AtomicInteger countFish1 = new AtomicInteger(0);
        AtomicInteger countFish2 = new AtomicInteger( 0);
        AtomicInteger countFish3 = new AtomicInteger(0);
        BoardGameGeometryService.getBoardGameGeometry(game2dGraphicTransformSettings).stream()
                .forEach(geometry
                        -> constructIce(boardGame, rootNth.getAndIncrement(), geometry, countFish1, countFish2, countFish3));
    }

    private static void constructIce(BoardGame boardGame, int rootNth, DoubleWidthRegularHexagonGeometry geometry,
                                     AtomicInteger countFish1, AtomicInteger countFish2, AtomicInteger countFish3) {
        int fishValue = ThreadLocalRandom.current().nextInt(1, 4);
        if(fishValue == 1 && countFish1.get() < 30) {
            countFish1.incrementAndGet();
        } else if(fishValue == 1) {
            fishValue = 2;
        }
        if(fishValue == 2 && countFish2.get() < 20) {
            countFish2.incrementAndGet();
        } else if(fishValue == 2){
            if(countFish1.get() < 30) {
                fishValue = 1;
                countFish1.incrementAndGet();
            } else {
                fishValue = 3;
            }
        }
        if(fishValue == 3 && countFish3.get() < 10) {
            countFish3.incrementAndGet();
        } else if(fishValue == 3) {
            if(countFish1.get() < 30) {
                fishValue = 1;
                countFish1.incrementAndGet();
            } else {
                fishValue = 2;
                countFish2.incrementAndGet();
            }
        }
        boardGame.getIceBoard().put(rootNth, constructIce(rootNth, fishValue, geometry));
    }

    private static Ice constructIce(int rootNth, int fishValue, DoubleWidthRegularHexagonGeometry geometry){
        Ice ice = new Ice();
        ice.setUuid(UUID.randomUUID().toString());
        ice.setRootNth(rootNth);
        ice.setFishValue(fishValue);
        ice.setGeometry(geometry);
        return ice;
    }

}
