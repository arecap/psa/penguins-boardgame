package org.arecap.game.penguinsboardgame.test.unit;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import lombok.Getter;
import lombok.Setter;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.BoardGame;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.BoardGameService;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.ComplexPlane;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.Game2dGraphicTransformSettings;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;

public class PenguinSsmBotGameTest {

    private static Logger logger = LoggerFactory.getLogger(PenguinSsmBotGameTest.class);


    @Test
    public void penguinSsmBotGameTest() {
        BoardGameContext boardGameContext = initBoardGameContext(4);
        IntStream.range(0, boardGameContext.getBoardGame().getPlayersNumber()).forEach(playerIndex -> initPlayerClient(playerIndex, boardGameContext));
    }

    private static BoardGameContext initBoardGameContext(int numberOfPlayers) {
        BoardGameContext context = new BoardGameContext();
        context.setBoardGame(BoardGameService.constructBoardGame(4, constructGame2dGraphicTransformSettings()));
        context.setHazelcastConfig(hazelcastConfig());
        context.setGameInstance(Hazelcast.newHazelcastInstance(context.getHazelcastConfig()));
        logger.info("hazelcast game instance member uuid:\t" + context.getGameInstance().getCluster().getLocalMember().getUuid());
//        logger.info("hazelcast game instance CP member uuid:\t" + context.getGameInstance().getCPSubsystem().getLocalCPMember().getUuid());
        return context;
    }

    private static void initPlayerClient(int playerIndex, BoardGameContext context) {
        HazelcastInstance playerClientInstance = Hazelcast.newHazelcastInstance(context.getHazelcastConfig());
        logger.info("hazelcast player client instance name:\t" + playerClientInstance.getName() +
                "\tmember uuid:\t" + playerClientInstance.getCluster().getLocalMember().getUuid());
        logger.info("public ip:\t" + playerClientInstance.getConfig().getNetworkConfig().getPublicAddress());
        for(String playerUuid: context.getBoardGame().getPlayers().keySet()) {
            if(Optional.ofNullable(context.getPlayerClientInstance().get(playerUuid)).isPresent()) {
                logger.info("player uuid:\t" + playerUuid + "\thas player client instance configured. move to next player uuid.");
                continue;
            }
            logger.info("hazelcast player client has been configured for player uuid:\t" + playerUuid);
            context.getPlayerClientInstance().put(playerUuid, playerClientInstance);
            break;
        }
    }


    private static Config hazelcastConfig() {
        Config config = new Config();
        config.getCPSubsystemConfig().setCPMemberCount(3);
        return config;
    }

    private static Game2dGraphicTransformSettings constructGame2dGraphicTransformSettings() {
        Game2dGraphicTransformSettings game2dGraphicTransformSettings = new Game2dGraphicTransformSettings();
        game2dGraphicTransformSettings.setClientBounds(new ComplexPlane(1200d, 900d));
        game2dGraphicTransformSettings.setClientCenter(new ComplexPlane(600D, 450D));
        game2dGraphicTransformSettings.setScale(1D);
        game2dGraphicTransformSettings.setRotationAngle(0D);
        return game2dGraphicTransformSettings;
    }

    @Getter @Setter
    static class BoardGameContext {

        private BoardGame boardGame;

        private Config hazelcastConfig;

        private HazelcastInstance gameInstance;

        private Map<String, HazelcastInstance> playerClientInstance = new HashMap<>();

    }
}
