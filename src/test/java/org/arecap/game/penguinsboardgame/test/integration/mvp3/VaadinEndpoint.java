package org.arecap.game.penguinsboardgame.test.integration.mvp3;


import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import lombok.extern.slf4j.Slf4j;

@Push
@StyleSheet("https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css")
@StyleSheet("/frontend/css/styles.css")
@Viewport("width=device-width, initial-scale=1.0, user-scalable=no")
@Slf4j
public abstract class VaadinEndpoint extends Div implements AfterNavigationObserver {

    @Override
    public void afterNavigation(AfterNavigationEvent afterNavigationEvent) {
        log.info("afterNavigation -- add bootstrap required scripts");
        UI.getCurrent().getPage().addJavaScript("https://code.jquery.com/jquery-3.5.1.slim.min.js");
        UI.getCurrent().getPage().addJavaScript("https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js");
        UI.getCurrent().getPage().addJavaScript("https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js");
    }

}
