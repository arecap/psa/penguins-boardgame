package org.arecap.game.penguinsboardgame.test.unit;

import org.arecap.game.penguinsboardgame.test.integration.mvp1.graphics.Graphics2dUtils;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.graphics.encoder.PngEncoder;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.ComplexPlane;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.DoubleWidthRegularHexagonGeometry;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.Game2dGraphicTransformSettings;
import org.beryx.awt.color.ColorFactory;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class RegularHexagonGeometryTest {

    @Test
    public void drawVerticesRegularHexagonTest() {
        Game2dGraphicTransformSettings game2dGraphicTransformSettings = new Game2dGraphicTransformSettings();
        game2dGraphicTransformSettings.setClientBounds(new ComplexPlane(600d, 600d));
        game2dGraphicTransformSettings.setClientCenter(new ComplexPlane(300D, 300D));
        game2dGraphicTransformSettings.setScale(2D);
        game2dGraphicTransformSettings.setRotationAngle(0D);
        BufferedImage bufferedImage = new BufferedImage(game2dGraphicTransformSettings.getClientBounds().getRealValue().intValue(),
                game2dGraphicTransformSettings.getClientBounds().getImaginaryValue().intValue(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics2D = Graphics2dUtils.constructGraphics2D(bufferedImage,
                game2dGraphicTransformSettings.getClientBounds().getRealValue().intValue(),
                game2dGraphicTransformSettings.getClientBounds().getImaginaryValue().intValue());
        drawContent(graphics2D, game2dGraphicTransformSettings);
        game2dGraphicTransformSettings.setClientCenter(new ComplexPlane(500D, 300D));
        drawContent(graphics2D, game2dGraphicTransformSettings);
        long t = System.currentTimeMillis();
        InputStream is = null;
        try {
            is = new ByteArrayInputStream(toByteArrayAutoClosable(bufferedImage, "PNG"));
            File targetFile = new File("test_regular_hexagon.png");
            Files.copy(is,targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
//            IOUtils.closeQuietly(is);
        }catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Write image processing :\t" + (System.currentTimeMillis() - t) + "\tms");
    }

    private void drawContent(Graphics2D graphics2D, Game2dGraphicTransformSettings game2dGraphicTransformSettings) {
        DoubleWidthRegularHexagonGeometry regularHexagonGeometry = new DoubleWidthRegularHexagonGeometry();
        regularHexagonGeometry.setOrigin(game2dGraphicTransformSettings.getClientCenter());
        regularHexagonGeometry.setInRadius(50D);
        regularHexagonGeometry.setRotationAngle(game2dGraphicTransformSettings.getRotationAngle());
        graphics2D.setColor(ColorFactory.BLUE);
        Graphics2dUtils.drawPoint(graphics2D, regularHexagonGeometry.getOrigin().getRealValue() ,
                regularHexagonGeometry.getOrigin().getImaginaryValue() ,
                1.6D, 1.8D);
        graphics2D.setColor(ColorFactory.RED);
//        Assert.isTrue(regularHexagonGeometry.isInRadiusRange(regularHexagonGeometry.getOrigin()), "Formula!!!");
        for(ComplexPlane vertex: regularHexagonGeometry.getVerticesClockWise(game2dGraphicTransformSettings.getScale())) {
//            Assert.isTrue(!regularHexagonGeometry.isInRadiusRange(vertex), "Formula!!!");
            int i = 0;
            System.out.println("vertex:\t"+ (i++) + " inside circle:\t" + regularHexagonGeometry.isInRadiusRange(vertex));
            Graphics2dUtils.drawPoint(graphics2D, vertex.getRealValue(),
                    vertex.getImaginaryValue(),
                    1.6D, 1.8D);
        }
    }

    private byte[] toByteArrayAutoClosable(BufferedImage image, String type) throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            new PngEncoder().write(image, out);
            return out.toByteArray();
        } catch (Exception e) {
        }
        return null;
    }

}
