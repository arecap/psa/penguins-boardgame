package org.arecap.game.penguinsboardgame.test.integration.mvp3;

import org.springframework.statemachine.StateContext;

import java.util.Optional;

public interface WebClientPlayerTurnSsmProcessor {

    default boolean isPlayerTurn(StateContext<String, String> stateContext) {
        return Optional.ofNullable(stateContext.getExtendedState()
                .getVariables().get(WebClientPlayerSsmService.CURRENT_PLAYER_VARIABLE)).isPresent()
                && stateContext.getExtendedState()
                .getVariables().get(WebClientPlayerSsmService.CURRENT_PLAYER_VARIABLE)
                .equals(stateContext.getExtendedState().getVariables().get(WebClientPlayerSsmService.PLAYER_VARIABLE));
    }

    void onPlayerTurn(StateContext<String, String> stateContext);

}
