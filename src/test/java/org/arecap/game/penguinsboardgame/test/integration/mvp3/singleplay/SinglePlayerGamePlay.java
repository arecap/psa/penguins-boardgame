package org.arecap.game.penguinsboardgame.test.integration.mvp3.singleplay;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.arecap.game.penguinsboardgame.test.integration.mvp3.WebApplicationClientModel;
import org.arecap.game.penguinsboardgame.test.integration.mvp3.WebClientPlayerSsmConfigurationBean;
import org.arecap.game.penguinsboardgame.test.integration.mvp3.WebClientPlayerSsmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.StateMachinePersist;
import org.springframework.statemachine.persist.DefaultStateMachinePersister;
import org.springframework.statemachine.persist.StateMachinePersister;

import javax.annotation.PostConstruct;
import java.util.Optional;

@SpringComponent
@UIScope
@Slf4j
public class SinglePlayerGamePlay implements WebClientSsmSinglePlayerProcessor, StateMachinePersist<String, String, String> {

    @Autowired
    private WebClientPlayerSsmService ssmService;

    @Autowired
    private ObjectMapper objectMapper;

    private StateMachinePersister<String, String, String> gamePlaySsmPersister;

    private StateMachine<String, String> gamePlaySsm;

    private WebApplicationClientModel clientModel;

    private UI ui;

    @PostConstruct
    private void postProcessPostConstruct() {
        gamePlaySsmPersister = new DefaultStateMachinePersister<>(this);
    }

    @SneakyThrows
    public void load(WebApplicationClientModel clientModel, UI ui) {
        log.info("load clientModel:\t" + objectMapper.writeValueAsString(clientModel));
        this.ui = ui;
        this.clientModel = clientModel;
        if(Optional.ofNullable(clientModel.getSsmContext()).isPresent()) {
            constructStateMachine();
            log.info("game play ssm state id:\t" + gamePlaySsm.getState().getId());
            gamePlaySsmPersister.restore(gamePlaySsm, "");
            log.info("game play ssm state id:\t" + gamePlaySsm.getState().getId());
            return;
        }
        startNewGame();
        log.info("game play ssm state id:\t" + gamePlaySsm.getState().getId());
    }

    @SneakyThrows
    private void startNewGame() {
        log.info("start new single play game play ssm");
        constructStateMachine();
        gamePlaySsm.sendEvent("NEXT");
        gamePlaySsmPersister.persist(gamePlaySsm, "");
    }


    private void constructStateMachine() {
        gamePlaySsm = ssmService.getStateMachine(clientModel.getUuid(), WebClientPlayerSsmConfigurationBean.of(this));
        gamePlaySsm.getExtendedState().getVariables().put(WebClientPlayerSsmService.CURRENT_PLAYER_VARIABLE, 1);
        gamePlaySsm.getExtendedState().getVariables().put(WebClientPlayerSsmService.PLAYER_VARIABLE, 1);
        gamePlaySsm.getExtendedState().getVariables().put(WebClientPlayerSsmService.GAME_STAT_VARIABLE, WebClientPlayerSsmService.GAME_STAT_PLAY);
        gamePlaySsm.start();
    }

    @Override
    public boolean isAiPlayerTurn(StateContext<String, String> stateContext) {
        return false;
    }

    @Override
    public void onAiPlayerTurn(StateContext<String, String> stateContext) {

    }

    @Override
    public void onPlayerTurn(StateContext<String, String> stateContext) {

    }


    @Override
    public void write(StateMachineContext<String, String> context, String contextObj) throws Exception {
        log.info("try to save in web client the state machine context");
        clientModel.setSsmContext(context);
        if(ui.isEnabled()) {
            ui.access(() -> {
                try {
                    ui.getPage()
                            .executeJs("PenguinUserModelPresenter.modelSaveSsmContext($0);",
                                    objectMapper.writeValueAsString(context));
                } catch (JsonProcessingException e) {
                    log.error("ssm context not JSON serializable", e.getStackTrace());

                }
            });
        }
    }

    @Override
    public StateMachineContext<String, String> read(String contextObj) throws Exception {
        return clientModel.getSsmContext();
    }
}
