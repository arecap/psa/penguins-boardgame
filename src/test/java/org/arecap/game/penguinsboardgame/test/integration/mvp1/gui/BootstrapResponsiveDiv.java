package org.arecap.game.penguinsboardgame.test.integration.mvp1.gui;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;

@StyleSheet("https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css")
public class BootstrapResponsiveDiv extends Div implements AfterNavigationObserver {

    @Override
    public void afterNavigation(AfterNavigationEvent afterNavigationEvent) {
        UI.getCurrent().getPage().addJavaScript("https://code.jquery.com/jquery-3.5.1.slim.min.js");
        UI.getCurrent().getPage().addJavaScript("https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js");
        UI.getCurrent().getPage().addJavaScript("https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js");
    }

}
