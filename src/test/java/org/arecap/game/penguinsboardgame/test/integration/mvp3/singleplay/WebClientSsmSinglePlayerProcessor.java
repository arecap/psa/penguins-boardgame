package org.arecap.game.penguinsboardgame.test.integration.mvp3.singleplay;

import org.arecap.game.penguinsboardgame.test.integration.mvp3.WebClientSsmFullProcessor;
import org.springframework.statemachine.StateContext;

public interface WebClientSsmSinglePlayerProcessor
        extends WebClientSsmFullProcessor {

    @Override
    default boolean isRemotePlayerTurn(StateContext<String, String> stateContext) {
        return false;
    }

    @Override
    default void onRemotePlayerTurn(StateContext<String, String> stateContext) {
        //N/A
    }
}
