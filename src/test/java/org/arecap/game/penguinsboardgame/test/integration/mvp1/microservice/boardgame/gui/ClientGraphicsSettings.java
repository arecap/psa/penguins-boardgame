package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.gui;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vaadin.flow.component.page.BrowserWindowResizeEvent;
import com.vaadin.flow.component.page.BrowserWindowResizeListener;
import com.vaadin.flow.component.page.ExtendedClientDetails;
import com.vaadin.flow.component.page.Page;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.ComplexPlane;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.Game2dGraphicTransformSettings;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//@InformationExchange
@SpringComponent
@UIScope
@JsonIgnoreProperties({"sub", "iss", "aud", "iat"})
public class ClientGraphicsSettings extends Game2dGraphicTransformSettings implements BrowserWindowResizeListener, Page.ExtendedClientDetailsReceiver {

    private List<GeometryChangeListener> geometryChangeListeners = new ArrayList<>();

    public void addGeometryChangeListener(GeometryChangeListener geometryChangeListener){
        geometryChangeListeners.add(geometryChangeListener);
    }

    public void removeGeometryChangeListener(GeometryChangeListener geometryChangeListener) {
        geometryChangeListeners.remove(geometryChangeListener);
    }

//    @PostConstruct
//    private void postConstruct() {
//        setScale(1.2D);
//        setRotationAngle(0.5D);
//    }

    public void refreshClientGraphicsSettings() {
        geometryChangeListeners.stream()
                .forEach(gcl -> gcl.clientGraphicsUpdate());
    }

    @Override
    public void browserWindowResized(BrowserWindowResizeEvent browserWindowResizeEvent) {
        setClientBounds(new ComplexPlane(Math.min(1200, Double.valueOf(browserWindowResizeEvent.getWidth())),
                Double.valueOf(browserWindowResizeEvent.getHeight())));
        if(!Optional.ofNullable(getClientCenter()).isPresent()) {
            setClientCenter(new ComplexPlane(getClientBounds().getRealValue() / 2D, getClientBounds().getImaginaryValue() / 2D));
        }
        refreshClientGraphicsSettings();
    }

    @Override
    public void receiveDetails(ExtendedClientDetails extendedClientDetails) {
        setClientBounds(new ComplexPlane(Math.min(1200, Double.valueOf(extendedClientDetails.getBodyClientWidth())),
                Double.valueOf(extendedClientDetails.getBodyClientHeight())));
        if(!Optional.ofNullable(getClientCenter()).isPresent()) {
            setClientCenter(new ComplexPlane(getClientBounds().getRealValue() / 2D, getClientBounds().getImaginaryValue() / 2D));
        }
        refreshClientGraphicsSettings();
    }

}
