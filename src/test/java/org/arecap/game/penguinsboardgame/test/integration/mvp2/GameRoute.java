package org.arecap.game.penguinsboardgame.test.integration.mvp2;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.router.*;
import org.arecap.game.penguinsboardgame.test.integration.mvp2.service.PlayerClient;
import org.arecap.game.penguinsboardgame.test.integration.mvp2.service.PlayerClientRegistry;
import org.arecap.game.penguinsboardgame.test.integration.mvp2.service.PlayerClientUiRequest;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.logging.Logger;

@Route("")
@Push
@StyleSheet("https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css")
@StyleSheet("/frontend/css/styles.css")
@Viewport("width=device-width, initial-scale=1.0, user-scalable=no")
public class GameRoute extends Div implements BeforeEnterObserver, AfterNavigationObserver {

    private static Logger logger = Logger.getLogger(GameRoute.class.getCanonicalName());

    @Autowired
    private PlayerClientRegistry playerClientRegistry;

    @Autowired
    private PlayerClientUiRequest playerClientUiRequest;

    private PlayerClient playerClient;

    @PostConstruct
    private void postConstructPostProcessor() {
        logger.info("a new instance of GameRoute available to UIScope context");
        ReconnectDialogConfiguration configuration = UI.getCurrent().getReconnectDialogConfiguration();
        configuration.setReconnectInterval(1000);
        addClassName("content");
        //        sender.addInterceptor(clientMessagingDispatcher);
        //        sender.subscribe(clientMessagingDispatcher);
        //
        //        MessagingTemplate messagingTemplate = new MessagingTemplate();
        //        messagingTemplate.send(publisher,  new GenericMessage<>("client uuid"));
        //        messagingTemplate.receive(hazelcastQueueChannel);
        //        messagingTemplate.sendAndReceive(publisher, new GenericMessage<>("client uuid"));
        //        publisher.send();
        //        if(getUI().isPresent()) {
        //            UI ui = getUI().get();
        //        }
        //        UI.getCurrent().addDetachListener(this);
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        logger.info("component attached to the UI");

    }

    @Override
    protected void onDetach(DetachEvent detachEvent) {
        logger.info("component detached from the UI");
        playerClientUiRequest.unregister(playerClient);
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        logger.info("before enter route instance");
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event) {
        logger.info("after navigation route instance");
        UI.getCurrent().getPage().addJavaScript("https://code.jquery.com/jquery-3.5.1.slim.min.js");
        UI.getCurrent().getPage().addJavaScript("https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js");
        UI.getCurrent().getPage().addJavaScript("https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js");
        UI.getCurrent().getPage().addJavaScript("/frontend/js/PenguinUserMVP.js");
        UI.getCurrent().getPage().addJavaScript("/frontend/js/PenguinRTC.js");
        UI.getCurrent().getPage()
                .executeJs("PenguinUserModelPresenter.mineOrLoadUserModel($0);",
                this.getElement());
    }


    @ClientCallable
    public void mineNewClient() {
        logger.info("route model request for new Player Client");
        playerClientUiRequest.requestNewPlayerClient(this::afterRegisterPlayerClient);
    }

    private void afterRegisterPlayerClient(PlayerClient playerClient) {
        logger.info("afterRegisterPlayerClient");
        playerClientRegistry.publishPlayerClient(playerClient);
        if(getUI().isPresent()) {
            UI ui = getUI().get();
            ui.access(() -> ui.getPage().executeJs("PenguinUserModelPresenter.initModel($0);", playerClient.getUuid()));
        }

    }

    @ClientCallable
    public void loadUserModel(String playerClientUuid) {
        logger.info("model request playerClientUuid:\t" + playerClientUuid);
        if(playerClientUiRequest.isValid(playerClientUuid)) {
            if(!playerClientUiRequest.isRegistered(playerClientUuid)) {
                playerClientUiRequest.register(playerClientUiRequest.constructPlayerClient(playerClientUuid));
            }
            playerClientRegistry.publishPlayerClient(playerClientUiRequest.getPlayerClient(playerClientUuid));
        } else {
            playerClientUiRequest.requestNewPlayerClient(this::afterRegisterPlayerClient);
        }

    }

}