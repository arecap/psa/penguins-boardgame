//package org.arecap.game.penguinsboardgame.microservice.boardgame.gui;
//
//import com.hazelcast.core.*;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//public class DistributedBroadcasterLifecycleListener implements LifecycleListener {
//
//    private static Logger logger = LoggerFactory.getLogger(DistributedBroadcasterLifecycleListener.class);
//
//    private final DistributedBroadcaster dsBroadcaster;
//
//
//    DistributedBroadcasterLifecycleListener(DistributedBroadcaster dsBroadcaster) {
//        this.dsBroadcaster = dsBroadcaster;
//    }
//
//    @Override
//    public void stateChanged(LifecycleEvent event) {
//        logger.info("event:\t" + event);
//    }
//
//}
