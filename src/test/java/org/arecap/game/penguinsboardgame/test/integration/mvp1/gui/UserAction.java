package org.arecap.game.penguinsboardgame.test.integration.mvp1.gui;


import lombok.Getter;
import lombok.Setter;

public class UserAction {

    @Getter @Setter
    private Boolean altKey;

    @Getter @Setter
    private Boolean metaKey;

    @Getter @Setter
    private Boolean ctrlKey;

    @Getter @Setter
    private Boolean shiftKey;

}
