package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.hazelcast;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ITopic;
import com.hazelcast.core.MessageListener;

public interface TopicBroadcaster<T> extends MessageListener<T> {


    String getTopicRegistryRef();

    void setTopicRegistryRef(String topicRegistryRef);

    HazelcastInstance getInstance();

    default void register(String topic) {
        setTopicRegistryRef(getTopic(topic).addMessageListener(this));
    }

    default void destroy(String topic) {
        getTopic(topic).removeMessageListener(getTopicRegistryRef());
    }

    default ITopic<T> getTopic(String topic) {
        return getInstance().getTopic(topic);
    }

    default void broadcast(String topic, T message) {
        ITopic iTopic = getTopic(topic);
        iTopic.publish(message);
    }

}
