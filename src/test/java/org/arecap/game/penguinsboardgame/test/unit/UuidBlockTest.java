package org.arecap.game.penguinsboardgame.test.unit;

import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.UuidBlock;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class UuidBlockTest {

    static Logger logger = LoggerFactory.getLogger(UuidBlockTest.class);

    int prefix = 2;
    String prefixString = new String(new char[prefix]).replace('\0', '0');
    List<UuidBlock> blockchain = getBlockchainStore(1, prefixString);

    @Test
    public void givenBlockchain_whenNewBlockAdded_thenSuccess() {
        assertTrue(blockchain.get(0).getHash().substring(0, prefix).equals(prefixString));
    }


    @Test
    public void givenBlockchain_whenValidated_thenSuccess() {
        Long t = System.currentTimeMillis();
        boolean flag = true;
        blockchain = getBlockchainStore(4, prefixString);
        for (int i = 0; i < blockchain.size(); i++) {
            String previousHash = i==0 ? prefixString : blockchain.get(i - 1).getHash();
            flag = blockchain.get(i).getHash().equals(blockchain.get(i).calculateBlockHash())
                    && previousHash.equals(blockchain.get(i).getPreviousHash())
                    && blockchain.get(i).getHash().substring(0, prefix).equals(prefixString);
            if (!flag) break;
        }
        logger.info("test execution time millis:\t" + (System.currentTimeMillis() - t));
        assertTrue(flag);
    }

    public static List<UuidBlock> getBlockchainStore(int blocksCount, String prefixString) {
        List<UuidBlock> blockchain = new ArrayList();
        for(int i = 0; i < blocksCount; ++i) {
            Long t = System.currentTimeMillis();
            UuidBlock newBlock = new UuidBlock(UUID.randomUUID().toString(), i == 0 ? prefixString : blockchain.get(i - 1).getHash(), t);
            logger.info("mining hash:\t" + newBlock.mineBlock(prefixString.length()) + "\tmining execution time millis:\t" + (System.currentTimeMillis() - t));
            blockchain.add(newBlock);
        }
        return blockchain;
    }

}
