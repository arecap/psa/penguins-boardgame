package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;

import lombok.Getter;
import lombok.Setter;
/*
 *  Clasa {@link Penguin} reprezinta un pion juctor dintr-o tabla de joc {@link BoardGame}
 *  descris print-un POJO.
 *
 *  @author Octavian Stirbei
 *  @author Cristian Claudiu Bercea
 *
 *  @since 1.0.0
 *
 */
@Getter @Setter
public class Penguin {

    /*
     * UUID-ul unui {@link Penguin}
     *
     *
     * {@link Penguin#setUuid}
     * @param uuid Este generat de sistem printr-un seviciu specific de generare UUID
     *
     * @{link Penguin#getUuid}
     * @return uuid-ul setat
     *
     */
    private String uuid;

    /*
     * Calea relativa a resursei imagine a avatarului unui {@link Penguin}
     *
     *
     * {@link Penguin#setAvatarPath}
     * @param avatarPath Calea relativa a resursei imagine a avatarului unui {@link Penguin}
     *
     * @{link Penguin#getAvatarPath}
     * @return avatarPath calea catre resursa imagine setata
     *
     */
    private String avatarPath;


}
