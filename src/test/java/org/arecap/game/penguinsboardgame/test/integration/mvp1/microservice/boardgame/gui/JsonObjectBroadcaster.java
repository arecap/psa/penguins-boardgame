package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.gui;

import com.vaadin.flow.shared.Registration;
import elemental.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

@Service
public class JsonObjectBroadcaster {

    private static Logger logger = LoggerFactory.getLogger(JsonObjectBroadcaster.class);

    private Map<String, Consumer<JsonObject>> listeners = new ConcurrentHashMap<>();

    public Registration register(String uuid, Consumer<JsonObject> callback) {
        listeners.put(uuid, callback);
        return () -> listeners.remove(uuid);
    }

    @Async
    public void broadcast(String uuid, JsonObject message) {
        listeners.keySet().parallelStream().filter(key -> !key.equals(uuid))
                .forEach(key -> listeners.get(key).accept(message));
    }

}
