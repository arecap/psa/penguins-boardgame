package org.arecap.game.penguinsboardgame.test.integration.mvp3;

import elemental.json.JsonNull;
import elemental.json.JsonObject;
import elemental.json.JsonValue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.statemachine.ExtendedState;
import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.state.ObjectState;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.support.DefaultExtendedState;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

@Component
@Slf4j
public class JsonObjectToWebApplicationClientModel implements Converter<JsonObject, WebApplicationClientModel> {

    @Override
    public WebApplicationClientModel convert(JsonObject jsonObject) {
        log.info("convert web JSON:\t" + jsonObject.toJson() + "\t WebApplicationClientModel.class instance");
        WebApplicationClientModel clientModel = new WebApplicationClientModel();
        clientModel.setUuid(jsonObject.getString("clientUuid"));
        Optional<StateMachineContext<String, String>> ssmContext = getSsmContext(jsonObject);
        if(ssmContext.isPresent()) {
            clientModel.setSsmContext(ssmContext.get());
        }
        return clientModel;
    }

    private Optional<StateMachineContext<String, String>> getSsmContext(JsonObject jsonObject) {
        String key = "ssmContext";
        if(!jsonObject.hasKey(key) || JsonNull.class.isAssignableFrom(jsonObject.get(key).getClass())) {
            return Optional.empty();
        }
        try {
            return Optional.ofNullable(buildStateMachineContext(jsonObject.getObject(key)));
        }catch (Throwable t) {
            log.error("fail to convert smmContext!", t.getStackTrace());
        }
        return Optional.empty();
    }

    private StateMachineContext<String, String> buildStateMachineContext(JsonObject jsonObject) {
        log.info("ssmContext:\t" + jsonObject.toJson());
        return new DefaultStateMachineContext<String, String>(new ArrayList<StateMachineContext<String, String>>(), jsonObject.getString("state"),
                null, null, builtExtendedState(jsonObject), null, jsonObject.getString("id"));
    }

    private State<String,String> buildState(JsonObject jsonObject) {
        if(jsonObject.hasKey("state")
                && !JsonNull.class.isAssignableFrom(jsonObject.get("state").getClass())) {
            return new ObjectState<>(jsonObject.getString("state"));
        }
        return null;
    }

    private ExtendedState builtExtendedState(JsonObject jsonObject) {
        ExtendedState extendedState = new DefaultExtendedState();
        if(jsonObject.hasKey("extendedState")
                && !JsonNull.class.isAssignableFrom(jsonObject.get("extendedState").getClass())) {
            JsonObject extendedStateJson = jsonObject.getObject("extendedState");
            if(extendedStateJson.hasKey("variables")
                    && !JsonNull.class.isAssignableFrom(extendedStateJson.get("variables").getClass())) {
                JsonObject variablesJson = extendedStateJson.getObject("variables");
                Arrays.stream(variablesJson.keys()).parallel().forEach(keyVariable -> {
                    extendedState.getVariables().put(keyVariable, valueFromJson(variablesJson.get(keyVariable)));
                });
            }
        }
        return extendedState;
    }

    private Object valueFromJson(JsonValue jsonValue) {
        switch (jsonValue.getType()) {
            case NUMBER:
                return jsonValue.asNumber();
            case STRING:
                return jsonValue.asString();
            default:
                return null;
        }
    }


}
