package org.arecap.game.penguinsboardgame.test.integration.mvp2.blockchain;

import java.util.function.Consumer;

public interface BlockchainService<T> {

    void mineBlock(String data, String entity);

    boolean valid(String hash, String entity);

    String registerNode(Consumer<T> listener, String entity);

    boolean destroyNode(String nodeRegistrationId, String entity);

}
