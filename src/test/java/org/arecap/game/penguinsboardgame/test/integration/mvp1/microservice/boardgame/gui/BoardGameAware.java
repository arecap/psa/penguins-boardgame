package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.gui;

import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.BoardGame;

public interface BoardGameAware {

    void setBoardGame(BoardGame boardGame);

}
