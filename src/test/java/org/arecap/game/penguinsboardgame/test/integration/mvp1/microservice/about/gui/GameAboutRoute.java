package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.about.gui;

import com.vaadin.flow.router.Route;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.gui.GameResponsiveLayout;

@Route("game-about")
public class GameAboutRoute extends GameResponsiveLayout {
}
