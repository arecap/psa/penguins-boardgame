package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;

import org.springframework.statemachine.StateMachine;

public final class PlayerBot {


    public static void playTurn(StateMachine<GameStates, GameEvents> gameSsm, BoardGame boardGame, Player player, PlayerBotAiLevel playerBotAiLevel) {
        switch (playerBotAiLevel) {
            case EASY:
                playTurnEasy(gameSsm, boardGame, player);
                break;
            case MEDIUM:
                playTurnMedium(gameSsm, boardGame, player);
                break;
            case ADVANCE:
                playTurnAdvance(gameSsm, boardGame, player);
                break;
        }
    }

    public static void playTurnEasy(StateMachine<GameStates, GameEvents> gameSsm, BoardGame boardGame, Player player) {

    }

    public static void playTurnMedium(StateMachine<GameStates, GameEvents> gameSsm, BoardGame boardGame, Player player) {

    }

    public static void playTurnAdvance(StateMachine<GameStates, GameEvents> gameSsm, BoardGame boardGame, Player player) {

    }

    public enum PlayerBotAiLevel {
        EASY, MEDIUM, ADVANCE
    }

}
