package org.arecap.game.penguinsboardgame.test.integration.mvp3;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.guard.Guard;

@Getter @Setter @NoArgsConstructor
public class WebClientPlayerSsmConfigurationBean {

    private Guard<String, String> switchPlayerTurn;

    private Action<String, String> onPlayerTurn;

    private Guard<String, String> switchAiTurn;

    private Action<String, String> onAiTurn;

    private Guard<String, String> switchRemoteTurn;

    private Action<String, String> onRemoteTurn;

    public static WebClientPlayerSsmConfigurationBean of(WebClientSsmFullProcessor ssmProcessor) {
        WebClientPlayerSsmConfigurationBean ssmConfiguration = new WebClientPlayerSsmConfigurationBean();
        ssmConfiguration.setSwitchPlayerTurn(ssmProcessor::isPlayerTurn);
        ssmConfiguration.setOnPlayerTurn(ssmProcessor::onPlayerTurn);
        ssmConfiguration.setSwitchAiTurn(ssmProcessor::isAiPlayerTurn);
        ssmConfiguration.setOnAiTurn(ssmProcessor::onAiPlayerTurn);
        ssmConfiguration.setSwitchRemoteTurn(ssmProcessor::isRemotePlayerTurn);
        ssmConfiguration.setOnRemoteTurn(ssmProcessor::isRemotePlayerTurn);
        return ssmConfiguration;
    }

}
