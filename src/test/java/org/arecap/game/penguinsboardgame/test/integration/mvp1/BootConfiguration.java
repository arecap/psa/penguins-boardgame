package org.arecap.game.penguinsboardgame.test.integration.mvp1;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.messaging.PollableChannel;

@Configuration
@EnableIntegration
public class BootConfiguration {


    @Bean
    public Config config() {
        Config config = new Config();
        config.getCPSubsystemConfig().setCPMemberCount(3);
        return config;
    }

    @Bean
    @Primary
    public HazelcastInstance hazelcastInstance() {
        return Hazelcast.newHazelcastInstance(config());
    }

    @Bean
    public HazelcastInstance dummyHazelcastInstanceCpSubsystem1() {
        return Hazelcast.newHazelcastInstance(config());
    }

    @Bean
    public HazelcastInstance dummyHazelcastInstanceCpSubsystem2() {
        return Hazelcast.newHazelcastInstance(config());
    }

    @Bean
    public PollableChannel hazelcastQueueChannel() {
        return new QueueChannel(hazelcastInstance().getQueue("siQueue"));
    }

}
