package org.arecap.game.penguinsboardgame.test.unit;

import org.arecap.game.penguinsboardgame.test.integration.mvp1.graphics.Graphics2dUtils;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.graphics.encoder.PngEncoder;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.ComplexPlane;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.DoubleWidthRegularHexagonGeometry;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.RotationMatrix;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

public class DoubleWidthRegularHexagonGeometryProjectionTest {

    @Test
    public void testDoubleWidthRegularHexagonGeometryProjection(){
        BufferedImage bufferedImage = new BufferedImage(1200,
                900, BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics2D = Graphics2dUtils.constructGraphics2D(bufferedImage,
                1200,
                900);
        DoubleWidthRegularHexagonGeometry geometry = new DoubleWidthRegularHexagonGeometry();
        geometry.setInRadius(60D);
        geometry.setRotationAngle(0D);
        geometry.setOrigin(new ComplexPlane(600D - geometry.getWidth(), 450D));
        graphics2D.setColor(Color.GREEN);
        Graphics2dUtils.drawPoint(graphics2D, geometry.getOrigin().getRealValue(), geometry.getOrigin().getImaginaryValue(), 2D, 2D);
        graphics2D.setColor(Color.BLUE);
        drawInscribedCircle(graphics2D, geometry);
        graphics2D.setColor(Color.PINK);
        drawInscribedRotateEllipse(graphics2D, geometry);
        graphics2D.setColor(Color.RED);
        drawCircumscribedCircle(graphics2D, geometry);
        graphics2D.setColor(Color.CYAN);
        drawCircumscribedRotateEllipse(graphics2D, geometry);
        graphics2D.setColor(Color.MAGENTA);
        drawVertices(graphics2D, geometry);
        graphics2D.setColor(Color.ORANGE);
        drawRect(graphics2D, geometry);
        ComplexPlane translatedLeft = new ComplexPlane(geometry.getOrigin().getRealValue() + geometry.getWidth(), geometry.getOrigin().getImaginaryValue());
//        translatedLeft = RotationMatrix.getComplexPlaneWithRotationAngle(translatedLeft, -geometry.getRotationAngle());
        ComplexPlane circumPlane = geometry.getRotationEllipseCircumRadiusPlane();
        translatedLeft = new ComplexPlane(translatedLeft.getRealValue(),
                translatedLeft.getImaginaryValue() + circumPlane.getImaginaryValue() * 2 * Math.sin(Math.toRadians(geometry.getRotationAngle())) );
        geometry.setOrigin(translatedLeft);
        graphics2D.setColor(Color.GREEN);
        Graphics2dUtils.drawPoint(graphics2D, geometry.getOrigin().getRealValue(), geometry.getOrigin().getImaginaryValue(), 2D, 2D);
//        graphics2D.setColor(Color.BLUE);
//        drawInscribedCircle(graphics2D, geometry);
//        graphics2D.setColor(Color.PINK);
//        drawInscribedRotateEllipse(graphics2D, geometry);
//        graphics2D.setColor(Color.RED);
//        drawCircumscribedCircle(graphics2D, geometry);
//        graphics2D.setColor(Color.CYAN);
//        drawCircumscribedRotateEllipse(graphics2D, geometry);
        graphics2D.setColor(Color.MAGENTA);
        drawVertices(graphics2D, geometry);
        graphics2D.setColor(Color.ORANGE);
        drawRect(graphics2D, geometry);
        diskWriteImage(bufferedImage, "text_regular_hexagon_projection.png");
    }


    private void drawRect(Graphics2D graphics2D, DoubleWidthRegularHexagonGeometry geometry) {
        Double w = geometry.getWidth(), h = geometry.getHeight();
        graphics2D.draw(new Rectangle2D.Double(geometry.getOrigin().getRealValue() - w / 2D, geometry.getOrigin().getImaginaryValue() - h / 2D, w, h));
    }

    private void drawCircumscribedRotateEllipse(Graphics2D graphics2D, DoubleWidthRegularHexagonGeometry geometry) {
        ComplexPlane rotationPlane = geometry.getRotationEllipseCircumRadiusPlane();
        graphics2D.draw(new Ellipse2D.Double(geometry.getOrigin().getRealValue() - rotationPlane.getRealValue(),
                geometry.getOrigin().getImaginaryValue() - rotationPlane.getImaginaryValue(),
                2D * rotationPlane.getRealValue(),
                2D * rotationPlane.getImaginaryValue()));
    }

    private void drawInscribedRotateEllipse(Graphics2D graphics2D, DoubleWidthRegularHexagonGeometry geometry) {
        ComplexPlane rotationPlane = geometry.getRotationEllipseInRadiusPlane();
        graphics2D.draw(new Ellipse2D.Double(geometry.getOrigin().getRealValue() - rotationPlane.getRealValue(),
                geometry.getOrigin().getImaginaryValue() - rotationPlane.getImaginaryValue(),
                2D * rotationPlane.getRealValue(),
                2D * rotationPlane.getImaginaryValue()));
    }

    private void drawVertices(Graphics2D graphics2D, DoubleWidthRegularHexagonGeometry geometry) {
        List<Line2D> sides = new ArrayList<>();
        List<ComplexPlane> vertices = geometry.getVerticesClockWise(1D);
        for(int i = 0; i < vertices.size(); ++i) {
            int j = i == vertices.size() - 1 ? 0 : i + 1;
            ComplexPlane vertexI = RotationMatrix.getComplexPlaneWithRotationAngle(vertices.get(i), 10D);
            ComplexPlane vertexJ = RotationMatrix.getComplexPlaneWithRotationAngle(vertices.get(j), 10D);
            Line2D side = new Line2D.Double(vertexI.getRealValue(), vertexI.getImaginaryValue(),
                    vertexJ.getRealValue(), vertexJ.getImaginaryValue());
            sides.add(side);
        }
        graphics2D.setStroke(new BasicStroke(2));
        sides.stream().forEach(side -> graphics2D.draw(side));
    }

    private void drawCircumscribedCircle(Graphics2D graphics2D, DoubleWidthRegularHexagonGeometry geometry) {
        Double circumRadius = geometry.getCircumRadius();
        graphics2D.draw(new Ellipse2D.Double(geometry.getOrigin().getRealValue() - circumRadius,
                geometry.getOrigin().getImaginaryValue() - circumRadius,
                2D * circumRadius, 2D * circumRadius));

    }

    private void drawInscribedCircle(Graphics2D graphics2D, DoubleWidthRegularHexagonGeometry geometry) {
        graphics2D.draw(new Ellipse2D.Double(geometry.getOrigin().getRealValue() - geometry.getInRadius(),
                geometry.getOrigin().getImaginaryValue() - geometry.getInRadius(),
                2D * geometry.getInRadius(), 2D * geometry.getInRadius()));
    }

    private void diskWriteImage(BufferedImage bufferedImage, String imageFilePath) {
        InputStream is = null;
        try {
            is = new ByteArrayInputStream(toByteArrayAutoClosable(bufferedImage, "PNG"));
            File targetFile = new File(imageFilePath);
            Files.copy(is,targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            //            IOUtils.closeQuietly(is);
        }catch (Exception e) {
            e.printStackTrace();
        }

    }
    private byte[] toByteArrayAutoClosable(BufferedImage image, String type) throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            new PngEncoder().write(image, out);
            return out.toByteArray();
        } catch (Exception e) {
        }
        return null;
    }



}
