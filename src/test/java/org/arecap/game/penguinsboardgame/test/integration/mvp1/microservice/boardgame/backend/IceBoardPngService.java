package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;

import org.arecap.game.penguinsboardgame.test.integration.mvp1.graphics.Graphics2dUtils;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class IceBoardPngService extends BoardGamePngService {

    public IceBoardPngService(BoardGame boardGame) {
        super(boardGame);
    }

    @Override
    protected synchronized void drawIce(Graphics2D graphics2D, Game2dGraphicTransformSettings game2dGraphicTransformSettings, Ice ice) {
//        graphics2D.setColor(ColorFactory.BLUE);
//        Graphics2dUtils.drawPoint(graphics2D, ice.getGeometry().getOrigin().getRealValue(),
//                ice.getGeometry().getOrigin().getImaginaryValue() ,
//                1.6D, 1.8D);
//        graphics2D.setColor(ColorFactory.RED);
//        for (ComplexPlane vertex : ice.getGeometry().getVerticesClockWise(game2dGraphicTransformSettings.getScale())) {
//            Graphics2dUtils.drawPoint(graphics2D, vertex.getRealValue(),
//                    vertex.getImaginaryValue(), 1.6D, 1.8D);
//        }
        List<Line2D> sides = new ArrayList<>();
        Path2D hexagon = new Path2D.Double();
        List<ComplexPlane> vertices = ice.getGeometry().getVerticesClockWise(game2dGraphicTransformSettings.getScale());
        for(int i = 0; i < vertices.size(); ++i) {
            int j = i == vertices.size() - 1 ? 0 : i + 1;
            ComplexPlane vertexI = RotationMatrix.getComplexPlaneWithRotationAngle(vertices.get(i), game2dGraphicTransformSettings.getRotationAngle());
            ComplexPlane vertexJ = RotationMatrix.getComplexPlaneWithRotationAngle(vertices.get(j), game2dGraphicTransformSettings.getRotationAngle());
            Line2D side = new Line2D.Double(vertexI.getRealValue(), vertexI.getImaginaryValue(),
                    vertexJ.getRealValue(), vertexJ.getImaginaryValue());
            sides.add(side);
            hexagon.append(side, true);
        }
        try {
            TexturePaint paint = new TexturePaint(Graphics2dUtils.resizeImage(Graphics2dUtils.getBufferedImage("static/frontend/images/icetexture3.png"), 1D),
                    new Rectangle(0, 0,
                            game2dGraphicTransformSettings.getClientBounds().getRealValue().intValue(),
                            game2dGraphicTransformSettings.getClientBounds().getImaginaryValue().intValue()));
//            graphics2D.setColor(ColorFactory.ALICEBLUE);
            graphics2D.setPaint(paint);
            graphics2D.fill(hexagon);
        } catch (IOException e) {
            e.printStackTrace();
        }
        graphics2D.setColor(Color.DARK_GRAY);
        graphics2D.setStroke(new BasicStroke(2));
        sides.stream().forEach(side -> graphics2D.draw(side));

    }

    @Override
    public void drawContent(Graphics2D graphics2D, Game2dGraphicTransformSettings game2dGraphicTransformSettings) {
        getBoardGame().getIceBoard().values().parallelStream()
                .forEach(ice -> drawIce(graphics2D, game2dGraphicTransformSettings, ice));
    }

}
