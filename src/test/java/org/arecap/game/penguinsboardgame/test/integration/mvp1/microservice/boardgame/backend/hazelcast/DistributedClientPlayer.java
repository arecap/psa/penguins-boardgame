package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.hazelcast;

import com.hazelcast.core.Message;
import com.hazelcast.core.MessageListener;

public class DistributedClientPlayer implements MessageListener<EventPayload<String>>  {

    private static String INTERNAL_BROADCAST_TOPIC = "INTERNAL_BROADCAST_TOPIC";

    @Override
    public void onMessage(Message<EventPayload<String>> message) {

    }
}
