package org.arecap.game.penguinsboardgame.test.integration.mvp1.gui;

import elemental.json.JsonObject;
import org.springframework.core.convert.converter.Converter;

public class JsonObjectMouseUserActionConversionService implements Converter<JsonObject, MouseUserAction> {

    @Override
    public MouseUserAction convert(JsonObject eventData) {
        MouseUserAction mouseUserAction = new MouseUserAction();
        JsonObjectUserActionConverterUtil.setMouseUserAction(mouseUserAction, eventData);
        return mouseUserAction;
    }


}
