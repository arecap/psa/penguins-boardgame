package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.gui;

import com.vaadin.flow.component.html.*;
import com.vaadin.flow.router.RouterLink;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.about.gui.GameAboutRoute;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.gui.MainBoardGameRoute;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.stats.gui.GameStatsRoute;

import java.util.stream.Stream;

public class GameTopNavView extends Nav {

    private NativeButton navToggleButton = new NativeButton();

    private RouterLink routerGameAboutLink = new RouterLink("About", GameAboutRoute.class);

    private ListItem gameAboutRouteNavigationItem = new ListItem(routerGameAboutLink);

    private RouterLink routerGameStatsLink = new RouterLink("Stats", GameStatsRoute.class);

    private ListItem gameStatsRouteNavigationItem = new ListItem(routerGameStatsLink);

    private RouterLink routerHomeLink = new RouterLink("Home", MainBoardGameRoute.class);

    private ListItem homeRouteNavigationItem = new ListItem(routerHomeLink);

    private UnorderedList navNavigationContainer = new UnorderedList(homeRouteNavigationItem,
            gameStatsRouteNavigationItem, gameAboutRouteNavigationItem);

    private Div navCollapsableContent = new Div(navNavigationContainer);

    public GameTopNavView(Class<?> gameResponsiveLayoutType) {
        add(navToggleButton, navCollapsableContent);
        getStyle().set("z-index", "1000");
        addClassNames("navbar", "navbar-expand-lg", "navbar-light", "bg-light");
        Span navToggleButtonIcon = new Span();
        navToggleButtonIcon.addClassNames("navbar-toggler-icon", "navbar-toggler-icon-custom");
        navToggleButton.add(navToggleButtonIcon);
        navToggleButton.addClassNames("navbar-toggler", "navbar-toggler-custom");
        navToggleButton.getElement().setAttribute("type", "button");
        navToggleButton.getElement().setAttribute("data-toggle", "collapse");
        navToggleButton.getElement().setAttribute("data-target", "#gameNavToggle");
        navToggleButton.getElement().setAttribute("aria-controls", "gameNavToggle");
        navToggleButton.getElement().setAttribute("aria-expanded", "false");
        navToggleButton.getElement().setAttribute("aria-label", "Toggle Game Navigation");
        navCollapsableContent.setId("gameNavToggle");
        navCollapsableContent.addClassNames("collapse", "navbar-collapse");
        navNavigationContainer.addClassNames("navbar-nav", "mr-auto", "mt-2", "mt-lg-0");
        setupRouteNavListItem(homeRouteNavigationItem, MainBoardGameRoute.class.isAssignableFrom(gameResponsiveLayoutType));
        setupRouteNavListItem(gameStatsRouteNavigationItem, GameStatsRoute.class.isAssignableFrom(gameResponsiveLayoutType));
        setupRouteNavListItem(gameAboutRouteNavigationItem, GameAboutRoute.class.isAssignableFrom(gameResponsiveLayoutType));
        setupRouteLinks(routerHomeLink, routerGameStatsLink, routerGameAboutLink);
    }

    private void setupRouteNavListItem(ListItem navigationItem, Boolean active) {
        navigationItem.addClassName("nav-item");
        if(active)
            navigationItem.addClassName("active");
    }

    private void setupRouteLinks(RouterLink... routerLinks) {
        Stream.of(routerLinks).forEach(routerLink -> {
//            routerLink.getElement().addEventListener("click", (DomEventListener) domEvent -> {
//                UI.getCurrent().getPage().reload();
//            });
//            routerLink.afterNavigation(new AfterNavigationEvent());
            routerLink.addClassName("nav-link");
        });
    }

}
