package org.arecap.game.penguinsboardgame.test.unit.orange;

import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;


public class SimpleHashMapTest {

    @Test
    public void testBinaryTreeHashMap() {
        Map<String, Integer> javaHasMap = new HashMap<>();
        javaHasMap.put("1", 1);
        javaHasMap.put("-4", -4);
        javaHasMap.put("9", 9);
        javaHasMap.put("4", 4);
        javaHasMap.put("-3", -3);
        javaHasMap.put("6", 6);
        javaHasMap.put("7", 7);
        javaHasMap.put("7023", 7023);
        javaHasMap.put("-5", -5);

        Map<String, Integer> bthm = new SimpleHashMap<>();
        Assert.isTrue(bthm.put("1", 1) == null);
        Assert.isTrue(bthm.put("-4", -4) == null);
        Assert.isTrue(bthm.put("9", 9) == null);
        Assert.isTrue(bthm.put("4", 4) == null);
        Assert.isTrue(bthm.put("-3", -3) == null);
        Assert.isTrue(bthm.put("6", 6) == null);
        Assert.isTrue(bthm.put("7", 7) == null);
        Assert.isTrue(bthm.put("7023", 7023) == null);
        Assert.isTrue(bthm.put("-5", -5)== null);

        Assert.isTrue(bthm.put("9", 10) == 9);
        Assert.isTrue(bthm.put("4", 4) == 4);
        Assert.isTrue(bthm.put("-3", -13)== -3);
        Assert.isTrue(bthm.put("6", 16) == 6);


        Assert.isTrue(bthm.put("90", 90)== null);
        Assert.isTrue(bthm.put("40", 40)== null);
        Assert.isTrue(bthm.put("-30", -30)== null);
        Assert.isTrue(bthm.put("60", 60)== null);


        Assert.isTrue(bthm.put("9", 9) == 10);
        Assert.isTrue(bthm.put("4", 4) == 4);
        Assert.isTrue(bthm.put("-3", -3)== -13);
        Assert.isTrue(bthm.put("6", 6) == 16);

    }
}
