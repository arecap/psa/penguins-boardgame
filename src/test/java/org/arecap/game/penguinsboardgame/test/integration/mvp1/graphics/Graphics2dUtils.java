package org.arecap.game.penguinsboardgame.test.integration.mvp1.graphics;

import org.springframework.core.io.ClassPathResource;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

public final class Graphics2dUtils {


    public static BufferedImage getBufferedImage(String path) throws IOException {
        return ImageIO.read(new ClassPathResource(path).getFile());
    }

    public static Graphics2D constructGraphics2D(BufferedImage bufferedImage, int width, int height) {
        Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.setBackground(new Color(0, 0, 0, 0));
        graphics2D.clearRect(0, 0, width, height);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        return graphics2D;
    }

    public static Dimension getScaledDimension(Dimension imgSize, Dimension boundary) {
        int original_width = imgSize.width;
        int original_height = imgSize.height;
        int bound_width = boundary.width;
        int bound_height = boundary.height;
        int new_width = 0;
        int new_height = 0;

        if (original_width > original_height) {
            new_width = bound_width;
            new_height = (new_width*original_height)/original_width;
        } else {
            new_height = bound_height;
            new_width = (new_height*original_width)/original_height;
        }

        return new Dimension(new_width, new_height);
    }

    public static BufferedImage resizeImage(BufferedImage source, Double scale) {
        Dimension newDimension = getScaledDimension(new Dimension(source.getWidth(), source.getHeight()),
                new Dimension(  (int) Math.round(source.getWidth() * scale), (int) Math.round(source.getHeight() * scale)));
        BufferedImage resizedImage = new BufferedImage((int) newDimension.getWidth(), (int) newDimension.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(source, 0, 0, (int) newDimension.getWidth(), (int) newDimension.getHeight(), null);
        g.dispose();

        return  resizedImage;
    }

    public static void drawPoint(Graphics2D graphics2D, Double x, Double y, Double penStroke, double resolution) {
        double radius = resolution * penStroke;
        graphics2D.fill(new Ellipse2D.Double(x - radius, y - radius, 2.0 * radius, 2.0 * radius));
    }

    public static void drawLine(Graphics2D graphics2D, Double fromX, Double fromY, Double toX, Double toY) {
        graphics2D.draw(new Line2D.Double(fromX, fromY, toX, toY));
    }

    public static void drawStringInCenter(Graphics2D graphics2D, String s, Double x, Double y) {
        FontMetrics fontMetrics = graphics2D.getFontMetrics();
        graphics2D.drawString(s, x.floatValue() - fontMetrics.stringWidth(s) / 2,
                y.floatValue() - fontMetrics.getHeight() / 2 + fontMetrics.getAscent());
    }

    public static void drawString(Graphics2D graphics2D, String s, Double x, Double y) {
        graphics2D.drawString(s, x.floatValue(), y.floatValue() );
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

}
