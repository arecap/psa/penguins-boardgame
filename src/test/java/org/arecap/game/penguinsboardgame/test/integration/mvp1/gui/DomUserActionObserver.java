package org.arecap.game.penguinsboardgame.test.integration.mvp1.gui;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.html.Div;

import java.util.List;
import java.util.Map;

public interface DomUserActionObserver {

    void userTouchStartAction(Map<Integer, TouchUserAction> touches,
                              Map<Integer, TouchUserAction> targetTouches,
                              Map<Integer, TouchUserAction> changedTouches);

    void userTouchEndAction(List<Integer> touches, List<Integer> targetTouches, List<Integer> changedTouches);

    void userTouchCancelAction(List<Integer> touches, List<Integer> targetTouches, List<Integer> changedTouches);

    void userTouchMoveAction(Map<Integer, TouchUserAction> touches, Map<Integer, TouchUserAction> targetTouches, Map<Integer, TouchUserAction> changedTouches);

    void userWheelAction(WheelUserAction wheelUserAction);

    void userMouseLeaveAction(MouseUserAction mouseUserAction);

    void userMouseEnterAction(MouseUserAction mouseUserAction);

    void userContextMenuAction(MouseUserAction mouseUserAction);

    void userMouseDownMoveAction(MouseUserAction mouseUserAction);

    void userMouseClickAction(ClickEvent<Div> divClickEvent);

}
