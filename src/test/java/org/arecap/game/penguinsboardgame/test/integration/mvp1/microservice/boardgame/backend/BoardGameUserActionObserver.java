package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;

public interface BoardGameUserActionObserver {

    void scale(Double scale);

    void pan(ComplexPlane pan);

    void rotate(Double angle);

    void selectIce(Ice ice);

    void selectPenguin(Penguin penguin);

    void moveTo(Ice ice);

}
