package org.arecap.game.penguinsboardgame.test.integration.mvp3;

import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineBuilder;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringComponent
@UIScope
@Slf4j
public class WebClientPlayerSsmService {

    public final static String GAME_PLAY_STATE = "GAME_PLAY";

    public final static String PLAYER_TURN_STATE = "PLAYER_TURN";

    public final static String AI_TURN_STATE = "AI_TURN";

    public final static String WAIT_TURN_STATE = "WAIT_TURN";

    public final static String GAME_OVER_STATE = "GAME_OVER";

    public final static String NEXT_EVENT = "NEXT";

    public final static String STOP_EVENT = "STOP";

    public final static String CURRENT_PLAYER_VARIABLE = "CURRENT_PLAYER";

    public final static String PLAYER_VARIABLE = "PLAYER";

    public final static String GAME_STAT_VARIABLE = "GAME_STAT";

    public final static String GAME_STAT_PLAY = "PLAY";

    public final static String GAME_STAT_END = "END";


    @PostConstruct
    public void postProcessorPostConstruct() {
        log.info("postProcessorPostConstruct");
    }

    @PreDestroy
    public void postProcessorPreDestroy() {
        log.info("postProcessorPreDestroy");
    }


    @SneakyThrows
    public StateMachine<String, String> getStateMachine(String uuid, WebClientPlayerSsmConfigurationBean configurationBean) {
        //todo
        StateMachineBuilder.Builder<String, String> builder = StateMachineBuilder.builder();
        //visitor pattern for builder configuration
        configureConfiguration(uuid, builder);
        configureStates(builder);
        configureTransitions(builder, configurationBean);
        return builder.build();
    }

    @SneakyThrows
    private void configureTransitions(StateMachineBuilder.Builder<String, String> builder, WebClientPlayerSsmConfigurationBean configurationBean) {
        builder.configureTransitions()
                .withExternal().source(GAME_PLAY_STATE).target(PLAYER_TURN_STATE)
                .guard(configurationBean.getSwitchPlayerTurn()).event(NEXT_EVENT)
                .action(configurationBean.getOnPlayerTurn())
                .and()
                .withExternal().source(PLAYER_TURN_STATE).target(GAME_PLAY_STATE)
                .action(WebClientPlayerSsmService::forward)
                .event(NEXT_EVENT)
                .and()
                .withExternal().source(GAME_PLAY_STATE).target(AI_TURN_STATE)
                .guard(configurationBean.getSwitchAiTurn())
                .action(configurationBean.getOnAiTurn())
                .event(NEXT_EVENT)
                .and()
                .withExternal().source(AI_TURN_STATE).target(GAME_PLAY_STATE)
                .action(WebClientPlayerSsmService::forward).event(NEXT_EVENT)
                .and()
                .withExternal().source(GAME_PLAY_STATE).target(WAIT_TURN_STATE)
                .guard(configurationBean.getSwitchRemoteTurn())
                .action(configurationBean.getOnRemoteTurn())
                .event(NEXT_EVENT)
                .and()
                .withExternal().source(WAIT_TURN_STATE).target(GAME_PLAY_STATE)
                .action(WebClientPlayerSsmService::forward)
                .event(NEXT_EVENT)
                .and()
                .withExternal().source(GAME_PLAY_STATE).target(GAME_OVER_STATE)
                .event(STOP_EVENT);
    }

    @SneakyThrows
    private void configureStates(StateMachineBuilder.Builder<String, String> builder) {
        builder.configureStates()
                .withStates()
                .initial("GAME_PLAY")
                .end("GAME_OVER")
                .states(Stream.of("PLAYER_TURN", "WAIT_TURN",
                        "AI_TURN", "WAIT_TO_CONNECT").collect(Collectors.toSet()));
    }

    @SneakyThrows
    private void configureConfiguration(String uuid, StateMachineBuilder.Builder<String, String> builder) {
        builder.configureConfiguration()
                .withConfiguration()
                .machineId(uuid);
    }

    private static void forward(StateContext<String, String> stateContext) {
        if (Optional.ofNullable(stateContext.getExtendedState().getVariables().get("GAME_STAT")).isPresent()
                && stateContext.getExtendedState().getVariables().get("GAME_STAT").equals("END")) {
            stateContext.getStateMachine().sendEvent("STOP");
        } else {
            stateContext.getStateMachine().sendEvent(stateContext.getEvent());
        }
    }

}
