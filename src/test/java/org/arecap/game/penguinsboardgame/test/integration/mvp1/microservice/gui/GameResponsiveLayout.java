package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.gui;

import com.vaadin.flow.component.dependency.StyleSheet;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.gui.BootstrapResponsiveDiv;

import javax.annotation.PostConstruct;

@StyleSheet("/frontend/css/styles.css")
public class GameResponsiveLayout extends BootstrapResponsiveDiv {

    private GameTopNavView gameTopNavView = new GameTopNavView(getClass());

    @PostConstruct
    private void postConstructPostProcessor() {
        addClassName("content");
//        add(gameTopNavView);
    }

}
