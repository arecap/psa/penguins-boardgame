package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;

import java.util.UUID;

public final class PlayerService {

    private final static String[] playersColorCodes = new String[] {"green", "red", "blue", "orange"};

    public final static Player constructPlayer(Integer index) {
        return constructPlayer(index, "/frontend/images/player" + (index + 1) + ".png");
    }

    public final static Player constructPlayer(Integer index, String avatarPath) {
        Player player = new Player();
        player.setUuid(UUID.randomUUID().toString());
        player.setAvatarPath(avatarPath);
        player.setColorCode(playersColorCodes[index]);
        return player;
    }

}
