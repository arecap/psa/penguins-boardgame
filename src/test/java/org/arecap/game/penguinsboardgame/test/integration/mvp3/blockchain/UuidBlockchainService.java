package org.arecap.game.penguinsboardgame.test.integration.mvp3.blockchain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IAtomicReference;
import com.hazelcast.core.IMap;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class UuidBlockchainService implements BlockchainService<UuidBlock> {

    private static Logger logger = LoggerFactory.getLogger(UuidBlockchainService.class);

    @Value("${spring.profiles.active: default}")
    private String profile;

    @Value("${blockchain.prefix.hash:2}")
    private int prefix;

    private String prefixString;

    @Autowired
    private HazelcastInstance hazelcastInstance;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    @PostConstruct
    protected void setup() {
        prefixString = new String(new char[prefix]).replace('\0', '0');
    }

    public IAtomicReference<String> getCurrentHash() {
        return hazelcastInstance.getCPSubsystem().getAtomicReference("currentHash");    
    }


    @Override
    public UuidBlock mineBlock(String data, String entity) {
        Long timestamp = System.currentTimeMillis();
        prepareCurrentHash();
        UuidBlock uuidBlock = new UuidBlock(constructUuidBlockData(data, entity), getCurrentHash().get(), timestamp);
        logger.info("system mine block hash:\t" + uuidBlock.mineBlock(prefix) + "\texecution time:\t" + (System.currentTimeMillis() - timestamp));
        if(valid(uuidBlock)) {
            getCurrentHash().set(uuidBlock.getHash());
            putBlockchain(uuidBlock, entity);
            return uuidBlock;
        }
        return null;
    }

    private boolean valid(UuidBlock uuidBlock) {
        return uuidBlock.getHash().substring(0, prefix).equals(prefixString)
                && uuidBlock.getHash().equals(uuidBlock.calculateBlockHash());
    }

    @Override
    public boolean valid(String hash, String entity) {
        return Optional.ofNullable(getBlockchain(entity).get(hash)).isPresent() &&
                valid(getBlockchain(entity).get(hash));
    }

//    @Override
//    public String registerNode(Consumer<UuidBlock> listener, String entity) {
//        return getBlockchain(entity).addEntryListener(new MapListenerAdapter<String, UuidBlock>() {
//            @Override
//            public void onEntryEvent(EntryEvent<String, UuidBlock> event) {
//                listener.accept(event.getValue());
//            }
//        }, true);
//    }
//
//    @Override
//    public boolean destroyNode(String nodeRegistrationId, String entity) {
//        return getBlockchain(entity).removeEntryListener(nodeRegistrationId);
//    }

    @SneakyThrows
    private  String constructUuidBlockData(String data, String entity) {
        Map<String, String> blockchainDataMap = new HashMap<>();
        blockchainDataMap.put("data", data);
        blockchainDataMap.put("entity", entity);
        return objectMapper.writeValueAsString(blockchainDataMap);
    }

    private IMap<String, UuidBlock> getBlockchain(String entity) {
        return hazelcastInstance.getMap(entity + "::"+"profile");
    }

    private void putBlockchain(UuidBlock uuidBlock, String entity) {
        getBlockchain(entity).put(uuidBlock.getHash(), uuidBlock);
    }

    private void prepareCurrentHash() {
        if(!Optional.ofNullable(getCurrentHash().get()).isPresent()) {
            getCurrentHash().set(prefixString);
        }
    }

}
