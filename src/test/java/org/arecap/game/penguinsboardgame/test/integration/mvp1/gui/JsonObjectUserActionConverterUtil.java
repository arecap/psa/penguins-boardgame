package org.arecap.game.penguinsboardgame.test.integration.mvp1.gui;

import elemental.json.JsonObject;

public final class JsonObjectUserActionConverterUtil {

    public final static void setUserAction(UserAction userAction, JsonObject eventData) {
        userAction.setAltKey(eventData.getBoolean(getEventDataKeyFor(eventData, "altKey")));
        userAction.setCtrlKey(eventData.getBoolean(getEventDataKeyFor(eventData, "ctrlKey")));
        userAction.setMetaKey(eventData.getBoolean(getEventDataKeyFor(eventData, "metaKey")));
        userAction.setShiftKey(eventData.getBoolean(getEventDataKeyFor(eventData, "shiftKey")));
    }

    public final static void setCoordinateUserAction(CoordinateUserAction coordinateUserAction, JsonObject eventData) {
        setUserAction(coordinateUserAction, eventData);
        coordinateUserAction.setClientX(eventData.getNumber(getEventDataKeyFor(eventData, "clientX")));
        coordinateUserAction.setClientY(eventData.getNumber(getEventDataKeyFor(eventData, "clientY")));
        coordinateUserAction.setPageX(eventData.getNumber(getEventDataKeyFor(eventData, "pageX")));
        coordinateUserAction.setPageY(eventData.getNumber(getEventDataKeyFor(eventData, "pageY")));
        coordinateUserAction.setScreenX(eventData.getNumber(getEventDataKeyFor(eventData, "screenX")));
        coordinateUserAction.setScreenY(eventData.getNumber(getEventDataKeyFor(eventData, "screenY")));
    }

    public final static void setMouseUserAction(MouseUserAction mouseUserAction, JsonObject eventData) {
        setCoordinateUserAction(mouseUserAction, eventData);
        mouseUserAction.setOffsetX(eventData.getNumber(getEventDataKeyFor(eventData, "offsetX")));
        mouseUserAction.setOffsetY(eventData.getNumber(getEventDataKeyFor(eventData, "offsetY")));
        mouseUserAction.setButton(getEventDataIntegerNumeric(eventData, "button"));
        mouseUserAction.setButtons(getEventDataIntegerNumeric(eventData, "buttons"));
        mouseUserAction.setWhich(getEventDataIntegerNumeric(eventData, "which"));
    }

    public final static void setWheelUserAction(WheelUserAction wheelUserAction, JsonObject eventData) {
        setMouseUserAction(wheelUserAction, eventData);
        wheelUserAction.setDeltaX(eventData.getNumber(getEventDataKeyFor(eventData, "deltaX")));
        wheelUserAction.setDeltaY(eventData.getNumber(getEventDataKeyFor(eventData, "deltaY")));
        wheelUserAction.setDeltaZ(eventData.getNumber(getEventDataKeyFor(eventData, "deltaZ")));
        wheelUserAction.setDeltaMode(getEventDataIntegerNumeric(eventData, "deltaMode"));
    }

    public static void setTouchUserAction(TouchUserAction touchUserAction, JsonObject eventData) {
        touchUserAction.setClientX(eventData.getNumber(getEventDataKeyFor(eventData, "clientX")));
        touchUserAction.setClientY(eventData.getNumber(getEventDataKeyFor(eventData, "clientY")));
        touchUserAction.setPageX(eventData.getNumber(getEventDataKeyFor(eventData, "pageX")));
        touchUserAction.setPageY(eventData.getNumber(getEventDataKeyFor(eventData, "pageY")));
        touchUserAction.setScreenX(eventData.getNumber(getEventDataKeyFor(eventData, "screenX")));
        touchUserAction.setScreenY(eventData.getNumber(getEventDataKeyFor(eventData, "screenY")));
        touchUserAction.setRotationAngle(eventData.getNumber(getEventDataKeyFor(eventData, "rotationAngle")));
        touchUserAction.setRadiusX(eventData.getNumber(getEventDataKeyFor(eventData, "radiusX")));
        touchUserAction.setRadiusY(eventData.getNumber(getEventDataKeyFor(eventData, "radiusY")));
        touchUserAction.setForce(eventData.getNumber(getEventDataKeyFor(eventData, "force")));
    }

    public final static String getEventDataKeyFor(JsonObject eventData, String eventKey) {
        if(eventData.hasKey("event." + eventKey)) {
            return "event." + eventKey;
        }
        return eventKey;
    }

    public final static Integer getEventDataIntegerNumeric(JsonObject eventData, String key) {
        return new Double(eventData.getNumber(getEventDataKeyFor(eventData, key))).intValue();
    }

}
