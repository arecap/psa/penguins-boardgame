package org.arecap.game.penguinsboardgame.test.integration.mvp3;

import org.springframework.statemachine.StateContext;

public interface RemotePlayerTurnSsmProcessor {

    boolean isRemotePlayerTurn(StateContext<String, String> stateContext);

    void onRemotePlayerTurn(StateContext<String, String> stateContext);

}
