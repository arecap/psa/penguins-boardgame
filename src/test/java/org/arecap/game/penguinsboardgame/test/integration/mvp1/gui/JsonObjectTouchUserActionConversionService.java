package org.arecap.game.penguinsboardgame.test.integration.mvp1.gui;

import elemental.json.JsonObject;
import org.springframework.core.convert.converter.Converter;

public class JsonObjectTouchUserActionConversionService implements Converter<JsonObject, TouchUserAction> {

    @Override
    public TouchUserAction convert(JsonObject eventData) {
        TouchUserAction touchUserAction = new TouchUserAction();
        JsonObjectUserActionConverterUtil.setTouchUserAction(touchUserAction, eventData);
        return touchUserAction;
    }

}
