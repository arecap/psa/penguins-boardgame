package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.gui;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.Getter;
import lombok.Setter;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.BoardGame;

//@InformationExchange
@SpringComponent
@UIScope
@Getter @Setter
@JsonIgnoreProperties({"sub", "iss", "aud", "iat"})
public class PlayerExchange {

    private BoardGame boardGame;


}
