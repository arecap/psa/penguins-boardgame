package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/player-info")
public class PlayerInfoController {

    @GetMapping("/active-players-size")
    public Integer getActivePlayersCount() {
        return 0;
    }
}
