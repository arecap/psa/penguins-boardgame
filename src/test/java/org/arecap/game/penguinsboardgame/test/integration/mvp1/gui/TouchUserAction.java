package org.arecap.game.penguinsboardgame.test.integration.mvp1.gui;

import lombok.Getter;
import lombok.Setter;

public class TouchUserAction extends CoordinateUserAction {

    @Getter @Setter
    private Double rotationAngle;

    @Getter @Setter
    private Double radiusX;

    @Getter @Setter
    private Double radiusY;

    @Getter @Setter
    private Double force;

}
