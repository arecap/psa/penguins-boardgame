package org.arecap.game.penguinsboardgame.test.unit.orange.btree;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Node  {

    private int value;

    private Node leftValue;

    private Node rightValue;

    public Node() {
    }

    public Node(int value) {
        this.value = value;
        leftValue = null;
        rightValue = null;
    }
}
