package org.arecap.game.penguinsboardgame.test.integration.mvp3;

import org.springframework.statemachine.StateContext;

public interface WebClientSsmOnlineProcessor extends WebClientSsmFullProcessor {

    @Override
    default boolean isAiPlayerTurn(StateContext<String, String> stateContext) {
        return false;
    }

    @Override
    default void onAiPlayerTurn(StateContext<String, String> stateContext) {
        //N/A
    }
}
