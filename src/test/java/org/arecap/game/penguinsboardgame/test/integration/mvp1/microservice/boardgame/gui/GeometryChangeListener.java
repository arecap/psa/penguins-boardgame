package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.gui;

public interface GeometryChangeListener {

    void clientGraphicsUpdate();

}
