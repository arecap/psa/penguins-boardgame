package org.arecap.game.penguinsboardgame.test.integration.mvp3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PenguinsBoardgameApplication3mvp {

    public static void main(String... args) {
        SpringApplication.run(PenguinsBoardgameApplication3mvp.class, args);
    }

}
