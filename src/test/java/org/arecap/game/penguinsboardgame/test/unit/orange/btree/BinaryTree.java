package org.arecap.game.penguinsboardgame.test.unit.orange.btree;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;
import java.util.function.Consumer;

@Getter
@Slf4j
public class BinaryTree  {

    private Node root;

    private static Node addRecursive(Node current, int value) {
        if(current == null) {
            return new Node(value);
        }
        if(value < current.getValue()) {
            current.setLeftValue(addRecursive(current.getLeftValue(), value));
        } else if (value > current.getValue()) {
            current.setRightValue(addRecursive(current.getRightValue(), value));
        }
        //value already exists
        return current;
    }


    private static boolean containsNodeRecursive(Node current, int value) {
        if (current == null) {
            return false;
        }
        if (value == current.getValue()) {
            return true;
        }
        return value < current.getValue()
                ? containsNodeRecursive(current.getLeftValue(), value)
                : containsNodeRecursive(current.getRightValue(), value);
    }

    private static Node deleteRecursive(Node current, int value) {
        if (current == null) {
            return null;
        }

        if (value == current.getValue()) {
            // Node to delete found
            // ... code to delete the node will go here
            BinaryTreeCleaner btCleaner = BinaryTreeCleaner.noChildren()
                    .appendNext(BinaryTreeCleaner.hasOnChild())
                    .appendNext(BinaryTreeCleaner.tree());
            return btCleaner.delete(current);
        }
        if (value < current.getValue()) {
            current.setLeftValue(deleteRecursive(current.getLeftValue(), value));
            return current;
        }
        current.setRightValue(deleteRecursive(current.getRightValue(), value));
        return current;
    }

    private static int findSmallestValue(Node node) {
        return Optional.ofNullable(node.getLeftValue()).isPresent() ?
                findSmallestValue(node.getLeftValue()) : node.getValue();
    }

    public void add(int value) {
        root = addRecursive(root, value);
    }

    public boolean containsNode(int value) {
        return containsNodeRecursive(root, value);
    }

    public void delete(int value) {
        root = deleteRecursive(root, value);
    }

    public static void traverseInOrder(Node node, Consumer<Node> inOrderConsumer) {
        if (node != null) {
            traverseInOrder(node.getLeftValue(), inOrderConsumer);
            inOrderConsumer.accept(node);
            traverseInOrder(node.getRightValue(), inOrderConsumer);
        }
    }

    public static void traversePreOrder(Node node, Consumer<Node> preOrderConsumer) {
        if (node != null) {
            preOrderConsumer.accept(node);
            traversePreOrder(node.getLeftValue(), preOrderConsumer);
            traversePreOrder(node.getRightValue(), preOrderConsumer);
        }
    }

    public static void traversePostOrder(Node node, Consumer<Node> postOrderConsumer) {
        if (node != null) {
            traversePostOrder(node.getLeftValue(), postOrderConsumer);
            traversePostOrder(node.getRightValue(), postOrderConsumer);
            postOrderConsumer.accept(node);
        }
    }

    public static void traverseLevelOrder(BinaryTree binaryTree, Consumer<Node> levelOrderConsumer) {
        if (binaryTree.getRoot() == null) {
            return;
        }

        Queue<Node> nodes = new LinkedList<>();
        nodes.add(binaryTree.getRoot());

        while (!nodes.isEmpty()) {

            Node node = nodes.remove();

            levelOrderConsumer.accept(node);

            if (Optional.ofNullable(node.getLeftValue()).isPresent()) {
                nodes.add(node.getLeftValue());
            }

            if (Optional.ofNullable(node.getRightValue()).isPresent()) {
                nodes.add(node.getRightValue());
            }
        }
    }

    @FunctionalInterface
    interface BinaryTreeCleaner {

        Node delete(Node node);

        default BinaryTreeCleaner appendNext(BinaryTreeCleaner nextBtc) {
            return (node) -> {
                Node deleted = delete(node);
                if(Optional.ofNullable(deleted).isPresent() && deleted.equals(node)) {
                    return nextBtc.delete(deleted);
                }
                return deleted;
            };
        }

        static BinaryTreeCleaner noChildren() {
            return (node) -> {
                if (!Optional.ofNullable(node.getLeftValue()).isPresent()
                        && !Optional.ofNullable(node.getRightValue()).isPresent()) {
                    return null;
                } else {
                    return node;
                }
            };
        }

        static BinaryTreeCleaner hasOnChild() {
            return (node) -> {
                if (!Optional.ofNullable(node.getRightValue()).isPresent()) {
                    return node.getLeftValue();
                }
                if (!Optional.ofNullable(node.getLeftValue()).isPresent()) {
                    return node.getRightValue();
                }
                return node;
            };
        }

        static BinaryTreeCleaner tree() {
            return (node) -> {
                int smallestValue = findSmallestValue(node.getRightValue());
                node.setValue(smallestValue);
                node.setRightValue(deleteRecursive(node.getRightValue(), smallestValue));
                return node;
            };
        }

    }



}
