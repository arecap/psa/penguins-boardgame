package org.arecap.game.penguinsboardgame.test.integration.mvp3;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteConfiguration;
import elemental.json.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.arecap.game.penguinsboardgame.test.integration.mvp3.gui.MainMenu;
import org.arecap.game.penguinsboardgame.test.integration.mvp3.gui.PlayerHud;
import org.arecap.game.penguinsboardgame.test.integration.mvp3.singleplay.SinglePlayerEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateMachine;

import java.util.Optional;

@Route("")
@Slf4j
public class WebApplicationEndpoint extends VaadinEndpoint {


    @Autowired
    private WebApplicationClientRegisterService webApplicationClientRegisterService;

    @Autowired
    private WebClientPlayerSsmService webClientPlayerSsmService;

    private String webClientUuid;

    private Div boardBackground = new Div();

    private PlayerHud playerHud = new PlayerHud();

    private MainMenu mainMenu = new MainMenu();

    private StateMachine<String, String> gamePlaySsm;


    private int gameTurnsCount = 0;

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        log.info("onAttach");
        addClassName("content");
        getStyle().set("position", "relative");
        getStyle().set("overflow", "hidden");
        boardBackground.getStyle().set("position", "absolute");
        boardBackground.getStyle().set("width", "200%");
        boardBackground.getStyle().set("height", "200%");
        boardBackground.getStyle().set("top", "-50%");
        boardBackground.getStyle().set("left", "-50%");
        boardBackground.getStyle().set("background-image", "url('frontend/images/tablecloth1.jpeg')");
        boardBackground.getStyle().set("background-repeat", "repeat");
        boardBackground.getStyle().set("background-color", "#fff");
        setSizeFull();
        playerHud.setMainMenu(Optional.of(mainMenu));
        mainMenu.onNewGameAction(this::newGame);
        add(boardBackground, playerHud, mainMenu);
    }

    private void newGame(int clickCount) {
        log.info("newGame click count:\t" + clickCount);
        if(clickCount > 1) {
            return;
        }
        playerHud.hideMainMenu();
        getUI().get().access(() -> getUI().get().getPage().setLocation(RouteConfiguration.forApplicationScope().getUrl(SinglePlayerEndpoint.class)));
//        WebClientPlayerSsmConfigurationBean clientPlayerSsmConfigurationBean = new WebClientPlayerSsmConfigurationBean();
//        clientPlayerSsmConfigurationBean.setSwitchPlayerTurn(this::isPlayerTurn);
//        clientPlayerSsmConfigurationBean.setOnPlayerTurn(this::onPlayerTurn);
//        clientPlayerSsmConfigurationBean.setSwitchAiTurn(this::isAiTurn);
//        clientPlayerSsmConfigurationBean.setOnAiTurn(this::onAiTurn);
//        clientPlayerSsmConfigurationBean.setSwitchRemoteTurn(this::isRemoteTurn);
//        clientPlayerSsmConfigurationBean.setOnRemoteTurn(this::onRemoteTurn);
//        gamePlaySsm = webClientPlayerSsmService.getStateMachine(webClientUuid, clientPlayerSsmConfigurationBean);
//        gamePlaySsm.getExtendedState().getVariables().put("CURRENT_PLAYER", 1);
//        gamePlaySsm.getExtendedState().getVariables().put("GAME_STAT", "PLAY");
//        gameTurnsCount = 0;
//        gamePlaySsm.start();
//        log.info("ssm state id:\t" + gamePlaySsm.getState().getId());
//        gamePlaySsm.sendEvent("NEXT");
//        log.info("ssm state id:\t" + gamePlaySsm.getState().getId());
    }

//    @SneakyThrows
//    private void onRemoteTurn(StateContext<String, String> stateContext) {
//        log.info("is remote turn:\t" + gameTurnsCount);
//        if(gameTurnsCount > 1899 ) {
//            gamePlaySsm.getExtendedState().getVariables().put("GAME_STAT", "END");
//        }
//        gamePlaySsm.getExtendedState().getVariables().put("CURRENT_PLAYER", 1);
//        gamePlaySsm.sendEvent("NEXT");
//    }
//
//    private boolean isRemoteTurn(StateContext<String, String> stateContext) {
//        return stateContext.getExtendedState().getVariables().get("CURRENT_PLAYER") == Integer.valueOf(3);
//    }
//
//    @SneakyThrows
//    private void onAiTurn(StateContext<String, String> stateContext) {
//        log.info("is AI turn:\t" + gameTurnsCount);
//        gamePlaySsm.getExtendedState().getVariables().put("CURRENT_PLAYER", 3);
//        gamePlaySsm.sendEvent("NEXT");
//    }
//
//    private boolean isAiTurn(StateContext<String, String> stateContext) {
//        return stateContext.getExtendedState().getVariables().get("CURRENT_PLAYER") == Integer.valueOf(2);
//    }
//
//    @SneakyThrows
//    private void onPlayerTurn(StateContext<String, String> stateContext) {
//        log.info("is player turn:\t"+ (++gameTurnsCount));
//        gamePlaySsm.getExtendedState().getVariables().put("CURRENT_PLAYER", 2);
//        gamePlaySsm.sendEvent("NEXT");
//    }
//
//    private boolean isPlayerTurn(StateContext<String, String> stateContext) {
//        return stateContext.getExtendedState().getVariables().get("CURRENT_PLAYER") == Integer.valueOf(1);
//    }

    @Override
    protected void onDetach(DetachEvent detachEvent) {
        log.info("onDetach");
        if(Optional.ofNullable(gamePlaySsm).isPresent()) {
            gamePlaySsm.stop();
        }
        webApplicationClientRegisterService.unregister(webClientUuid);
    }

    @Override
    public void afterNavigation(AfterNavigationEvent afterNavigationEvent) {
        super.afterNavigation(afterNavigationEvent);
        log.info("afterNavigation -- load user mvp script and init user model ");
        UI.getCurrent().getPage().addJavaScript("/frontend/js/PenguinUserMVP.js");
        UI.getCurrent().getPage()
                .executeJs("PenguinUserModelPresenter.mineOrLoadUserModel($0);",
                        this.getElement());
    }

    @ClientCallable
    public void mineWebClient() {
        log.info("mineWebClient");
        webApplicationClientRegisterService.requestNewPlayerClient(this::webClientReady);
    }

    private void webClientReady(WebApplicationClientModel webApplicationClientModel) {
        log.info("webClientReady");
        webClientUuid = webApplicationClientModel.getUuid();
        if(getUI().isPresent()) {
            UI ui = getUI().get();
            ui.access(() -> ui.getPage().executeJs("PenguinUserModelPresenter.initModel($0);",
                    webApplicationClientModel.getUuid()));
        }
    }

    @ClientCallable
    public void loadWebClientModel(JsonObject jsonObject) {
        log.info("JSON client model:\t" + jsonObject.toJson());
        this.webClientUuid = jsonObject.getString("clientUuid");
    }



}
