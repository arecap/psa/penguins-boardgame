//package org.arecap.game.penguinsboardgame.microservice.boardgame.gui;
//
//import com.hazelcast.core.MemberAttributeEvent;
//import com.hazelcast.core.MembershipEvent;
//import com.hazelcast.core.MembershipListener;
//import lombok.SneakyThrows;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//public class DistributedBroadcasterMembershipListener implements MembershipListener {
//
//    private static Logger logger = LoggerFactory.getLogger(DistributedBroadcasterMembershipListener.class);
//
//    private final DistributedBroadcaster dsBroadcaster;
//
//
//    DistributedBroadcasterMembershipListener(DistributedBroadcaster dsBroadcaster) {
//        this.dsBroadcaster = dsBroadcaster;
//    }
//
//
//
//    //        public DistributedBroadcasterMembershipListener()
//
//    @SneakyThrows
//    @Override
//    public void memberAdded(MembershipEvent membershipEvent) {
//        dsBroadcaster.setInstanceBroadcasterRegistryRef(dsBroadcaster.getInstanceBroadcaster().register(membershipEvent.getMember().getUuid(),
//                event -> dsBroadcaster.getInstanceBroadcaster().instanceMessage((DistributedBroadcaster.DistributedBroadcasterEvent) event)));
//        if(dsBroadcaster.getDistributedSystemType().equals(DistributedBroadcaster.DistributedSystemType.DUAL_CHANNEL)) {
//            dsBroadcaster.getInstanceBroadcaster().broadcast(DistributedBroadcaster.DistributedBroadcasterEvent.RESTART);
//        }
//    }
//
//    @SneakyThrows
//    @Override
//    public void memberRemoved(MembershipEvent membershipEvent) {
//        if(!DistributedBroadcaster.getDistributedSystemType().equals(DistributedBroadcaster.DistributedSystemType.RAFT_CONSENSUS)) {
//            dsBroadcaster.getInstanceBroadcaster().broadcast(membershipEvent.getMember().getUuid(), DistributedBroadcaster.DistributedBroadcasterEvent.RESTART);
//            dsBroadcaster.destroyInstanceBroadcasterRegistryRef();
//        }
//    }
//
//    @Override
//    public void memberAttributeChanged(MemberAttributeEvent memberAttributeEvent) {
//        //N/A
//    }
//
//}
