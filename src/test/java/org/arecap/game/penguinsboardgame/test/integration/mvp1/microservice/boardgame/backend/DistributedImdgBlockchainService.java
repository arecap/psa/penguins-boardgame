package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.*;
import com.hazelcast.map.impl.MapListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

@Service
public class DistributedImdgBlockchainService {

    private static Logger logger = LoggerFactory.getLogger(DistributedImdgBlockchainService.class);

    @Value("${spring.profiles.active: default}")
    private String profile;

    @Value("${blockchain.prefix.hash:2}")
    private int prefix;

    private String prefixString;

    @Autowired
    private HazelcastInstance hazelcastInstance;

    private IAtomicReference<String> currentHash() {
        return hazelcastInstance.getCPSubsystem().getAtomicReference("currentHash");
    }

//    private IMap<String, UuidBlock> blockchain = hazelcastInstance.getMap("blockchain-" + profile);




    @PostConstruct
    protected void setup() {
        prefixString = new String(new char[prefix]).replace('\0', '0');
    }

    public String registerNode(Consumer<UuidBlock> blockchainMiningEventListener, String entity) {
        IMap<String, UuidBlock> blockchain = hazelcastInstance.getMap( entity + "-" + profile);
        String listenerUuid = blockchain.addEntryListener(new MapListenerAdapter<String, UuidBlock>() {
            @Override
            public void onEntryEvent(EntryEvent<String, UuidBlock> event) {
                blockchainMiningEventListener.accept(event.getValue());
            }

            @Override
            public void onMapEvent(MapEvent event) {
                super.onMapEvent(event);
            }
        }, true);
        return listenerUuid;
    }

    public void destroyNode(String nodeId, String entity) {
        IMap<String, UuidBlock> blockchain = hazelcastInstance.getMap( entity + "-" + profile);
        blockchain.removeEntryListener(nodeId);
    }

    private synchronized boolean valid(UuidBlock value) {
        if(!Optional.ofNullable(value).isPresent()) {
            return false;
        }
        logger.info("validate block hash:\t" + value.getHash());
        return value.getHash().substring(0, prefix).equals(prefixString)
                && currentHash().get().equals(value.getPreviousHash())
                && value.getHash().equals(value.calculateBlockHash());
    }

    @Async
    public void mineBlock(String uuid, String entity) throws JsonProcessingException {
        IMap<String, UuidBlock> blockchain = hazelcastInstance.getMap( entity + "-" + profile);
        Map<String, String> blockchainDataMap = new HashMap<>();
        blockchainDataMap.put("uuid", uuid);
        blockchainDataMap.put("entity", entity);
        Long timestamp = System.currentTimeMillis();
        if(!Optional.ofNullable(currentHash().get()).isPresent()) {
            currentHash().set(prefixString);
        }
        UuidBlock uuidBlock = new UuidBlock(new ObjectMapper().writeValueAsString(blockchainDataMap), currentHash().get(), timestamp);
        logger.info("system mine block hash:\t" + uuidBlock.mineBlock(prefix) + "\texecution time:\t" + (System.currentTimeMillis() - timestamp));
        if(valid(uuidBlock)) {
            currentHash().set(uuidBlock.getHash());
            blockchain.put(uuidBlock.getHash(), uuidBlock);
        }
    }

    public boolean isValid(String uuid, String entity) {
        logger.info("verify uuidBlock:\t" + uuid + "\tof entity:\t" + entity);
        IMap<String, UuidBlock> blockchain = hazelcastInstance.getMap( entity + "-" + profile);
        UuidBlock uuidBlock = blockchain.get(uuid);
        return Optional.ofNullable(uuidBlock).isPresent()
                && uuidBlock.getHash().substring(0, prefix).equals(prefixString)
                && uuidBlock.getHash().equals(uuidBlock.calculateBlockHash())
                && Optional.ofNullable(blockchain.get(uuidBlock.getPreviousHash())).isPresent();
    }

}
