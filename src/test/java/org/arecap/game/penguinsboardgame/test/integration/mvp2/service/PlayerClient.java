package org.arecap.game.penguinsboardgame.test.integration.mvp2.service;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter @Setter
public class PlayerClient implements Serializable {

    public static String entity = "playerClientRegistry";

    String uuid;

}
