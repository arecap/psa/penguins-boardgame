package org.arecap.game.penguinsboardgame.test.unit;

import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.BoardGame;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.Ice;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.Penguin;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class BoardGameModelTest {


    @Test
    public void testPlayersScore() {
        for(int n = 0; n < 100; ++n) {
            BoardGame boardGame = new BoardGame();
            boardGame.setPenguins(setupPenguins());
            boardGame.setPlayersNumber(boardGame.getPenguins().size());
            boardGame.setIceBoard(setupIceBoard());
            int totalScore = getBoardGameTotalScore(boardGame);
            System.out.println("total score:\t" + totalScore);
            Assert.isTrue(totalScore == 100, "Ice Board miss configuration!");
            int playerRedScore = 0, playerBlueScore = 0, playerGreenScore = 0, playerYellowScore = 0;
            for (int p = 0; p < boardGame.getPlayersNumber(); ++p) {
                for (int i = 0; i < ThreadLocalRandom.current().nextInt(3, 6); ++i) {
                    int icePos = 0;
                    do {
                        icePos = ThreadLocalRandom.current().nextInt(0, 60);
                    } while (boardGame.getIceBoard().get(icePos).isVisited());
                    switch (p) {
                        case 0:
                            playerRedScore += boardGame.getIceBoard().get(icePos).getFishValue();
                            break;
                        case 1:
                            playerBlueScore += boardGame.getIceBoard().get(icePos).getFishValue();
                            break;
                        case 2:
                            playerGreenScore += boardGame.getIceBoard().get(icePos).getFishValue();
                            break;
                        case 3:
                            playerYellowScore += boardGame.getIceBoard().get(icePos).getFishValue();
                            break;
                    }
                }
            }
            System.out.println("Board game has:\t" + boardGame.getPlayersNumber() + "\tplayers");
            for (int p = 0; p < boardGame.getPlayersNumber(); ++p) {
                switch (p) {
                    case 0:
                        System.out.println("Player red score:\t" + playerRedScore);
                        break;
                    case 1:
                        System.out.println("Player blue score:\t" + playerBlueScore);
                        break;
                    case 2:
                        System.out.println("Player green score:\t" + playerGreenScore);
                        break;
                    case 3:
                        System.out.println("Player yellow score:\t" + playerYellowScore);
                        break;
                }
            }
        }
    }

    private int getBoardGameTotalScore(BoardGame boardGame) {
        return boardGame.getIceBoard().values().stream().map(Ice::getFishValue).reduce(0, Integer::sum);
    }

    private Map<Integer, Ice> setupIceBoard() {
        Map<Integer, Ice> iceBoard = new LinkedHashMap<>();
        int countFish1 = 0;
        int countFish2 = 0;
        int countFish3 = 0;
        for(int i =0; i < 60; ++i) {
            int fishValue = ThreadLocalRandom.current().nextInt(1, 4);
            if(fishValue == 1 && countFish1 < 30) {
                countFish1++;
            } else if(fishValue == 1) {
                fishValue = 2;
            }
            if(fishValue == 2 && countFish2 < 20) {
                countFish2++;
            } else if(fishValue == 2){
                if(countFish1 < 30) {
                    fishValue = 1;
                    countFish1++;
                } else {
                    fishValue = 3;
                }
            }
            if(fishValue == 3 && countFish3 < 10) {
                countFish3++;
            } else if(fishValue == 3) {
                if(countFish1 < 30) {
                    fishValue = 1;
                    countFish1++;
                } else {
                    fishValue = 2;
                    countFish2++;
                }
            }
            iceBoard.put(i, setupIce(fishValue, i));
        }
        System.out.println("count fish1:\t" + countFish1 + "\tcount fish2:\t" + countFish2 + "\tcount fish3:\t" + countFish3);
        return iceBoard;
    }

    private Ice setupIce(int fishValue, int rootNth) {
        Ice ice = new Ice();
        ice.setUuid(UUID.randomUUID().toString());
        ice.setRootNth(rootNth);
        ice.setFishValue(fishValue);
        return ice;
    }

    private Map<String, Penguin> setupPenguins() {
        Map<String, Penguin> players = new HashMap<>();
        for(int i =0; i < ThreadLocalRandom.current().nextInt(2, 5); ++i) {
            Penguin penguin = setupPlayer(i);
            players.put(penguin.getUuid(), penguin);
        }
        return players;
    }

    private Penguin setupPlayer(int i) {
        Penguin penguin = new Penguin();
        penguin.setUuid(UUID.randomUUID().toString());
//        penguin.setColorCode(returnColorCode(i));
        return penguin;
    }

    private String returnColorCode(int i) {
        return i == 0 ? "red" : ( i == 1 ? "blue" : (i == 2 ? "green" : "yellow") );
    }

}
