package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;

public enum GameStates {

    USER_MENU, GAME_PAUSED, GAME_PLAY, PLAYER_TURN, WAIT_REMOTE_TURN, AI_TURN, WAIT_TO_CONNECT


}
