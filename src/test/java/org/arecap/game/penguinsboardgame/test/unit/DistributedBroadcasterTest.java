package org.arecap.game.penguinsboardgame.test.unit;

import com.hazelcast.core.Hazelcast;
import lombok.SneakyThrows;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.gui.DistributedBroadcaster;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.function.Consumer;

public class DistributedBroadcasterTest {

    static class StringDistributedBroadcaster extends DistributedBroadcaster<String> {

        @Override
        public Map<String, Consumer<String>> getConsumerMap() {
            if(Hazelcast.getAllHazelcastInstances().size() > 0) {
                return Hazelcast.getAllHazelcastInstances().stream().findAny().get().getMap("string-distributed-broadcaster-test");
            }
            return null;
        }

    }


    @SneakyThrows
    @Test
    public void test() {
        StringDistributedBroadcaster broadcasterChannel1 = new StringDistributedBroadcaster();
        broadcasterChannel1.init();
        Thread.sleep(2000);
        StringDistributedBroadcaster broadcasterChannel2 = new StringDistributedBroadcaster();
        broadcasterChannel2.init();
        Thread.sleep(2000);
        StringDistributedBroadcaster broadcasterChannel3 = new StringDistributedBroadcaster();
        broadcasterChannel3.init();
        Thread.sleep(2000);
        StringDistributedBroadcaster broadcasterChannel4 = new StringDistributedBroadcaster();
        broadcasterChannel4.init();
        Thread.sleep(45000);
    }


}
