package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.gui;

import com.vaadin.flow.spring.annotation.UIScope;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.GameEvents;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.GameStates;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import java.util.EnumSet;

@Configuration
@EnableStateMachine
@UIScope
public class UiStateMachineConfiguration extends EnumStateMachineConfigurerAdapter<GameStates, GameEvents> {


    @Override
    public void configure(StateMachineStateConfigurer<GameStates, GameEvents> states)
            throws Exception {
        states
                .withStates()
                .initial(GameStates.USER_MENU)
                .states(EnumSet.allOf(GameStates.class));
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<GameStates, GameEvents> transitions)
            throws Exception {
        transitions
                .withExternal()
                .source(GameStates.USER_MENU)
                .target(GameStates.GAME_PLAY)
                .event(GameEvents.PLAY)
                .and()
                .withExternal()
                .source(GameStates.GAME_PLAY)
                .target(GameStates.USER_MENU)
                .event(GameEvents.PAUSE);
    }


}
