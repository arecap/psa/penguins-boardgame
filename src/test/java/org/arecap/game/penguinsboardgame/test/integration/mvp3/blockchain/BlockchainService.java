package org.arecap.game.penguinsboardgame.test.integration.mvp3.blockchain;

public interface BlockchainService<T> {

    T mineBlock(String data, String entity);

    boolean valid(String hash, String entity);

}
