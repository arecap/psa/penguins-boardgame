package org.arecap.game.penguinsboardgame.test.integration.mvp1.graphics;

import org.arecap.game.penguinsboardgame.test.integration.mvp1.graphics.encoder.PngEncoder;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.Game2dGraphicTransformSettings;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public interface PngGraphics2d {

    void drawContent(Graphics2D graphics2D, Game2dGraphicTransformSettings game2dGraphicTransformSettings);

    default InputStream draw(Game2dGraphicTransformSettings game2dGraphicTransformSettings) {

        BufferedImage bufferedImage = new BufferedImage(game2dGraphicTransformSettings.getClientBounds().getRealValue().intValue(),
                game2dGraphicTransformSettings.getClientBounds().getImaginaryValue().intValue(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics2D = Graphics2dUtils.constructGraphics2D(bufferedImage,
                game2dGraphicTransformSettings.getClientBounds().getRealValue().intValue(),
                game2dGraphicTransformSettings.getClientBounds().getImaginaryValue().intValue());
        drawContent(graphics2D, game2dGraphicTransformSettings);
        graphics2D.dispose();
        long t = System.currentTimeMillis();


        InputStream is = null;
        try {
            is = new ByteArrayInputStream(toByteArrayAutoClosable(bufferedImage));
        }catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Write image processing :\t" + (System.currentTimeMillis() - t) + "\tms");
        return is;

    }

    default byte[] toByteArrayAutoClosable(BufferedImage image) throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            new PngEncoder().write(image, out);
            return out.toByteArray();
        } catch (Exception e) {
        }
        return null;
    }

}
