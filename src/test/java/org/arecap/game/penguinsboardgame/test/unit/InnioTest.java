package org.arecap.game.penguinsboardgame.test.unit;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class InnioTest {


    private static Logger logger = LoggerFactory.getLogger(InnioTest.class);
 /*

 ["dog", "palm", "god", "lamp", "whatever", "lllad" , "alldl"] => [["dog", "god"], ["palm", "lamp"], ["whatever"], ["lllad" , "alldl"]]
  */



    @Test
    public void sortCollectAnagram() {
        String[] anagrams = {"alldl", "dog", "palm", "god", "lamp", "whatever", "lllad" , "alldl"};
        List<List<String>> collectionAnagram = new ArrayList<>();
        for(String world:anagrams) {
            boolean match = false;
            for(List<String> collections: collectionAnagram) {
                if(collections.get(0).length() == world.length()) {
                    Map<Character, Integer> worldCntMap = new HashMap<>();
                    Map<Character, Integer> collenctionCntMap = new HashMap<>();
                    for(int i = 0; i < world.length(); ++i) {
                        setCharacterCountInMap(worldCntMap, world.charAt(i));
                        setCharacterCountInMap(collenctionCntMap, collections.get(0).charAt(i));
                    }
                    match = checkCharacterMaps(worldCntMap, collenctionCntMap);
                    if(match) {
                        collections.add(world);
                    }
                }
            }
            if(!match) {
                List<String> colls = new ArrayList<>();
                colls.add(world);
                collectionAnagram.add(colls);
            }
        }
        try {
            logger.info("result: " + new ObjectMapper().writeValueAsString(collectionAnagram));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }


    private static void setCharacterCountInMap(Map<Character, Integer> cMap, Character c) {
        if(Optional.ofNullable(cMap.get(c)).isPresent()) {
            cMap.put(c, cMap.get(c) + 1);
        } else {
            cMap.put(c, 1);
        }

    }

    private static boolean checkCharacterMaps(Map<Character, Integer> cMap1, Map<Character, Integer> cMap2) {
        if(cMap1.size() != cMap2.size()) {
            return false;
        }
        for(Character c1: cMap1.keySet()) {
            if(!Optional.ofNullable(cMap2.get(c1)).isPresent() && cMap1.get(c1) == cMap2.get(c1)) {
                return false;
            }
        }
        return true;
    }

}
