package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;

import lombok.Getter;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.graphics.PngGraphics2d;

import java.awt.*;

public abstract class BoardGamePngService implements PngGraphics2d  {

    @Getter
    private BoardGame boardGame;

    public BoardGamePngService(BoardGame boardGame) {
        this.boardGame = boardGame;
    }

    protected abstract void drawIce(Graphics2D graphics2D, Game2dGraphicTransformSettings game2dGraphicTransformSettings, Ice ice);

}
