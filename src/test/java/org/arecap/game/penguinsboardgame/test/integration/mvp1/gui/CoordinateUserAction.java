package org.arecap.game.penguinsboardgame.test.integration.mvp1.gui;

import lombok.Getter;
import lombok.Setter;

public class CoordinateUserAction extends UserAction {

    @Getter @Setter
    private Double clientX;

    @Getter @Setter
    private Double clientY;

    @Getter @Setter
    private Double pageX;

    @Getter @Setter
    private Double pageY;

    @Getter @Setter
    private Double screenX;

    @Getter @Setter
    private Double screenY;


}
