package org.arecap.game.penguinsboardgame.test.integration.mvp2.messaging;


import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ITopic;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.FixedSubscriberChannel;
import org.springframework.messaging.Message;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@SpringComponent
public class TopicEventBus {


    @Autowired
    private HazelcastInstance hazelcastInstance;

    private Map<String, FixedSubscriberChannel> topicPublisherMap = new HashMap<>();

    private Map<String, DirectChannel> subscribers = new ConcurrentHashMap<>();

    public void subscribe(String topic, DirectChannel channel) {
        if(Optional.ofNullable(subscribers.get(topic)).isPresent()) {
            unsubscribe(topic);
        }
        subscribers.put(topic, channel);
    }

    public void unsubscribe(String topic) {
        subscribers.remove(topic);
    }

    public void publish(String topic, Message<?> message) {
        FixedSubscriberChannel fsc = getFixedSubscriberChannel(topic);
        fsc.send(message);
    }

    private FixedSubscriberChannel getFixedSubscriberChannel(String topic) {
        if(!Optional.ofNullable(topicPublisherMap.get(topic)).isPresent()) {
            initFixedSubscriberChannel(topic);
        }
        return topicPublisherMap.get(topic);
    }

    private void initFixedSubscriberChannel(String topic) {
        ITopic<Message<?>> hazelcastTopic = hazelcastInstance.getTopic(topic);
        FixedSubscriberChannel fsc = new FixedSubscriberChannel(hazelcastTopic::publish);
        hazelcastTopic.addMessageListener(m -> {
           multicast(topic, m.getMessageObject());
        });
        topicPublisherMap.put(topic, fsc);
//        hazelcastTopic.addMessageListener(this);

    }

    private void multicast(String topic, Message<?> messageObject) {
        if(Optional.ofNullable(subscribers.get(topic)).isPresent()) {
            subscribers.get(topic).send(messageObject);
        }
//        subscribers.keySet().parallelStream()
//                .filter(key -> key.equals(topic))
//                .map(subscribers::get)
//                .filter(l -> l.getSubscriberCount() > 0)
//                .forEach(l -> l.send(messageObject));
    }


}
