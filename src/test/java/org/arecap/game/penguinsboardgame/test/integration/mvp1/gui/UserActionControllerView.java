package org.arecap.game.penguinsboardgame.test.integration.mvp1.gui;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.dom.DomEvent;
import com.vaadin.flow.dom.DomListenerRegistration;
import elemental.json.JsonArray;
import elemental.json.JsonObject;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class UserActionControllerView extends Div {

    private final static Logger logger = LoggerFactory.getLogger(UserActionControllerView.class);

    @Getter @Setter
    private DomUserActionObserver userActionObserver;

    private JsonObjectMouseUserActionConversionService mouseUserActionConversionService = new JsonObjectMouseUserActionConversionService();

    private JsonObjectWheelUserActionConversionService wheelUserActionConversionService = new JsonObjectWheelUserActionConversionService();

    private JsonObjectTouchUserActionConversionService touchUserActionConversionService = new JsonObjectTouchUserActionConversionService();

    public UserActionControllerView() {
        registerUserActions();
    }

    public UserActionControllerView(Component... components) {
        super(components);
        registerUserActions();
    }

    private void registerUserActions() {
        addClickListener(this::onUserClickAction);
        UI.getCurrent().getPage().executeJs(
                  "function getJsonTouches(event) {\n" +
                          "\tvar touchesData = [];\n" +
                          "\tfor(var i = 0; i < event.touches.length; ++i) {\n" +
                          "\t\tvar touche = event.touches[i];" +
                          "\t\ttouchesData.push({clientX: touche.clientX, clientY: touche.clientY," +
                                                    "screenX: touche.screenX, screenY: touche.screenY," +
                                                    "pageX: touche.pageX, pageY: touche.pageY," +
                                                    "rotationAngle: touch.rotationAngle || touch.webkitRotationAngle || 0, " +
                                                    "radiusX: touch.radiusX || touch.webkitRadiusX || 1," +
                                                    "radiusY: touch.radiusY || touch.webkitRadiusY || 1," +
                                                    "force: touch.force || touch.webkitForce || 0});\n" +
                          "\t}\n" +
                          "\treturn {altKey: event.altKey, changedTouches: event.changedTouches, shiftKey: event.shiftKey," +
                                            "ctrlKey: event.ctrlKey, metaKey: event.metaKey, targetTouches: event.targetTouches," +
                                            "touches: event.touches, touchesData: touchesData};\n" +
                          "};\n" +
                        "$(document).ready(function() {\n" +
                        "$0.addEventListener('touchstart', function(event){\n" +
                        "\t$0.$server.onUserTouchStartAction(getJsonTouches(event));\n" +
                        "\tevent.stopPropagation();\n" +
                          "\tevent.preventDefault();\n" +
                        "}, false);\n" +
                          "$0.addEventListener('touchmove', function(event){\n" +
                          "\t$0.$server.onUserTouchMoveAction(getJsonTouches(event));\n" +
                          "\tevent.stopPropagation();\n" +
                          "\tevent.preventDefault();\n" +
                          "}, false);\n" +
                        "var userActionMouseDown = false;\n" +
                        "$($0).mouseup(function(){\n" +
                        "\tuserActionMouseDown = false;\n" +
                        "});\n" +
                        "$($0).mousedown(function(){\n" +
                        "\tuserActionMouseDown = true;\n" +
                        "});\n" +
                        "$($0).mousemove(function(event){\n" +
                        "\tif (userActionMouseDown) {\n" +
                        "\t\t$0.$server.onUserMouseDownMoveAction({altKey: event.altKey, metaKey: event.metaKey," +
                          "shiftKey: event.shiftKey, ctrlKey: event.ctrlKey," +
                          "button: event.button, buttons: event.buttons, which: event.which," +
                          "offsetX: event.offsetX, offsetY: event.offsetY," +
                          "movementX: event.movementX, movementY: event.movementY," +
                          "pageX: event.pageX, pageY: event.pageY," +
                          "screenX: event.screenX, screenY: event.screenY," +
                          "clientX: event.clientX, clientY: event.clientY});\n" +
                        "\t}\n" +
                        "});\n" +
                        "});\n",
                getElement());
        registerMouseEventData(getElement().addEventListener("contextmenu", this::onUserContextMenuAction));
        registerMouseEventData(getElement().addEventListener("mouseenter", this::onUserMouseEnterAction));
        registerMouseEventData(getElement().addEventListener("mouseleave", this::onUserMouseLeaveAction));
        registerWheelEventData(getElement().addEventListener("wheel", this::onUserWheelAction));
        registerToucheEventData(getElement().addEventListener("touchcancel", this::onUserTouchCancelAction));
        registerToucheEventData(getElement().addEventListener("touchend", this::onUserTouchEndAction));
    }

    private void registerToucheEventData(DomListenerRegistration domListener) {
        domListener
                .addEventData("event.altKey")
                .addEventData("event.changedTouches")
                .addEventData("event.ctrlKey")
                .addEventData("event.metaKey")
                .addEventData("event.shiftKey")
                .addEventData("event.targetTouches")
                .addEventData("event.touches")
                .addEventData("event.stopPropagation()");
        //                .addEventData("event.preventDefault()");
    }


    private void registerWheelEventData(DomListenerRegistration domListener) {
        domListener
                .addEventData("event.deltaX")
                .addEventData("event.deltaY")
                .addEventData("event.deltaZ")
                .addEventData("event.deltaMode");
        registerMouseEventData(domListener);
    }

    private void registerMouseEventData(DomListenerRegistration domListener) {
        domListener
                .addEventData("event.altKey")
                .addEventData("event.button")
                .addEventData("event.buttons")
                .addEventData("event.clientX")
                .addEventData("event.clientY")
                .addEventData("event.ctrlKey")
                .addEventData("event.metaKey")
                .addEventData("event.movementX")
                .addEventData("event.movementY")
                .addEventData("event.offsetX")
                .addEventData("event.offsetY")
                .addEventData("event.pageX")
                .addEventData("event.pageY")
                .addEventData("event.screenX")
                .addEventData("event.screenY")
                .addEventData("event.shiftKey")
                .addEventData("event.which")
                .addEventData("event.stopPropagation()")
                .addEventData("event.preventDefault()");
    }

    @ClientCallable
    public void onUserTouchStartAction(JsonObject eventData) {
        logger.debug("user touchstart domEvent:\t"+eventData.toJson());
        List<TouchUserAction> touchesData = getTouchesData(eventData);
        AtomicInteger touchIndex = new AtomicInteger(0);
        Map<Integer, TouchUserAction> touches = touchesData.stream()
                .collect(Collectors.toMap(touchUserAction -> touchIndex.getAndIncrement(),
                touchUserAction -> touchUserAction));
        Map<Integer, TouchUserAction> targetTouches = new LinkedHashMap<>();
        Map<Integer, TouchUserAction> changedTouches = new LinkedHashMap<>();
        setTargetAndChangedTouches(eventData, touches, targetTouches, changedTouches);
        if(Optional.ofNullable(userActionObserver).isPresent()) {
            userActionObserver.userTouchStartAction(touches, targetTouches, changedTouches);
        }
    }

    private void setTargetAndChangedTouches(JsonObject eventData, Map<Integer, TouchUserAction> touches,
                                            Map<Integer, TouchUserAction> targetTouches, Map<Integer, TouchUserAction> changedTouches) {
        for(int i = 0; i < touches.size(); ++i) {
            if(eventData.getObject(JsonObjectUserActionConverterUtil.getEventDataKeyFor(eventData, "targetTouches")).hasKey(i + "")) {
                targetTouches.put(i, touches.get(i));
            }
            if(eventData.getObject(JsonObjectUserActionConverterUtil.getEventDataKeyFor(eventData, "changedTouches")).hasKey(i + "")) {
                changedTouches.put(i, touches.get(i));
            }
        }
    }

    private List<TouchUserAction> getTouchesData(JsonObject eventData) {
        List<TouchUserAction> touchesData = new ArrayList<>();
        JsonArray jsonTouchesData = eventData.getArray("touchesData");
        for(int i = 0; i < jsonTouchesData.length(); ++i) {
            TouchUserAction touchUserAction = touchUserActionConversionService.convert(jsonTouchesData.getObject(i));
            JsonObjectUserActionConverterUtil.setUserAction(touchUserAction, eventData);
            touchesData.add(touchUserAction);
        }
        return touchesData;
    }

    private void onUserTouchEndAction(DomEvent domEvent) {
        logger.debug("user touchend domEvent:\t"+domEvent.getEventData().toJson());
        List<Integer> touches = new ArrayList<>();
        List<Integer> targetTouches = new ArrayList<>();
        List<Integer> changedTouches = new ArrayList<>();
        setTouchesList(domEvent.getEventData(), touches, targetTouches, changedTouches);
        if(Optional.ofNullable(userActionObserver).isPresent()) {
            userActionObserver.userTouchEndAction(touches, targetTouches, changedTouches);
        }
    }

    private void onUserTouchCancelAction(DomEvent domEvent) {
        logger.debug("user touchcancel domEvent:\t"+domEvent.getEventData().toJson());
        List<Integer> touches = new ArrayList<>();
        List<Integer> targetTouches = new ArrayList<>();
        List<Integer> changedTouches = new ArrayList<>();
        setTouchesList(domEvent.getEventData(), touches, targetTouches, changedTouches);
        if(Optional.ofNullable(userActionObserver).isPresent()) {
            userActionObserver.userTouchCancelAction(touches, targetTouches, changedTouches);
        }
    }

    private void setTouchesList(JsonObject eventData, List<Integer> touches, List<Integer> targetTouches, List<Integer> changedTouches) {
        for(int i = 0; i < 5; ++i) {
            if(eventData.getObject(JsonObjectUserActionConverterUtil.getEventDataKeyFor(eventData, "touches")).hasKey(i + "")) {
                touches.add(i);
            }
            if(eventData.getObject(JsonObjectUserActionConverterUtil.getEventDataKeyFor(eventData, "targetTouches")).hasKey(i + "")) {
                targetTouches.add(i);
            }
            if(eventData.getObject(JsonObjectUserActionConverterUtil.getEventDataKeyFor(eventData, "changedTouches")).hasKey(i + "")) {
                changedTouches.add(i);
            }
        }
    }

    @ClientCallable
    public void onUserTouchMoveAction(JsonObject eventData) {
        logger.debug("user touchmove domEvent:\t"+eventData.toJson());
        List<TouchUserAction> touchesData = getTouchesData(eventData);
        AtomicInteger touchIndex = new AtomicInteger(0);
        Map<Integer, TouchUserAction> touches = touchesData.stream()
                .collect(Collectors.toMap(touchUserAction -> touchIndex.getAndIncrement(),
                        touchUserAction -> touchUserAction));
        Map<Integer, TouchUserAction> targetTouches = new LinkedHashMap<>();
        Map<Integer, TouchUserAction> changedTouches = new LinkedHashMap<>();
        setTargetAndChangedTouches(eventData, touches, targetTouches, changedTouches);
        if(Optional.ofNullable(userActionObserver).isPresent()) {
            userActionObserver.userTouchMoveAction(touches, targetTouches, changedTouches);
        }
    }

    private void onUserWheelAction(DomEvent domEvent) {
        logger.debug("user wheel domEvent:\t"+domEvent.getEventData().toJson());
        if(Optional.ofNullable(userActionObserver).isPresent()) {
            userActionObserver.userWheelAction(wheelUserActionConversionService.convert(domEvent.getEventData()));
        }
    }

    private void onUserMouseLeaveAction(DomEvent domEvent) {
        logger.debug("user mouse leave domEvent:\t"+domEvent.getEventData().toJson());
        if(Optional.ofNullable(userActionObserver).isPresent()) {
            userActionObserver.userMouseLeaveAction(mouseUserActionConversionService.convert(domEvent.getEventData()));
        }
    }

    private void onUserMouseEnterAction(DomEvent domEvent) {
        logger.debug("user mouse enter action domEvent:\t"+domEvent.getEventData().toJson());
        if(Optional.ofNullable(userActionObserver).isPresent()) {
            userActionObserver.userMouseEnterAction(mouseUserActionConversionService.convert(domEvent.getEventData()));
        }
    }

    private void onUserContextMenuAction(DomEvent domEvent) {
        logger.debug("user context menu action domEvent:\t"+domEvent.getEventData().toJson());
        if(Optional.ofNullable(userActionObserver).isPresent()) {
            userActionObserver.userContextMenuAction(mouseUserActionConversionService.convert(domEvent.getEventData()));
        }
    }

    @ClientCallable
    public void onUserMouseDownMoveAction(JsonObject eventData) {
        logger.debug("user mouse move action domEvent:\t"+eventData.toJson());
        if(Optional.ofNullable(userActionObserver).isPresent()) {
            userActionObserver.userMouseDownMoveAction(mouseUserActionConversionService.convert(eventData));
        }
    }

    private void onUserClickAction(ClickEvent<Div> divClickEvent) {
        logger.debug("user click action button info:\t" + divClickEvent.getButton()
                + "\tclick count info:\t"+divClickEvent.getClickCount() + "\tclientX:\t"
                + divClickEvent.getClientX() + "\tclientY:\t" + divClickEvent.getClientY());
        if(Optional.ofNullable(userActionObserver).isPresent()) {
            userActionObserver.userMouseClickAction(divClickEvent);
        }
    }

}
