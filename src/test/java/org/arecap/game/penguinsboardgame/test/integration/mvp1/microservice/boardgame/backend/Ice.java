package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

@Getter @Setter
public class Ice {

    private String uuid;

    private Integer fishValue;

    private DoubleWidthRegularHexagonGeometry geometry;

    private Integer rootNth;

    private String penguinUuid;;

    @JsonIgnore
    public Boolean isVisited() {
        return Optional.ofNullable(penguinUuid).isPresent();
    }

}
