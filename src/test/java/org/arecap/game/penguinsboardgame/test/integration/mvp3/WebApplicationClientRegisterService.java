package org.arecap.game.penguinsboardgame.test.integration.mvp3;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.arecap.game.penguinsboardgame.test.integration.mvp3.blockchain.BlockchainService;
import org.arecap.game.penguinsboardgame.test.integration.mvp3.blockchain.UuidBlock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;

@SpringComponent
@UIScope
@Slf4j
public class WebApplicationClientRegisterService {

    private static String topic = "webClient";

    public static String entity = "webClientRegistry";

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Autowired
    private BlockchainService<UuidBlock> blockchainService;

    @Autowired
    private ObjectMapper objectMapper;

    private String blockchainNodeId;

    @PostConstruct
    public void registerBlockchainService() {
        log.info("registerBlockchainService");
//        blockchainNodeId = blockchainService.registerNode(this::webClientRegistered, entity);
    }

    @SneakyThrows
    private void webClientRegistered(UuidBlock uuidBlock) {
        //todo
        log.info("new uuid block:\t" + objectMapper.writeValueAsString(uuidBlock));

    }

    @PreDestroy
    public void unregisterBlockchainService() {
        log.info("unregisterBlockchainService");
//        blockchainService.destroyNode(blockchainNodeId, entity);
    }


    @Async
    public void requestNewPlayerClient(Consumer<WebApplicationClientModel> clientCallback) {
        UuidBlock uuidBlock = blockchainService.mineBlock(UUID.randomUUID().toString(), entity);
        if(!isRegistered(uuidBlock.getHash())) {
            WebApplicationClientModel webClient = constructWebClientModel(uuidBlock.getHash());
            register(webClient);
            if(Optional.ofNullable(clientCallback).isPresent()) {
                clientCallback.accept(webClient);
            }
        }
    }

    public WebApplicationClientModel constructWebClientModel(String hash) {
        WebApplicationClientModel webClient = new WebApplicationClientModel();
        webClient.setUuid(hash);
        return webClient;
    }

    private IMap<String, WebApplicationClientModel> getRegistry() {
        return hazelcastInstance.getMap(entity);
    }

    public boolean isValid(String webClientUuid) {
        return blockchainService.valid(webClientUuid, entity);
    }

    public boolean isRegistered(String webClientUuid) {
        return getRegistry().containsKey(webClientUuid);
    }

    public WebApplicationClientModel getPlayerClient(String webClientUuid) {
        return getRegistry().get(webClientUuid);
    }

    @SneakyThrows
    public void register(WebApplicationClientModel webClientModel) {
        log.info("register playerClient instance:\t" + objectMapper.writeValueAsString(webClientModel));
        getRegistry().put(webClientModel.getUuid(), webClientModel);
    }

    @SneakyThrows
    public void unregister(String webClientUuid) {
        log.info("unregister playerClient instance:\t" + objectMapper.writeValueAsString(getRegistry().get(webClientUuid)));
        getRegistry().remove(webClientUuid);
    }


}
