package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.stats.gui;


import com.vaadin.flow.router.Route;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.gui.GameResponsiveLayout;

@Route("game-stats")
public class GameStatsRoute extends GameResponsiveLayout {
}
