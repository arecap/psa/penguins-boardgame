package org.arecap.game.penguinsboardgame.test.integration.mvp3.singleplay;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteConfiguration;
import elemental.json.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.arecap.game.penguinsboardgame.test.integration.mvp3.VaadinEndpoint;
import org.arecap.game.penguinsboardgame.test.integration.mvp3.WebApplicationClientModel;
import org.arecap.game.penguinsboardgame.test.integration.mvp3.WebApplicationEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.convert.ConversionService;

@Route("single-player")
@Push
@StyleSheet("https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css")
@StyleSheet("/frontend/css/styles.css")
@Viewport("width=device-width, initial-scale=1.0, user-scalable=no")
@Slf4j
public class SinglePlayerEndpoint extends VaadinEndpoint {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private SinglePlayerGamePlay gamePlay;

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        log.info("Component attached on UI");
    }

    @ClientCallable
    public void loadWebClientModel(JsonObject jsonObject) {
        log.info("JSON client model:\t" + jsonObject.toJson());
        gamePlay.load(((ConversionService)applicationContext.getBean("mvcConversionService")).convert(jsonObject, WebApplicationClientModel.class), getUI().get());
    }

    @ClientCallable
    public void mineWebClient() {
        log.warn("Web Client not register!! This should redirect to web client registration route");
        if(getUI().isPresent()) {
            getUI().get().access(() -> getUI().get().getPage().executeJs("window.location=$0;",
                    RouteConfiguration.forApplicationScope().getUrl(WebApplicationEndpoint.class)));
        }
    }

    @Override
    protected void onDetach(DetachEvent detachEvent) {
        log.info("Component detached from UI");
    }

    @Override
    public void afterNavigation(AfterNavigationEvent afterNavigationEvent) {
        super.afterNavigation(afterNavigationEvent);
        log.info("afterNavigation - load client Model");
        UI.getCurrent().getPage().addJavaScript("/frontend/js/PenguinUserMVP.js");
        UI.getCurrent().getPage()
                .executeJs("PenguinUserModelPresenter.mineOrLoadUserModel($0);",
                        this.getElement());
    }
}
