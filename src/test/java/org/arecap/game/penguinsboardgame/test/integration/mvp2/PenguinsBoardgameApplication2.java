package org.arecap.game.penguinsboardgame.test.integration.mvp2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class PenguinsBoardgameApplication2 {

    public static void main(String... args) {
        SpringApplication.run(PenguinsBoardgameApplication2.class, args);
    }
}
