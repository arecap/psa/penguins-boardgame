package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.gui;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.gui.DomUserActionObserver;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.gui.MouseUserAction;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.gui.TouchUserAction;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.gui.WheelUserAction;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@SpringComponent
@UIScope
public class PlayerBoardGameActionService implements BoardGameUserActionObserver, DomUserActionObserver, BoardGameAware {


    private final static Logger logger = LoggerFactory.getLogger(PlayerBoardGamePngService.class);

    @Autowired
    private ClientGraphicsSettings clientGraphicsSettings;

    private BoardGame boardGame;

    private Optional<Player> turnPlayer;

    private Boolean dblClickResetScale = true;

    private Double dlbClickScale = 1.4D;

    private Integer tapCount = 0;

    private Integer fingersCount = 0;

    @Value("${useraction.tapCountIntervalMillis:226}")
    private Long tapCountIntervalMillis;

    private Long tapCurrentMillis = System.currentTimeMillis();

    @Value("${useraction.touchSwipeIntervalMillis:50}")
    private Long touchSwipeIntervalMillis;

    private Long qDeltaTouchMoveMillis = 0L;

    private Long currentTouchMoveMillis = System.currentTimeMillis();

    private Optional<ComplexPlane> mouseDownClientXY = Optional.empty();

    private Long currentMouseMoveMillis = System.currentTimeMillis();

    private Map<Integer, TouchUserAction> lastTouches = new LinkedHashMap<>();

    private Map<Integer, TouchUserAction> lastTargetTouches = new LinkedHashMap<>();

    private Map<Integer, TouchUserAction> lastChangedTouches = new LinkedHashMap<>();

    @Override
    public void setBoardGame(BoardGame boardGame) {
        this.boardGame = boardGame;
    }

    @Override
    public void scale(Double scale) {
        logger.info("clientGraphicsSettings scale:\t" + scale);
        clientGraphicsSettings.setScale(scale);
        clientGraphicsSettings.refreshClientGraphicsSettings();
    }

    @Override
    public void pan(ComplexPlane pan) {
        logger.info("clientGraphicsSettings pan center X:\t" + pan.getRealValue() + "\tY:\t" + pan.getImaginaryValue());
        clientGraphicsSettings.setClientCenter(pan);
        clientGraphicsSettings.refreshClientGraphicsSettings();
    }

    @Override
    public void rotate(Double angle) {
        logger.info("clientGraphicsSettings rotation angle:\t" + angle);
        clientGraphicsSettings.setRotationAngle(angle);
        clientGraphicsSettings.refreshClientGraphicsSettings();
    }

    @Override
    public void selectIce(Ice ice) {
    }

    @Override
    public void selectPenguin(Penguin penguin) {
    }

    @Override
    public void moveTo(Ice ice) {
    }

    @Override
    public void userTouchStartAction(Map<Integer, TouchUserAction> touches, Map<Integer, TouchUserAction> targetTouches, Map<Integer, TouchUserAction> changedTouches) {
        try {
            ObjectMapper loggerObjectMapper = new ObjectMapper();
            logger.info("user start touches:\t " + loggerObjectMapper.writeValueAsString(touches) +
                    "\ttargetTouches:\t" + loggerObjectMapper.writeValueAsString(targetTouches) +
                    "\tchangedTouches:\t" + loggerObjectMapper.writeValueAsString(changedTouches));
        } catch (JsonProcessingException e) {
        }
        lastTouches = touches;
        lastTargetTouches = targetTouches;
        lastChangedTouches = changedTouches;
        fingersCount = touches.size();
        Long deltaTapCurrentMillis = System.currentTimeMillis() - tapCurrentMillis;
        logger.info("deltaTapCurrentMillis:\t" + deltaTapCurrentMillis + "\ttapCountIntervalMillis:\t"  + tapCountIntervalMillis);
        if(deltaTapCurrentMillis.compareTo(tapCountIntervalMillis) <= 0 && fingersCount == 1) {
            tapCount++;
        } else {
            tapCount = 0;
        }
        tapCurrentMillis = System.currentTimeMillis();
        currentTouchMoveMillis = System.currentTimeMillis();
        qDeltaTouchMoveMillis = 0L;
    }

    @Override
    public void userTouchEndAction(List<Integer> touches, List<Integer> targetTouches, List<Integer> changedTouches) {
        try {
            ObjectMapper loggerObjectMapper = new ObjectMapper();
            logger.info("user touches end touches:\t" + loggerObjectMapper.writeValueAsString(touches) +
                    "\ttargetTouches:\t" + loggerObjectMapper.writeValueAsString(targetTouches) +
                    "\tchangedTouches:\t" + loggerObjectMapper.writeValueAsString(changedTouches));
        } catch (JsonProcessingException e) {
        }
        if(qDeltaTouchMoveMillis.compareTo(0L) != 0) {
            if(qDeltaTouchMoveMillis.compareTo(touchSwipeIntervalMillis) <= 0) {
                logger.info("user swipe screen with:\t" + fingersCount + "\t finger" + (fingersCount <= 1 ? "" : "s"));
            } else {
                logger.info("user touch move with:\t" + fingersCount + "\t finger" + (fingersCount == 0 ? "" : "s"));
            }
        }
        fingersCount -= changedTouches.size();
        if(tapCount >= 1) {
            dlbClickOrTapScale();
        }

    }

    @Override
    public void userTouchCancelAction(List<Integer> touches, List<Integer> targetTouches, List<Integer> changedTouches) {
        try {
            ObjectMapper loggerObjectMapper = new ObjectMapper();
            logger.info("user touches cancel touches:\t" + loggerObjectMapper.writeValueAsString(touches) +
                    "\ttargetTouches:\t" + loggerObjectMapper.writeValueAsString(targetTouches) +
                    "\tchangedTouches:\t" + loggerObjectMapper.writeValueAsString(changedTouches));
        } catch (JsonProcessingException e) {
        }
        fingersCount -= changedTouches.size();
    }

    @Override
    public void userTouchMoveAction(Map<Integer, TouchUserAction> touches, Map<Integer, TouchUserAction> targetTouches, Map<Integer, TouchUserAction> changedTouches) {
        try {
            ObjectMapper loggerObjectMapper = new ObjectMapper();
            logger.info("user move touches:\t " + loggerObjectMapper.writeValueAsString(touches) +
                    "\ttargetTouches:\t" + loggerObjectMapper.writeValueAsString(targetTouches) +
                    "\tchangedTouches:\t" + loggerObjectMapper.writeValueAsString(changedTouches));
        } catch (JsonProcessingException e) {
        }

        if(fingersCount == 1) {
            ComplexPlane moveCenterWith = new ComplexPlane(lastTouches.get(0).getClientX() - touches.get(0).getClientX(),
                    lastTouches.get(0).getClientY() - touches.get(0).getClientY());
            pan(new ComplexPlane(clientGraphicsSettings.getClientCenter().getRealValue() - moveCenterWith.getRealValue(),
                    clientGraphicsSettings.getClientCenter().getImaginaryValue() - moveCenterWith.getImaginaryValue()));
        }
        if(fingersCount == 2) {
            Double distance = Math.hypot(touches.get(1).getClientX() - touches.get(0).getClientX(),
                    touches.get(1).getClientY() - touches.get(0).getClientY());
            Double lastDistance = Math.hypot(lastTouches.get(1).getClientX() - lastTouches.get(0).getClientX(),
                    lastTouches.get(1).getClientY() - lastTouches.get(0).getClientY());
            Double rotationAngle = Math.toDegrees(Math.atan2(touches.get(1).getClientY() - touches.get(0).getClientY(),
                    touches.get(1).getClientX() - touches.get(0).getClientX()));
            Double lastRotationAngle = Math.toDegrees(Math.atan2(lastTouches.get(1).getClientY() - lastTouches.get(0).getClientY(),
                    lastTouches.get(1).getClientX() - lastTouches.get(0).getClientX()));
            logger.info("distance:\t" + distance + "\tlastDistance:\t" + lastDistance + "\trotationAngle:\t" +
                    rotationAngle + "\tlastRotationAngle:\t" + lastRotationAngle);
            if(new Double(Math.abs(lastRotationAngle - rotationAngle)).compareTo(1.5D) > 0) {
                rotate(clientGraphicsSettings.getRotationAngle() + (lastRotationAngle.compareTo(rotationAngle) < 0 ? 0.01D :  -0.01D));
            } else {
                scale(clientGraphicsSettings.getScale() + (lastDistance.compareTo(distance) < 0 ? 0.01D :  -0.01D));
            }
        }

        Long deltaTouchMoveMillis = System.currentTimeMillis() - currentTouchMoveMillis;
        if(qDeltaTouchMoveMillis.compareTo(0L) != 0) {
            qDeltaTouchMoveMillis = (qDeltaTouchMoveMillis + deltaTouchMoveMillis) / 2;
        } else {
            qDeltaTouchMoveMillis = deltaTouchMoveMillis;
        }
        logger.info("deltaTouchMoveMillis:\t" + qDeltaTouchMoveMillis + "\ttouchSwipeIntervalMillis:\t"  + touchSwipeIntervalMillis);
        currentTouchMoveMillis = System.currentTimeMillis();
        lastTouches = touches;
        lastTargetTouches = targetTouches;
        lastChangedTouches = changedTouches;
    }

    @Override
    public void userWheelAction(WheelUserAction wheelUserAction) {
        try {
            logger.info("user wheel:\t " + new ObjectMapper().writeValueAsString(wheelUserAction));
        } catch (JsonProcessingException e) {
        }
        if(wheelUserAction.getCtrlKey()) {
            if(Math.abs(wheelUserAction.getDeltaY()) >= 20D) {
                scale(clientGraphicsSettings.getScale() + (wheelUserAction.getDeltaY().compareTo(0D) < 0 ? 0.01D :  -0.01D));
            }
        }
        if(wheelUserAction.getAltKey()) {
            if(Math.abs(wheelUserAction.getDeltaY()) >= 20D) {
                rotate(clientGraphicsSettings.getRotationAngle() + (wheelUserAction.getDeltaY().compareTo(0D) < 0 ? 0.01D :  -0.01D));
            }
        }
    }

    @Override
    public void userMouseLeaveAction(MouseUserAction mouseUserAction) {
        try {
            logger.info("user mouse leave:\t " + new ObjectMapper().writeValueAsString(mouseUserAction));
        } catch (JsonProcessingException e) {
        }
    }

    @Override
    public void userMouseEnterAction(MouseUserAction mouseUserAction) {
        try {
            logger.info("user mouse enter:\t " + new ObjectMapper().writeValueAsString(mouseUserAction));
        } catch (JsonProcessingException e) {
        }
    }

    @Override
    public void userContextMenuAction(MouseUserAction mouseUserAction) {
        try {
            logger.info("user context menu:\t " + new ObjectMapper().writeValueAsString(mouseUserAction));
        } catch (JsonProcessingException e) {
        }
    }

    @Override
    public void userMouseDownMoveAction(MouseUserAction mouseUserAction) {
        try {
            logger.info("user mouse down:\t " + new ObjectMapper().writeValueAsString(mouseUserAction));
        } catch (JsonProcessingException e) {
        }
        Long deltaTouchMoveMillis = System.currentTimeMillis() - currentMouseMoveMillis;
        if(deltaTouchMoveMillis.compareTo(30L) > 0) {
            mouseDownClientXY = Optional.empty();
        }
        Double distance = !mouseDownClientXY.isPresent() ? 0D : Math.hypot(mouseDownClientXY.get().getRealValue() - mouseUserAction.getClientX(),
                mouseDownClientXY.get().getImaginaryValue() - mouseUserAction.getClientY());
        logger.info("distance:\t" + distance);
        if(distance.compareTo(0D) != 0) {
            ComplexPlane moveCenterWith = new ComplexPlane(mouseDownClientXY.get().getRealValue() - mouseUserAction.getClientX(), mouseDownClientXY.get().getImaginaryValue() - mouseUserAction.getClientY());
            logger.info("moveCenterTo X:\t" + moveCenterWith.getRealValue() + "\tY:\t" + moveCenterWith.getImaginaryValue());
            pan(new ComplexPlane(clientGraphicsSettings.getClientCenter().getRealValue() - moveCenterWith.getRealValue(),
                    clientGraphicsSettings.getClientCenter().getImaginaryValue() - moveCenterWith.getImaginaryValue()));
        }
        mouseDownClientXY = Optional.of(new ComplexPlane(mouseUserAction.getClientX(), mouseUserAction.getClientY()));
        currentMouseMoveMillis = System.currentTimeMillis();
    }

    @Override
    public void userMouseClickAction(ClickEvent<Div> divClickEvent) {
        if(divClickEvent.getClickCount() >= 2) {
            dlbClickOrTapScale();
        }
    }

    private void dlbClickOrTapScale() {
        if(dblClickResetScale) {
            dblClickResetScale = false;
            dlbClickScale = clientGraphicsSettings.getScale();
            scale(1D);
        } else {
            dblClickResetScale = true;
            scale(dlbClickScale);
        }
    }

}
