package org.arecap.game.penguinsboardgame.test.integration.mvp2.messaging;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.SneakyThrows;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.support.ChannelInterceptor;

import java.util.logging.Level;
import java.util.logging.Logger;

@SpringComponent
@UIScope
public class UiClientMessagingDispatcher implements ChannelInterceptor, MessageHandler {

    private static Logger logger = Logger.getLogger(UiClientMessagingDispatcher.class.getCanonicalName());

    private ObjectMapper objectMapper = new ObjectMapper();

    @SneakyThrows
    @Override
    public void afterSendCompletion(Message<?> message, MessageChannel channel, boolean sent, Exception ex) {
        if(!sent) {
            logger.log(Level.SEVERE, "after sent message channel:\t" + channel.toString() + "fail to send message:\t" + ex.getStackTrace());
            return;
        }
        logger.info("after send message channel:\t" + channel.toString() + " send message:\t"
                + objectMapper.writeValueAsString(message));
    }

    @Override
    public void afterReceiveCompletion(Message<?> message, MessageChannel channel, Exception ex) {
        logger.info("after receive message channel:\t" + channel.toString());
    }

    @SneakyThrows
    @Override
    public void handleMessage(Message<?> message) throws MessagingException {
        logger.info("handle message:\t" + objectMapper.writeValueAsString(message));

    }
}
