package org.arecap.game.penguinsboardgame.test.integration.mvp3;

import lombok.Getter;
import lombok.Setter;
import org.springframework.statemachine.StateMachineContext;

import java.io.Serializable;

@Getter @Setter
public class WebApplicationClientModel implements Serializable {

    private String uuid;

    private StateMachineContext<String, String> ssmContext;

}
