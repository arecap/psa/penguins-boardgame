package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.hazelcast;

public interface EventPayload<E> {

    String getSenderMemberUuid();

    E getPayload();

}
