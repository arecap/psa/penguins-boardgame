package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.gui;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.router.*;
import com.vaadin.flow.shared.Registration;
import elemental.json.JsonObject;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.gui.UserActionControllerView;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.*;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.gui.GameResponsiveLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateMachine;

import javax.annotation.PostConstruct;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;


@Route("board-game")
@Push
@Viewport("width=device-width, initial-scale=1.0, user-scalable=no")
public class MainBoardGameRoute extends GameResponsiveLayout implements GeometryChangeListener, BeforeEnterObserver, BeforeLeaveObserver, ComponentEventListener<DetachEvent> {

    private Logger logger = LoggerFactory.getLogger(MainBoardGameRoute.class);

//    private Optional<BoardGame> boardGame = Optional.empty();

    @Autowired
    private ClientGraphicsSettings clientGraphicsSettings;

    @Autowired
    private PlayerBoardGamePngService playerBoardGamePngService;

    @Autowired
    private PlayerBoardGameActionService playerBoardGameActionService;

    @Autowired
    private PlayerExchange playerExchange;

    @Autowired
    private PlayerClient playerClient;

    @Autowired
    private JsonObjectBroadcaster broadcaster;

    private Registration broadcastRegistration;

    @Autowired
    private ViewController viewController;

    private Div boardBackground = new Div();

    private Image iceBoardImage = new Image();

    private Image iceBoardFishesImage = new Image();

    private Paragraph clientInfoParagraph = new Paragraph();

    private UserActionControllerView userActionControllerView = new UserActionControllerView();

    private PlayerHud playerHud = new PlayerHud();

    private MainMenu mainMenu = new MainMenu();

    @Autowired
    private StateMachine<GameStates, GameEvents> uiStateMachine;

    @PostConstruct
    private void build() {
        logger.info("post construct");
        ReconnectDialogConfiguration configuration = UI.getCurrent().getReconnectDialogConfiguration();
        configuration.setReconnectInterval(1000);
        clientGraphicsSettings.addGeometryChangeListener(this);
        getStyle().set("position", "relative");
        getStyle().set("overflow", "hidden");
        boardBackground.getStyle().set("position", "absolute");
        boardBackground.getStyle().set("width", "200%");
        boardBackground.getStyle().set("height", "200%");
        boardBackground.getStyle().set("top", "-50%");
        boardBackground.getStyle().set("left", "-50%");
        boardBackground.getStyle().set("background-image", "url('frontend/images/tablecloth1.jpeg')");
        boardBackground.getStyle().set("background-repeat", "repeat");
        boardBackground.getStyle().set("background-color", "#fff");
        setSizeFull();
        setupImages(iceBoardImage, iceBoardFishesImage);
        userActionControllerView.getStyle().set("position", "absolute");
        userActionControllerView.getStyle().set("top", "0px");
        userActionControllerView.getStyle().set("left", "0px");
        userActionControllerView.getStyle().set("z-index", "1000");
        userActionControllerView.setSizeFull();
        userActionControllerView.setUserActionObserver(playerBoardGameActionService);
        playerHud.setMainMenu(Optional.of(mainMenu));
        UI.getCurrent().addDetachListener(this);
        add(clientInfoParagraph, boardBackground, iceBoardImage,
                iceBoardFishesImage, userActionControllerView, playerHud, mainMenu, viewController);
    }

    private void initPlayerClient(String uuid) {
        if(getUI().isPresent()) {
            logger.info("UI State Machine UUID:\t" + uiStateMachine.getUuid() + "\tinstance:\t" + uiStateMachine);
        }
    }

    private void initNewPlayerClient(String uuid) {
        if(getUI().isPresent()) {
            UI ui = getUI().get();
            ui.access(() -> ui.getCurrent().getPage().executeJs("PenguinUserModelPresenter.initModel($0);", uuid));
            broadcastRegistration = broadcaster.register(uuid, this::postProcessBroadcast);
            logger.info("UI State Machine UUID:\t" + uiStateMachine.getUuid() + "\tinstance:\t" + uiStateMachine);
        }
    }

    @Override
    public void afterNavigation(AfterNavigationEvent afterNavigationEvent) {
        super.afterNavigation(afterNavigationEvent);
        UI.getCurrent().getPage().addJavaScript("/frontend/js/PenguinUserMVP.js");
        UI.getCurrent().getPage().addJavaScript("/frontend/js/PenguinRTC.js");
        UI.getCurrent().getPage().executeJs("initialize($0);\n" +
                "createOffer($0);\n" +
                "PenguinUserModelPresenter.mineOrLoadUserModel($0);", getElement());

    }

    @Override
    public void onComponentEvent(DetachEvent event) {
        logger.info("detach UI player uuid:\t" + playerClient.getUuid());
        if(Optional.ofNullable(broadcastRegistration).isPresent()) {
            broadcastRegistration.remove();
            broadcastRegistration = null;
        }
    }

    public void postProcessBroadcast(JsonObject message) {
        logger.info("Got message:\t" + message.toJson());
        if(getUI().isPresent()) {
            logger.info("UI has page");
            UI ui = getUI().get();
            switch (message.getString("event")) {
                case "offer":
                    logger.info("offer:\t" + message.getObject("data").toJson());
                    ui.access(() -> ui.getPage().executeJs("handleOffer($0, $1)", message.getObject("data").toJson(), getElement()));
                    break;
                case "answer":
                    logger.info("answer:\t" + message.getObject("data").toJson());
                    ui.access(() -> ui.getPage().executeJs("handleAnswer($0)", message.getObject("data").toJson()));
                    break;
                case "candidate":
                    logger.info("candidate:\t" + message.getObject("data").toJson());
                    ui.access(() -> ui.getPage().executeJs("handleCandidate($0)", message.getObject("data").toJson()));
                    break;
            }
        } else {
            logger.info("UI is not present!");
        }
    }

    @ClientCallable
    public void mineNewClient() {
        logger.info("mine client uuidBlock");
        playerClient.registerMine(this::initNewPlayerClient);
    }

    @ClientCallable
    public void loadUserModel(JsonObject userModel) {
        String clientUuid = userModel.getString("clientUuid");
        if(playerClient.isValid(clientUuid)) {
            playerClient.setUuid(clientUuid);
            playerClient.register(this::initPlayerClient);
            broadcastRegistration = broadcaster.register(clientUuid, this::postProcessBroadcast);
            logger.info("load user model:\t" + userModel.toJson());
        }  else {
            playerClient.registerMine(this::initNewPlayerClient);
            logger.error("client uuid block:\t" + clientUuid + "\tis invalid!");
        }
    }

    @ClientCallable
    public void postProcessMessage(JsonObject message) {
        broadcaster.broadcast(playerClient.getUuid(), message);
    }

    private void setupImages(Image... images) {
        AtomicInteger zIndex = new AtomicInteger(10);
        Stream.of(images).forEach(image -> setupImage(image, zIndex.getAndAdd(10)));
    }

    private void setupImage(Image image, int zIndex) {
        image.getStyle().set("position", "absolute");
        image.getStyle().set("top", "0px");
        image.getStyle().set("left", "0px");
        image.getStyle().set("z-index", zIndex + "");
    }

    @Override
    public void clientGraphicsUpdate() {
        //TODO init board game delegate to service
        if(!Optional.ofNullable(playerExchange.getBoardGame()).isPresent()) {
            playerExchange.setBoardGame(BoardGameService.constructBoardGame(4, clientGraphicsSettings));
        } else {
            BoardGameService.updateBoardGame2dGraphicTransformSettings(playerExchange.getBoardGame(), clientGraphicsSettings);
        }
        playerBoardGamePngService.setBoardGame(playerExchange.getBoardGame());
        playerBoardGameActionService.setBoardGame(playerExchange.getBoardGame());
        //TODO end
        Player player =  playerExchange.getBoardGame().getPlayers().values().stream().findAny().get();
        logger.info("player color code:\t" + player.getColorCode());
        playerHud.setupPlayers(playerExchange.getBoardGame(), player, clientGraphicsSettings);
        boardBackground.getStyle().set("transform", "rotate("+clientGraphicsSettings.getRotationAngle()+"deg)");
        iceBoardImage.setSrc(playerBoardGamePngService.getIceBoard());
        iceBoardFishesImage.setSrc(playerBoardGamePngService.getIceBoardFishes());
    }

    private void setClientInfo(int width, int height) {
        clientInfoParagraph.setText("Browser window size width:\t" + width + "\theight:\t" + height);
        clientGraphicsSettings.setClientBounds(new ComplexPlane(width * 1D, height * 1D));
    }

    @Override
    public void beforeLeave(BeforeLeaveEvent beforeLeaveEvent) {
        clientGraphicsSettings.removeGeometryChangeListener(this);
    }

    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        UI.getCurrent().getPage().retrieveExtendedClientDetails(clientGraphicsSettings);
        UI.getCurrent().getPage().addBrowserWindowResizeListener(clientGraphicsSettings);
    }

}
