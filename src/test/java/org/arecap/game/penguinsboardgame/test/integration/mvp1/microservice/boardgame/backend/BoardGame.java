package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter @Setter
public class BoardGame {

    private String uuid;

    private Long gameElapsedTimeMs;

    private Integer playersNumber;

    private String currentPlayerUuid;

    private Map<String, Player> players;

    private Map<String, Penguin> penguins;

    private Map<Integer, Ice> iceBoard;

    private Map<String, String> penguinsRepartition;

    private Map<String, List<String>> playerPenguins;

}
