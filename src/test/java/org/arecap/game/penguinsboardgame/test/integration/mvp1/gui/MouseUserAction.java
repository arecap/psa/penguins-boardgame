package org.arecap.game.penguinsboardgame.test.integration.mvp1.gui;

import lombok.Getter;
import lombok.Setter;

public class MouseUserAction extends CoordinateUserAction {

    @Getter @Setter
    private Double offsetX;

    @Getter @Setter
    private Double offsetY;

    @Getter @Setter
    private Integer button;

    @Getter @Setter
    private Integer buttons;

    @Getter @Setter
    private Integer which;

}
