package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Game2dGraphicTransformSettings {

    private ComplexPlane clientBounds;

    private ComplexPlane clientCenter;

    private Double scale = 1D;

    private Double rotationAngle = 0D;


}
