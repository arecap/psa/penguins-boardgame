package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.gui;

import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.BoardGame;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.IceBoardFishesPngService;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.IceBoardPngService;
import org.springframework.beans.factory.annotation.Autowired;

@SpringComponent
@UIScope
public class PlayerBoardGamePngService  implements BoardGameAware {

    @Autowired
    private ClientGraphicsSettings clientGraphicsSettings;

    private BoardGame boardGame;


    @Override
    public void setBoardGame(BoardGame boardGame) {
        this.boardGame = boardGame;
    }

    public StreamResource getIceBoard() {
        IceBoardPngService iceBoardPngService = new IceBoardPngService(boardGame);
        return new StreamResource("ice_board_"+System.currentTimeMillis(), () -> iceBoardPngService.draw(clientGraphicsSettings));
    }

    public StreamResource getIceBoardFishes() {
        IceBoardFishesPngService iceBoardFishesPngService = new IceBoardFishesPngService(boardGame);
        return new StreamResource("ice_board_fishes"+System.currentTimeMillis(), () -> iceBoardFishesPngService.draw(clientGraphicsSettings));
    }

    public StreamResource getIceBoardPenguinRepartition() {
        return null;
    }

}
