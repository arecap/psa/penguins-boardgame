package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;


import lombok.Getter;
import lombok.Setter;

public class ComplexPlane {

    @Getter @Setter
    private Double imaginaryValue;

    @Getter @Setter
    private Double realValue;

    public ComplexPlane() {
    }

    public ComplexPlane(Double real, Double imaginary) {
        this.realValue = real;
        this.imaginaryValue = imaginary;
    }
}
