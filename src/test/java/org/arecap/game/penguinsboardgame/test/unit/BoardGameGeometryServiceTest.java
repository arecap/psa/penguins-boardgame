package org.arecap.game.penguinsboardgame.test.unit;

import org.arecap.game.penguinsboardgame.test.integration.mvp1.graphics.Graphics2dUtils;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.graphics.PngGraphics2d;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.BoardGameGeometryService;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.ComplexPlane;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.DoubleWidthRegularHexagonGeometry;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.Game2dGraphicTransformSettings;
import org.beryx.awt.color.ColorFactory;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.awt.*;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class BoardGameGeometryServiceTest implements PngGraphics2d {

    @Test
    public void testIceBoard() {
        Game2dGraphicTransformSettings game2dGraphicTransformSettings = new Game2dGraphicTransformSettings();
        game2dGraphicTransformSettings.setClientBounds(new ComplexPlane(1200d, 900d));
        game2dGraphicTransformSettings.setClientCenter(new ComplexPlane(600D, 450D));
        game2dGraphicTransformSettings.setScale(1.2D);
        game2dGraphicTransformSettings.setRotationAngle(Math.PI / 6D);
        try {
            File targetFile = new File("test_board_game.png");
            Files.copy(draw(game2dGraphicTransformSettings),targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            //            IOUtils.closeQuietly(is);
        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void drawContent(Graphics2D graphics2D, Game2dGraphicTransformSettings game2dGraphicTransformSettings) {
        List<DoubleWidthRegularHexagonGeometry> iceBoard = BoardGameGeometryService.getBoardGameGeometry(game2dGraphicTransformSettings);
        Assert.isTrue(iceBoard.size() == 60, "Fail iceBoard size:\t" + iceBoard.size());
        AtomicInteger idx = new AtomicInteger(0);
        iceBoard.stream().forEach(ice -> drawIce(graphics2D, game2dGraphicTransformSettings, ice, idx.getAndIncrement()));
    }

    private synchronized void drawIce(Graphics2D graphics2D, Game2dGraphicTransformSettings game2dGraphicTransformSettings,
                                      DoubleWidthRegularHexagonGeometry ice, Integer idx) {
//        if(idx <= 0) {
        graphics2D.setColor(ColorFactory.BLUE);
        Graphics2dUtils.drawPoint(graphics2D, ice.getOrigin().getRealValue(), ice.getOrigin().getImaginaryValue() ,
                1.6D, 1.8D);
            graphics2D.setColor(ColorFactory.RED);
            Assert.isTrue(ice.isInRadiusRange(ice.getOrigin()), "Formula!!!");
            for (ComplexPlane vertex : ice.getVerticesClockWise(game2dGraphicTransformSettings.getScale())) {
                Graphics2dUtils.drawPoint(graphics2D, vertex.getRealValue(),
                        vertex.getImaginaryValue(), 1.6D, 1.8D);
            }
//        }
    }
}
