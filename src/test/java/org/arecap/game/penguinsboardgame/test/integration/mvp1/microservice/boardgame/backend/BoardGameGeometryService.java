package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;

import java.util.ArrayList;
import java.util.List;

public final class BoardGameGeometryService {


    public static List<DoubleWidthRegularHexagonGeometry> getBoardGameGeometry(Game2dGraphicTransformSettings game2dGraphicTransformSettings) {
        List<DoubleWidthRegularHexagonGeometry> iceBoard = new ArrayList<>();
//        Double realDeltaRotation = Math.cos(Math.toRadians(game2dGraphicTransformSettings.getRotationAngle()));
//        Double imaginaryDeltaRotation = Math.sin(Math.toRadians(game2dGraphicTransformSettings.getRotationAngle()));
//        Double imaginaryDeviation = game2dGraphicTransformSettings.getClientCenter().getImaginaryValue() * imaginaryDeltaRotation;
//        game2dGraphicTransformSettings.getClientCenter()
//                .setRealValue( //game2dGraphicTransformSettings.getClientCenter().getRealValue() +
//                        game2dGraphicTransformSettings.getClientCenter().getRealValue() * realDeltaRotation);
//        game2dGraphicTransformSettings.getClientCenter()
//                .setImaginaryValue(game2dGraphicTransformSettings.getClientCenter().getImaginaryValue() -
//                        imaginaryDeviation);
        DoubleWidthRegularHexagonGeometry measurementIce = new DoubleWidthRegularHexagonGeometry();
        measurementIce.setInRadius(game2dGraphicTransformSettings.getClientBounds().getRealValue() > 900D ? 52D : 30D);
        measurementIce.setRotationAngle(0D);
        Double totalWidth = 8D * game2dGraphicTransformSettings.getScale() * measurementIce.getWidth(),
                totalHeight = (8D * 3D / 4D + 1 / 4D) * game2dGraphicTransformSettings.getScale() * measurementIce.getHeight();
        for(int row = 0; row < 8; ++row) {
            for(int column = 0; column < (row % 2 != 0 ? 8 : 7); ++column) {
                DoubleWidthRegularHexagonGeometry ice = new DoubleWidthRegularHexagonGeometry();
                ice.setInRadius(measurementIce.getInRadius());
                ice.setRotationAngle(measurementIce.getRotationAngle());
                ComplexPlane origin =getOrigin(ice, game2dGraphicTransformSettings.getScale(), row, column);
                ice.setOrigin(
                        new ComplexPlane(
                                getClientOriginValue(origin.getRealValue(),
                                        2D * game2dGraphicTransformSettings.getClientCenter().getRealValue(),
                                        totalWidth ),
                        getClientOriginValue(origin.getImaginaryValue(),
                                2D * game2dGraphicTransformSettings.getClientCenter().getImaginaryValue(),
                                totalHeight )));
//                ice.setOrigin(origin);
                iceBoard.add(ice);
            }
        }
        return iceBoard;
    }

    private static Double getClientOriginValue(Double originValue, Double clientCenterValue, Double iceBoardCenterValue) {
        return originValue + (clientCenterValue - iceBoardCenterValue) / 2D;
    }

    private static ComplexPlane getOrigin(DoubleWidthRegularHexagonGeometry ice, Double scale, Integer row, Integer column) {
        return new ComplexPlane(getOriginRealValue(ice, scale, row, column), getOriginImaginaryValue(ice, scale, row));
    }

    private static Double getOriginRealValue(DoubleWidthRegularHexagonGeometry ice, Double scale, Integer row,Integer column) {
        return ice.getWidth() / 2  + (row % 2 == 0 ? ice.getInRadius() * scale : 0) + ( column  ) * ice.getWidth() * scale;
    }

    private static Double getOriginImaginaryValue(DoubleWidthRegularHexagonGeometry ice, Double scale, Integer row) {
        return ice.getHeight() / 2 + (row ) * (3D / 4D) * ice.getHeight() * scale;
    }

}
