package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class DoubleWidthRegularHexagonGeometry {

    @Getter @Setter
    private ComplexPlane origin;

    @Getter @Setter
    private Double inRadius;

    @Getter @Setter
    private Double rotationAngle = 0D;

    @JsonIgnore
    public Double getWidth() {
        ComplexPlane inPlane = getRotationEllipseInRadiusPlane();
        return 2 * inPlane.getRealValue();
    }

    @JsonIgnore
    public Double getHeight() {
        ComplexPlane circumPlane = getRotationEllipseCircumRadiusPlane();
        return 2 * circumPlane.getImaginaryValue();
    }

    @JsonIgnore
    public Double getMinimaMaximaFactor() {
        return 2 / Math.sqrt(3);
    }

    @JsonIgnore
    public Double getCircumRadius() {
        return inRadius * getMinimaMaximaFactor();
    }

    @JsonIgnore
    public ComplexPlane getRotationEllipseInRadiusPlane() {
        return RotationMatrix.getComplexPlaneWithRotationAngle(new ComplexPlane(inRadius, inRadius), rotationAngle);
    }

    @JsonIgnore
    public ComplexPlane getRotationEllipseCircumRadiusPlane() {
        Double circumRadius = getCircumRadius();
        return RotationMatrix.getComplexPlaneWithRotationAngle(new ComplexPlane(circumRadius, circumRadius), rotationAngle);
    }

    @JsonIgnore
    public List<ComplexPlane> getVerticesClockWise(Double scale) {
        List<ComplexPlane> clockWiseVertices = new ArrayList<>();
        ComplexPlane circumPlane = getRotationEllipseCircumRadiusPlane();
        ComplexPlane vertexRoot1Real = RotationMatrix.getComplexPlaneWithRotationAngle(
                RootsOfUnity.get10RootOf12(circumPlane.getRealValue() * scale), rotationAngle);
        ComplexPlane vertexRoot1Imaginary = RotationMatrix.getComplexPlaneWithRotationAngle(
                RootsOfUnity.get10RootOf12(circumPlane.getImaginaryValue() * scale), rotationAngle);
        clockWiseVertices.add(new ComplexPlane(getOrigin().getRealValue() + vertexRoot1Real.getRealValue(),
                getOrigin().getImaginaryValue() + vertexRoot1Imaginary.getImaginaryValue()));
        ComplexPlane vertexRoot2Real = RotationMatrix.getComplexPlaneWithRotationAngle(
                RootsOfUnity.get12RootOf12(circumPlane.getRealValue() * scale), rotationAngle);
        ComplexPlane vertexRoot2Imaginary = RotationMatrix.getComplexPlaneWithRotationAngle(
                RootsOfUnity.get12RootOf12(circumPlane.getImaginaryValue() * scale), rotationAngle);
        clockWiseVertices.add(new ComplexPlane(getOrigin().getRealValue() + vertexRoot2Real.getRealValue(),
                getOrigin().getImaginaryValue() + vertexRoot2Imaginary.getImaginaryValue()));
        ComplexPlane vertexRoot3Real = RotationMatrix.getComplexPlaneWithRotationAngle(
                RootsOfUnity.get2RootOf12(circumPlane.getRealValue() * scale), rotationAngle);
        ComplexPlane vertexRoot3Imaginary = RotationMatrix.getComplexPlaneWithRotationAngle(
                RootsOfUnity.get2RootOf12(circumPlane.getImaginaryValue() * scale), rotationAngle);
        clockWiseVertices.add(new ComplexPlane(getOrigin().getRealValue() + vertexRoot3Real.getRealValue(),
                getOrigin().getImaginaryValue() + vertexRoot3Imaginary.getImaginaryValue()));
        ComplexPlane vertexRoot4Real = RotationMatrix.getComplexPlaneWithRotationAngle(
                RootsOfUnity.get4RootOf12(circumPlane.getRealValue() * scale), rotationAngle);
        ComplexPlane vertexRoot4Imaginary = RotationMatrix.getComplexPlaneWithRotationAngle(
                RootsOfUnity.get4RootOf12(circumPlane.getImaginaryValue() * scale), rotationAngle);
        clockWiseVertices.add(new ComplexPlane(getOrigin().getRealValue() + vertexRoot4Real.getRealValue(),
                getOrigin().getImaginaryValue() + vertexRoot4Imaginary.getImaginaryValue()));
        ComplexPlane vertexRoot5Real = RotationMatrix.getComplexPlaneWithRotationAngle(
                RootsOfUnity.get6RootOf12(circumPlane.getRealValue() * scale), rotationAngle);
        ComplexPlane vertexRoot5Imaginary = RotationMatrix.getComplexPlaneWithRotationAngle(
                RootsOfUnity.get6RootOf12(circumPlane.getImaginaryValue() * scale), rotationAngle);
        clockWiseVertices.add(new ComplexPlane(getOrigin().getRealValue() + vertexRoot5Real.getRealValue(),
                getOrigin().getImaginaryValue() + vertexRoot5Imaginary.getImaginaryValue()));
        ComplexPlane vertexRoot6Real = RotationMatrix.getComplexPlaneWithRotationAngle(
                RootsOfUnity.get8RootOf12(circumPlane.getRealValue() * scale), rotationAngle);
        ComplexPlane vertexRoot6Imaginary = RotationMatrix.getComplexPlaneWithRotationAngle(
                RootsOfUnity.get8RootOf12(circumPlane.getImaginaryValue() * scale), rotationAngle);
        clockWiseVertices.add(new ComplexPlane(getOrigin().getRealValue() + vertexRoot6Real.getRealValue(),
                getOrigin().getImaginaryValue() + vertexRoot6Imaginary.getImaginaryValue()));
        return clockWiseVertices;
    }

    @JsonIgnore
    public Boolean isInRadiusRange(ComplexPlane complexPlane) {
        ComplexPlane inPlane = getRotationEllipseInRadiusPlane();
        return ((complexPlane.getRealValue() - getOrigin().getRealValue())
                * (complexPlane.getRealValue() - getOrigin().getRealValue())) /
                (inPlane.getRealValue() * inPlane.getRealValue())
                +
                ((complexPlane.getImaginaryValue() - getOrigin().getImaginaryValue()) *
                        (complexPlane.getImaginaryValue() - getOrigin().getImaginaryValue())) /
                        (inPlane.getImaginaryValue() * inPlane.getImaginaryValue())
                <= 1;
    }

}
