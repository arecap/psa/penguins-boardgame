package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.gui;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.Message;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend.hazelcast.TopicBroadcaster;

import java.io.Serializable;
import java.util.Optional;
import java.util.stream.Stream;

public abstract class DistributedBroadcaster<T> implements Broadcaster<T> {


    public enum DistributedBroadcasterEvent {
        NEW_INSTANCE, INIT_CP_SUBSYSTEM, RESTART, SHUTDOWN
    }

    public enum DistributedSystemType {
        NA, SINGLE_NODE, DUAL_CHANNEL, RAFT_CONSENSUS
    }

    @Getter @Setter
    static class DistributedBroadcasterEventPayload implements Serializable {
        private DistributedBroadcasterEvent eventType;
        private String senderMemberUuid;

        public DistributedBroadcasterEventPayload() {
        }

        public DistributedBroadcasterEventPayload(DistributedBroadcasterEvent eventType, String senderMemberUuid) {
            this.eventType = eventType;
            this.senderMemberUuid = senderMemberUuid;
        }
    }


//    class DistributedBroadcasterMembershipListener implements MembershipListener {
//
//        @SneakyThrows
//        @Override
//        public void memberAdded(MembershipEvent membershipEvent) {
//            logger.info("member added:\t" + membershipEvent.getMember().getUuid() +
//                    "\tdistributedSystemType:\t" + getDistributedSystemType());
////            setInstanceBroadcasterRegistryRef(getInstanceBroadcaster().register(membershipEvent.getMember().getUuid(),
////                    event -> getInstanceBroadcaster().instanceMessage(event)));
//            if(getDistributedSystemType().equals(DistributedSystemType.RAFT_CONSENSUS)
//                    && instance.getConfig().getCPSubsystemConfig().getCPMemberCount() == 0) {
//                instanceBroadcaster.broadcast(INTERNAL_BROADCAST_TOPIC, new DistributedBroadcasterEventPayload(
//                        DistributedBroadcaster.DistributedBroadcasterEvent.RESTART, membershipEvent.getMember().getUuid()));
//            }
//        }
//
//        @SneakyThrows
//        @Override
//        public void memberRemoved(MembershipEvent membershipEvent) {
//            logger.info("member removed:\t" + membershipEvent.getMember().getUuid());
////            if(!DistributedBroadcaster.getDistributedSystemType().equals(DistributedBroadcaster.DistributedSystemType.RAFT_CONSENSUS)) {
////                instanceBroadcaster.broadcast(INTERNAL_BROADCAST_TOPIC, DistributedBroadcaster.DistributedBroadcasterEvent.RESTART);
//////                getInstanceBroadcaster().broadcast(membershipEvent.getMember().getUuid(), DistributedBroadcaster.DistributedBroadcasterEvent.RESTART);
////                destroyInstanceBroadcasterRegistryRef();
////            }
//        }
//
//        @Override
//        public void memberAttributeChanged(MemberAttributeEvent memberAttributeEvent) {
//            //N/A
//        }
//
//    }

//    class DistributedBroadcasterLifecycleListener implements LifecycleListener {
//
//        @Override
//        public void stateChanged(LifecycleEvent event) {
//            logger.info("event:\t" + event);
//        }
//
//    }

//    class DistributedBroadcasterEventListener implements MessageListener<DistributedBroadcasterEvent> {
//
//        @Override
//        public void onMessage(Message<DistributedBroadcasterEvent> message) {
//            logger.info("post porcess event:\t" + message.getMessageObject());
//            Stream.of(eventProcessor).parallel().forEach(eventProcessor -> eventProcessor.event(message.getMessageObject()));
//
//        }
//    }

    class InstanceBroadcaster implements TopicBroadcaster<DistributedBroadcasterEventPayload>  {

        @Getter @Setter
        private String topicRegistryRef;

        @Override
        public HazelcastInstance getInstance() {
            return instance;
        }

        @SneakyThrows
        @Override
        public void onMessage(Message<DistributedBroadcasterEventPayload> message) {
            logger.info("post porcess event:\t" + objectMapper.writeValueAsString(message.getMessageObject()));
            Stream.of(eventProcessor).parallel().forEach(eventProcessor -> eventProcessor.event(message.getMessageObject()));
        }
    }

//    class InstanceBroadcaster implements Broadcaster<DistributedBroadcasterEvent> {
//
//        @Override
//        public Map<String, Consumer<DistributedBroadcasterEvent>> getConsumerMap() {
//            return instance.getMap("hInstanceBroadcaster");
//        }
//
//
//        /*
//         * only one process the message
//         */
//        public void instanceMessage(DistributedBroadcasterEvent distributedBroadcasterEvent) {
//        }
//
//
//    }

    interface DistributedBroadcastEventProcessor {

        boolean accept(DistributedBroadcasterEventPayload event);

        default void event(DistributedBroadcasterEventPayload event) {
            if(accept(event)) {
                postProcessEvent();
            }
        }

        void postProcessEvent();

    }

    class NewInstanceEventProcessor implements  DistributedBroadcastEventProcessor {

        private DistributedBroadcasterEventPayload eventPayload;

        @Override
        public boolean accept(DistributedBroadcasterEventPayload event) {
            eventPayload = event;
            return event.getEventType().equals(DistributedBroadcasterEvent.NEW_INSTANCE);
        }

        @Override
        public void postProcessEvent() {
            if(getDistributedSystemType().equals(DistributedSystemType.RAFT_CONSENSUS) && !hasCpSubsystem()) {
                logger.info("broadcast init CP subsystem member uuid:\t" + getLocalMemberUuid());
                destroy();
                instance = Hazelcast.newHazelcastInstance(configWithCpSubsystem());
                instanceBroadcaster.register(INTERNAL_BROADCAST_TOPIC);
                instanceBroadcaster.broadcast(INTERNAL_BROADCAST_TOPIC, new DistributedBroadcasterEventPayload(
                        DistributedBroadcasterEvent.NEW_INSTANCE, getLocalMemberUuid()
                        ));
                //                instance.getConfig().getCPSubsystemConfig().setCPMemberCount(3);
//                instanceBroadcaster.broadcast(INTERNAL_BROADCAST_TOPIC, new DistributedBroadcasterEventPayload(
//                        DistributedBroadcasterEvent.INIT_CP_SUBSYSTEM, getLocalMemberUuid()
//                ));
            }
        }
    }

//    class InitCpSubsystemEventProcessor implements  DistributedBroadcastEventProcessor {
//
//        @Override
//        public boolean accept(DistributedBroadcasterEventPayload event) {
//            return event.getEventType().equals(DistributedBroadcasterEvent.INIT_CP_SUBSYSTEM);
//        }
//
//        @Override
//        public void postProcessEvent() {
//            logger.info("instance:\t" + getLocalMemberUuid() +" config cp subsystem");
//            instance.getConfig().getCPSubsystemConfig().setCPMemberCount(3);
//        }
//    }

    class ShutdownEventProcessor implements DistributedBroadcastEventProcessor {

        private DistributedBroadcasterEventPayload eventPayload;

        @Override
        public boolean accept(DistributedBroadcasterEventPayload event) {
            eventPayload = event;
            return event.getEventType().equals(DistributedBroadcasterEvent.SHUTDOWN);
        }

        @Override
        public void postProcessEvent() {
//            instance.getLifecycleService().terminate();
        }
    }

    class RestartEventProcessor implements DistributedBroadcastEventProcessor {

        private DistributedBroadcasterEventPayload eventPayload;

        @Override
        public boolean accept(DistributedBroadcasterEventPayload event) {
            eventPayload = event;
            return event.getEventType().equals(DistributedBroadcasterEvent.RESTART);
        }

        @Override
        public void postProcessEvent() {
            if(!instance.getCluster().getLocalMember().getUuid().equals(eventPayload.getSenderMemberUuid())) {
                destroy();
                init();
            }
        }
    }

    interface DistributedBroadcasterInitializer {

        boolean accept();

        default void init() {
            if(accept()) {
                postProcessInitializer();
            }
        }

        void postProcessInitializer();
    }

    abstract class DistributedBroadcasterListenersConfigurationInitializer implements DistributedBroadcasterInitializer {


        public void postProcessInitializer() {
            constructInstance();
//            instance.getCluster().addMembershipListener(new DistributedBroadcasterMembershipListener());
//            instance.getLifecycleService().addLifecycleListener(new DistributedBroadcasterLifecycleListener());
            instanceBroadcaster.register(INTERNAL_BROADCAST_TOPIC);
            instanceBroadcaster.broadcast(INTERNAL_BROADCAST_TOPIC, new DistributedBroadcasterEventPayload(
                    DistributedBroadcasterEvent.NEW_INSTANCE, getLocalMemberUuid()
            ));
        }

        abstract void constructInstance();
    }

    private boolean hasCpSubsystem() {
        return instance.getConfig().getCPSubsystemConfig().getCPMemberCount() != 0;
    }

    private boolean isCpSubsystemRunning() {
        return instance.getConfig().getCPSubsystemConfig().getCPMemberCount() > 2;
    }

    private String getLocalMemberUuid() {
        return instance.getCluster().getLocalMember().getUuid();
    }

    class SingleOrDualNodeDistributedBroadcasterInitializer extends DistributedBroadcasterListenersConfigurationInitializer{

        @Override
        public boolean accept() {
            return !getDistributedSystemType().equals(DistributedSystemType.RAFT_CONSENSUS);
        }

        @Override
        public void constructInstance() {
            instance = Hazelcast.newHazelcastInstance(config());
        }

    }


    class RaftConsensusDistributedBroadcasterInitializer extends DistributedBroadcasterListenersConfigurationInitializer {

        @Override
        public boolean accept() {
            return getDistributedSystemType().equals(DistributedSystemType.RAFT_CONSENSUS);
        }


        @Override
        public void constructInstance() {
            instance = Hazelcast.newHazelcastInstance(configWithCpSubsystem());
        }

    }


    private static String INTERNAL_BROADCAST_TOPIC = "INTERNAL_BROADCAST_TOPIC";

    @Getter @Setter
    private DistributedSystemType distributedSystemType = getDistributedSystemType();

    private InstanceBroadcaster instanceBroadcaster = new InstanceBroadcaster();

    private HazelcastInstance instance;

    private DistributedBroadcasterInitializer[] initializerSystem =
            { new SingleOrDualNodeDistributedBroadcasterInitializer(), new RaftConsensusDistributedBroadcasterInitializer() };


    private DistributedBroadcastEventProcessor[] eventProcessor =
            { new NewInstanceEventProcessor(), // new InitCpSubsystemEventProcessor(),
                    new ShutdownEventProcessor(), new RestartEventProcessor() };


//    @Setter
//    private Registration instanceBroadcasterRegistryRef;

//    @Getter
//    private final InstanceBroadcaster instanceBroadcaster = this.new InstanceBroadcaster();


    //    private


    public void init() {
        logger.info("start broadcaster initialization");
        Stream.of(initializerSystem).parallel().forEach(DistributedBroadcasterInitializer::init);
    }

    public void destroy() {
        destroyInstanceBroadcasterRegistryRef();
        if(Optional.ofNullable(instance).isPresent() && instance.getLifecycleService().isRunning()) {
            instance.getLifecycleService().terminate();
        }
    }

    public void destroyInstanceBroadcasterRegistryRef() {
        logger.info("destroy instance ref");
        if(Optional.ofNullable(instanceBroadcaster.getTopicRegistryRef()).isPresent()) {
            instanceBroadcaster.destroy(INTERNAL_BROADCAST_TOPIC);
        }
    }

    private Config config() {
        Config config = new Config();
//        config
//                .addListenerConfig(new ListenerConfig("org.arecap.game.penguinsboardgame.microservice.boardgame.gui.DistributedBroadcasterMembershipListener"));
        return config;
    }

    private Config configWithCpSubsystem() {
        Config config = config();
        config.getCPSubsystemConfig().setCPMemberCount(3);
        return config;
    }

    public static boolean shouldCpSubsystemEnable() {
        return Hazelcast.getAllHazelcastInstances().size() >= 3;
    }

    public static DistributedSystemType getDistributedSystemType() {
        switch (Hazelcast.getAllHazelcastInstances().size()) {
            case 0: return DistributedSystemType.NA;
            case 1: return DistributedSystemType.SINGLE_NODE;
            case 2: return DistributedSystemType.DUAL_CHANNEL;
            default: return DistributedSystemType.RAFT_CONSENSUS;
        }
//Which you like more. Which is more performant? Why?
//
//        return Hazelcast.getAllHazelcastInstances().size() == 0 ? DistributedSystemType.NA :
//                (Hazelcast.getAllHazelcastInstances().size() == 1 ? DistributedSystemType.SINGLE_NODE :
//                        (Hazelcast.getAllHazelcastInstances().size() < 3 ? DistributedSystemType.DUAL_CHANNEL :
//                        DistributedSystemType.RAFT_CONSENSUS));
    }

}
