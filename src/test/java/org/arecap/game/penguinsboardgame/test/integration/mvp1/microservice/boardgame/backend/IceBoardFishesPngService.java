package org.arecap.game.penguinsboardgame.test.integration.mvp1.microservice.boardgame.backend;

import org.arecap.game.penguinsboardgame.test.integration.mvp1.graphics.Graphics2dUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class IceBoardFishesPngService extends BoardGamePngService {

    private final static Logger logger = LoggerFactory.getLogger(IceBoardFishesPngService.class);

    public IceBoardFishesPngService(BoardGame boardGame) {
        super(boardGame);
    }

    @Override
    protected synchronized void drawIce(Graphics2D graphics2D, Game2dGraphicTransformSettings game2dGraphicTransformSettings, Ice ice) {
        try {
            BufferedImage fishSource = getFishImage(ice.getFishValue());
            Image fishImage = Toolkit.getDefaultToolkit().createImage(Graphics2dUtils.resizeImage(fishSource, ice.getGeometry().getCircumRadius() * game2dGraphicTransformSettings.getScale() / fishSource.getHeight()).getSource());
            ComplexPlane origin = RotationMatrix.getComplexPlaneWithRotationAngle(ice.getGeometry().getOrigin(), game2dGraphicTransformSettings.getRotationAngle());
            graphics2D.drawImage(fishImage,
                        origin.getRealValue().intValue() - fishImage.getWidth(null) / 2,
                        origin.getImaginaryValue().intValue() - fishImage.getHeight( null) / 2,
                    null);
        } catch(Throwable t){
            logger.error("error image draw", t);
        }
    }

    private BufferedImage getFishImage(Integer fishValue) throws IOException {
        return Graphics2dUtils.getBufferedImage("static/frontend/images/fish" + fishValue + ".png");
    }

    @Override
    public void drawContent(Graphics2D graphics2D, Game2dGraphicTransformSettings game2dGraphicTransformSettings) {
        getBoardGame().getIceBoard().values().parallelStream()
                .filter(ice -> !ice.isVisited())
                .forEach(ice -> drawIce(graphics2D, game2dGraphicTransformSettings, ice));
        graphics2D.setColor(Color.CYAN);
        DoubleWidthRegularHexagonGeometry geometry = getBoardGame().getIceBoard().values().stream().findAny().get().getGeometry();
        Double totalWidth = 8D * game2dGraphicTransformSettings.getScale() *  geometry.getWidth(),
                totalHeight = 8D * game2dGraphicTransformSettings.getScale() * geometry.getHeight();
        Graphics2dUtils.drawPoint(graphics2D,  totalWidth / 2D + (game2dGraphicTransformSettings.getClientBounds().getRealValue() - totalWidth ) / 2D,
                 totalHeight / 2D + (game2dGraphicTransformSettings.getClientBounds().getImaginaryValue() - totalHeight ) / 2D,
                3D, 4D);
        graphics2D.setColor(Color.RED);
        Graphics2dUtils.drawPoint(graphics2D,  game2dGraphicTransformSettings.getClientCenter().getRealValue(),
                game2dGraphicTransformSettings.getClientCenter().getImaginaryValue(),
                3D, 2D);
    }

}
