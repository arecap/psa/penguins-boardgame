package org.arecap.game.penguinsboardgame.test.integration.mvp1.gui;

import elemental.json.JsonObject;
import org.springframework.core.convert.converter.Converter;

public class JsonObjectWheelUserActionConversionService implements Converter<JsonObject, WheelUserAction> {

    @Override
    public WheelUserAction convert(JsonObject eventData) {
        WheelUserAction wheelUserAction = new WheelUserAction();
        JsonObjectUserActionConverterUtil.setWheelUserAction(wheelUserAction, eventData);
        return wheelUserAction;
    }

}
