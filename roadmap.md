# Penguins Board Game engineering road map

![](https://gitlab.com/arecap/psa/penguins-boardgame/-/raw/master/src/test/resources/static/frontend/images/logo.png)

## First chapter 
### What is penguins board game?

The Penguins Board Game is a social board game.

This open source project try to publish and to be available to be played by users in web. The present project should be feature the game play in graphical user interface capable of instantiate single and multiplayer Penguins Board Game sessions. 

The original board game it can be found here https://www.fantasyflightgames.com/en/products/hey-thats-my-fish/

The game rules are described by this link https://www.yourturnmyturn.com/rules/penguin.php

### How the application can run?

![](https://gitlab.com/arecap/psa/penguins-boardgame/-/raw/master/docs/Penguins%20Board%20Game%20Running%20Environment.png)
